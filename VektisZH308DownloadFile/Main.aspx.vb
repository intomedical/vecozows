﻿Option Strict Off
Imports System
Imports System.IO
Imports System.IO.Compression
Imports Ionic.Zip

Public Class WebForm1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    End Sub

    'Protected Sub btnDownload0_Click(sender As Object, e As EventArgs) Handles btnDownload0.Click
    '    Dim aVektisDGM As New VektisDGM

    '    Dim memStream As MemoryStream = aVektisDGM.GetVektisDGMFile(Integer.Parse(txtVecozoBatchID.Text))
    '    If memStream IsNot Nothing Then
    '        Dim file As FileStream = New FileStream("c:\temp\" + txtVecozoBatchID.Text + ".txt", FileMode.Create, FileAccess.Write)
    '        memStream.WriteTo(file)
    '        file.Close()
    '        memStream.Close()
    '    End If

    'End Sub

    'Protected Sub btnWSValideerZH308_Click(sender As Object, e As EventArgs) Handles btnWSValideerZH308.Click
    '    Dim VektisZH308 As New VektisZH308ZH309
    '    Dim oVSPEDPValidatieIndienen As New VSPEDPValidatieIndienen(getCertificate, getEndPoint(EndpointType.IndienenValidatieV1Soap11))

    '    'Dim memStream As MemoryStream = VektisZH308.GetVektisZH308File(Integer.Parse(txtVecozoBatchID.Text))
    '    Dim aFile As String = "AY12112.txt"
    '    Dim bData As Byte()
    '    Dim br As BinaryReader = New BinaryReader(System.IO.File.OpenRead("c:\temp\" & aFile))
    '    bData = br.ReadBytes(CInt(br.BaseStream.Length))
    '    Dim memStream As MemoryStream = New MemoryStream(bData, 0, bData.Length)
    '    memStream.Write(bData, 0, bData.Length)
    '    br.Close()

    '    If memStream IsNot Nothing Then
    '        Dim respons As VSPEDPValidatieIndienen.ValidatieIndienenResultaat = oVSPEDPValidatieIndienen.ValidatieIndienen(memStream, aFile)
    '        'Dim file As FileStream = New FileStream("c:\temp\" + txtVecozoBatchID.Text + ".txt", FileMode.Create, FileAccess.Write)
    '        txtResultaatIndienenValidatie.Text = respons.DebugText
    '        memStream.Close()
    '    End If
    'End Sub

    'Protected Sub btnWSValideerZH309_Click(sender As Object, e As EventArgs) Handles btnWSValideerZH309.Click
    '    Dim oVSPEDPValidatieDownloaden As New VSEDPValidatieDownloaden(getCertificate, getEndPoint(EndpointType.DownloadenValidatieV1Soap11))
    '    Dim respons As VSEDPValidatieDownloaden.ValidatieDownloadenResultaat = oVSPEDPValidatieDownloaden.ValidatieDownloaden(Integer.Parse(txtValidatieIDforWS.Text))
    '    txtResultaatIDowloadenValidatie.Text = respons.DebugText
    'End Sub

    'Private Sub btnWSValideerZH309_Load(sender As Object, e As EventArgs) Handles btnWSValideerZH309.Load
    '    'txtThumbprintCertificate.Text = "74477c76fcbbe329e431859b758fcc93e3157207" 'ACC PERS AY I2M 
    '    If txtThumbprintCertificateACC.Text = "" Then txtThumbprintCertificateACC.Text = "26ac3ac97eb858eb724e85e1f75d497641834056" 'ACC SYSTEEM I2M
    '    If txtThumbprintCertificatePRD.Text = "" Then txtThumbprintCertificatePRD.Text = "aff093259ba2c38300aae5082872b6451d698411" 'PRD SYSTEEM MCV
    '    If txtVecozoBatchID.Text = "" Then txtVecozoBatchID.Text = "2002"
    '    If txtValidatieIDforWS.Text = "" Then txtValidatieIDforWS.Text = "11454270" '"11454059"
    'End Sub

    'Protected Sub btnWSIndienenDeclaratie_Click(sender As Object, e As EventArgs) Handles btnWSIndienenDeclaratie.Click
    '    Dim oVSPEDPDeclaratieIndienen As New VSPEDPDeclaratieIndienen(getCertificate, getEndPoint(EndpointType.IndienenDeclaratieV1Soap11))

    '    Dim aFile As String = "ZH308-basisbestand-correct.txt"
    '    Dim bData As Byte()
    '    Dim br As BinaryReader = New BinaryReader(System.IO.File.OpenRead("C:\Dropbox\Intomedical\Vecozo\ZH308\" & aFile))
    '    bData = br.ReadBytes(CInt(br.BaseStream.Length))
    '    Dim memStream As MemoryStream = New MemoryStream(bData, 0, bData.Length)
    '    memStream.Write(bData, 0, bData.Length)
    '    br.Close()

    '    If memStream IsNot Nothing Then
    '        Dim respons As VSPEDPDeclaratieIndienen.DeclaratieIndienenResultaat = oVSPEDPDeclaratieIndienen.DeclaratieIndienen(memStream, aFile)
    '        txtResultaatIDowloadenValidatie.Text = respons.DebugText
    '        memStream.Close()
    '    End If

    'End Sub

    'Protected Sub btnWSDownloadDeclaratie_Click(sender As Object, e As EventArgs) Handles btnWSDownloadDeclaratie.Click
    '    Dim oVSPEDPDeclaratie As New VSEDPDeclaratieDownloaden(getCertificate, getEndPoint(EndpointType.DownloadenRetourinformatieV1Soap11))
    '    Dim respons As VSEDPDeclaratieDownloaden.DeclaratieDownloadenResultaat = oVSPEDPDeclaratie.DeclaratieDownloaden(Integer.Parse(txtValidatieIDforWS.Text))
    '    If respons IsNot Nothing Then
    '        txtResultaatIDowloadenValidatie.Text = respons.DebugText
    '    Else
    '        txtResultaatIDowloadenValidatie.Text = "ONBEKENDE FOUT"
    '    End If
    'End Sub

    'Function getEndPoint(Endpoint As EndpointType) As String
    '    Dim iIndex As Integer
    '    If rblstEnvironment.SelectedValue = "T" Then
    '        iIndex = 0
    '    ElseIf rblstEnvironment.SelectedValue = "A" Then
    '        iIndex = 1
    '    ElseIf rblstEnvironment.SelectedValue = "P" Then
    '        iIndex = 2
    '    Else
    '        Return ""
    '    End If

    '    Select Case Endpoint
    '        Case EndpointType.IndienenDeclaratieV1
    '            Return EndpointsIndienenDeclaratieV1(iIndex)
    '        Case EndpointType.IndienenDeclaratieV1Soap11
    '            Return EndpointsIndienenDeclaratieV1Soap11(iIndex)
    '        Case EndpointType.DownloadenRetourinformatieV1
    '            Return EndpointsDownloadenRetourinformatieV1(iIndex)
    '        Case EndpointType.DownloadenRetourinformatieV1Soap11
    '            Return EndpointsDownloadenRetourinformatieV1Soap11(iIndex)
    '        Case EndpointType.IndienenValidatieV1
    '            Return EndpointsIndienenValidatieV1(iIndex)
    '        Case EndpointType.IndienenValidatieV1Soap11
    '            Return EndpointsIndienenValidatieV1Soap11(iIndex)
    '        Case EndpointType.DownloadenValidatieV1
    '            Return EndpointsDownloadenValidatieV1(iIndex)
    '        Case EndpointType.DownloadenValidatieV1Soap11
    '            Return EndpointsDownloadenValidatieV1Soap11(iIndex)
    '        Case EndpointType.IndienenRetourinformatieV1
    '            Return EndpointsIndienenRetourinformatieV1(iIndex)
    '        Case EndpointType.IndienenRetourinformatieV1Soap11
    '            Return EndpointsIndienenRetourinformatieV1Soap11(iIndex)
    '    End Select

    'End Function

    'Function getCertificate() As String
    '    If rblstEnvironment.SelectedValue = "T" Then
    '        Return txtThumbprintCertificateTST.Text
    '    ElseIf rblstEnvironment.SelectedValue = "A" Then
    '        Return txtThumbprintCertificateACC.Text
    '    ElseIf rblstEnvironment.SelectedValue = "P" Then
    '        Return txtThumbprintCertificatePRD.Text
    '    Else
    '        Return ""
    '    End If

    'End Function

    '    Protected Sub btnWSDeclaratieStatus_Click(sender As Object, e As EventArgs) Handles btnWSDeclaratieStatus.Click
    '        Dim oVSPEDPDeclaratie As New VSEDPDeclaratieDownloaden(getCertificate, getEndPoint(EndpointType.DownloadenRetourinformatieV1Soap11))
    '        Dim respons As VSEDPDeclaratieDownloaden.DeclaratieStatusResultaat = oVSPEDPDeclaratie.DeclaratieStatus(Integer.Parse(txtValidatieIDforWS.Text))
    '        If respons IsNot Nothing Then
    '            txtResultaatIDowloadenValidatie.Text = respons.DebugText
    '        Else
    '            txtResultaatIDowloadenValidatie.Text = "ONBEKENDE FOUT"
    '        End If
    '    End Sub

    '    Protected Sub btnProcessZH309_Click(sender As Object, e As EventArgs) Handles btnProcessZH309.Click
    '        Dim VektisZH308 As New VektisZH308ZH309

    '        Dim aFiles() As String = {
    '"W:\Financiën\070 - Facturen\Vecozo\AY\AEV170778979.zip",
    '"W:\Financiën\070 - Facturen\Vecozo\AY\R_EDP_202005151359_173549749.zip",
    '"W:\Financiën\070 - Facturen\Vecozo\AY\zrg8145e_4_145997609.zip",
    '"W:\Financiën\070 - Facturen\Vecozo\AY\zrg8145e_4_145997614.zip"}

    '        'Dim aFile As String
    '        For Each aFile As String In aFiles


    '            Dim bData As Byte()
    '            Dim br As BinaryReader = New BinaryReader(System.IO.File.OpenRead(aFile))
    '            bData = br.ReadBytes(CInt(br.BaseStream.Length))
    '            Dim memStream As MemoryStream = New MemoryStream(bData, 0, bData.Length)
    '            memStream.Write(bData, 0, bData.Length)
    '            memStream.Position = 0
    '            br.Close()

    '            If Ionic.Zip.ZipFile.IsZipFile(memStream, True) Then  '(FileUpload1.FileContent, True) Then '(memStream, True) Then
    '                Dim mems As MemoryStream = New MemoryStream()
    '                Dim oZipArchive As ZipArchive = New ZipArchive(memStream) '(FileUpload1.FileContent) '(memStream)
    '                For Each oZipArchiveEntry As ZipArchiveEntry In oZipArchive.Entries
    '                    Dim memsXX As Stream = oZipArchiveEntry.Open()
    '                    memsXX.CopyTo(mems)
    '                    mems.Position = 0
    '                Next
    '                If mems IsNot Nothing Then
    '                    Dim respons As VektisZH308ZH309.Resultaat = VektisZH308.ProcessVektisZH309File(mems)
    '                    If respons IsNot Nothing Then
    '                        txtResultaatProcessZH309.Text = respons.DebugTextLight
    '                        Dim bDoIT As Boolean = VektisZH308.UpdateZH309(respons)
    '                    Else
    '                        txtResultaatProcessZH309.Text = "ONBEKENDE FOUT"
    '                    End If

    '                    mems.Close()
    '                End If
    '                'memStream.Close()
    '            End If
    '        Next
    '    End Sub


End Class