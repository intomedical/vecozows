﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="VecozoRetour.aspx.vb" Inherits="VektisZH308DownloadFile.VecozoRetour" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 217px;
        }
        .auto-style1 {
            width: 217px;
            height: 30px;
        }
        .auto-style2 {
            height: 30px;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="style1">
            <tr>
                <td class="style2">
                    <a href="Main.aspx">Main</a></td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style1">
                    </td>
                <td class="auto-style2">
                    <asp:CheckBox ID="chkSimulatie" runat="server" Text="Simulatie" Checked="True" />
                </td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    Selecteer bestand(en)</td>
                <td>
                    <asp:FileUpload ID="FileUpload1" runat="server" style="margin-bottom: 4px" AllowMultiple="True" />
                &nbsp;&nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;</td>
                <td>
                    <asp:Button ID="btnProcessZH309" runat="server" Text="Verwerk Retourbericht" />
                </td>
            </tr>
            <tr>
                <td class="style2" valign="top">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2" valign="top">
                    Resultaat</td>
                <td>
                    <asp:TextBox ID="txtResultaatHeader" runat="server" Height="340px" Width="623px" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2" valign="top">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2" valign="top">
                    Detail</td>
                <td>
                    <asp:TextBox ID="txtResultaatProcessZH309" runat="server" Height="340px" Width="623px" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            </table>
    
    </div>
    </form>
</body>
</html>
