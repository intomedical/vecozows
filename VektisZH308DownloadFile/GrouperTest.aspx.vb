﻿Option Strict Off
Imports System

Public Class GrouperTest
    Inherits System.Web.UI.Page

    Protected Sub btnControleer_Click(sender As Object, e As EventArgs) Handles btnControleer.Click
        Dim s As String = txtBehandelID.Text
        txtResultaat.Text = "Groupercheck gestart"
        txtBericht.Text = ""
        If Not chkWriteDB.Checked Then
            txtResultaat.Text += " in simulatie mode"
        End If
        txtResultaat.Text += "." + vbCrLf
        Try
            Dim behandelids As String() = s.Split(New Char() {","c})

            Dim behandelid As Integer
            For Each behandelid In behandelids
                txtBericht.Text += "=============================================" + vbCrLf

                txtResultaat.Text += "=============================================" + vbCrLf
                txtResultaat.Text += "BehandelID: " + behandelid.ToString + vbCrLf

                Dim aGrouper As New Grouper(1, 1, "YOKARIBA")
                If aGrouper IsNot Nothing Then

                    Dim aGrouperResult As Grouper.GrouperResult = aGrouper.Controleer(behandelid)
                    If aGrouperResult IsNot Nothing Then
                        If chkWriteDB.Checked Then
                            Dim result As Boolean = aGrouper.Insert(aGrouperResult)
                        End If
                        txtBericht.Text += aGrouperResult.DebugText
                    Else
                        txtBericht.Text += aGrouper.Message
                    End If
                    txtResultaat.Text += "Resultaat: " + aGrouper.Resultaat.ToString + vbCrLf
                End If
            Next

        Catch ex As Exception
            txtResultaat.Text = "Undefined error: " & ex.Message
        End Try
    End Sub
End Class