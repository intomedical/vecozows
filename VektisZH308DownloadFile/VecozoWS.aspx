﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="VecozoWS.aspx.vb" Inherits="VektisZH308DownloadFile.VecozoWS" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 217px;
        }
        .auto-style7 {
            margin-bottom: 41px;
        }
        .auto-style8 {
            height: 333px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="style1">
            <tr>
                <td>
                    <a href="Main.aspx">Hoofdmenu</a></td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style8" colspan="2">
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" DataSourceID="SqlDataSource1" AutoGenerateColumns="False" KeyFieldName="VecozoBatchID" CssClass="auto-style7">
            <SettingsPager Visible="False" Mode="ShowAllRecords">
            </SettingsPager>
            <Settings ShowFilterRow="True" HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto" />
            <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
            <Columns>
                <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowSelectCheckbox="True" VisibleIndex="0">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn FieldName="VecozoBatchID" ReadOnly="True" VisibleIndex="1">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="InsuranceCompanyOrgcode" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="OrgName" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="UZOVICode" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Batchnummer" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataDateColumn FieldName="Batchdate" VisibleIndex="6">
                </dx:GridViewDataDateColumn>
                <dx:GridViewDataTextColumn FieldName="VecozoBatchSendmethodID" VisibleIndex="7">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="VecozoBatchSendMethod" VisibleIndex="8">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="MessageStandardID" VisibleIndex="9">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="VecozoMessageStandard" VisibleIndex="10">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="VecozoBatchStatusID" VisibleIndex="11">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="VecozoBatchStatus" VisibleIndex="12">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="VecozoDeclaratieID" VisibleIndex="13">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Period" VisibleIndex="14">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Bookyear" VisibleIndex="15">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataDateColumn FieldName="Senddate" VisibleIndex="16">
                </dx:GridViewDataDateColumn>
                <dx:GridViewDataDateColumn FieldName="Returndate" VisibleIndex="17">
                </dx:GridViewDataDateColumn>
                <dx:GridViewDataTextColumn FieldName="Processed" VisibleIndex="19">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="ClientID" VisibleIndex="31">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Requested" ReadOnly="True" VisibleIndex="32">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Assigned" ReadOnly="True" VisibleIndex="33">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="LocationID" VisibleIndex="36">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="LocationName" VisibleIndex="37">
                </dx:GridViewDataTextColumn>
            </Columns>
        </dx:ASPxGridView>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:IntomediFin %>" SelectCommand="select * from av_VecozoBatch where Deleted = 'F' and vecozobatchid &gt; 5000"></asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:CheckBox ID="chkSimulatie" runat="server" Text="Simulatie" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Button ID="btnWSValideerZH308" runat="server" Text="WS Indienen Validatie" />
                    <asp:Button ID="btnWSIndienenDeclaratie" runat="server" Text="WS Indienen Declaratie" />
                </td>
            </tr>
            <tr>
                <td>
                    Resultaat</td>
                <td>
                    <asp:TextBox ID="txtResultaatIndienenValidatie" runat="server" Height="121px" Width="447px" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Button ID="btnWSValideerZH309" runat="server" Text="WS Download Validatie" />
                    <asp:Button ID="btnWSDownloadDeclaratie" runat="server" Text="WS Download Declaratie" />
                    <asp:Button ID="btnWSDeclaratieStatus" runat="server" Text="WS DeclaratieStatus" />
                </td>
            </tr>
            <tr>
                <td>
                    Resultaat</td>
                <td>
                    <asp:TextBox ID="txtResultaatIDowloadenValidatie" runat="server" Height="114px" Width="447px" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
