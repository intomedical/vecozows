﻿Imports System.IO
Imports DevExpress.Web

Public Class OHWGrouperLines
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim DISBatchID As String = Request.QueryString("DISBatchID")


        SqlDataSource2.SelectCommand = "SELECT * FROM av_OHWBatchAll WHERE OHWGrouperID = " + DISBatchID + " ORDER BY LocationName, Koepel, UZOVICode"
        ASPxGridView1.DataBind()
    End Sub

    Protected Sub cmdDownloadZH310_Click(sender As Object, e As EventArgs) Handles cmdDownloadZH310.Click
        Dim aVektisZH310 As New VektisZH308ZH309

        '        Response.Write("<script type=""text/javascript"">alert(""" + "TEST" + """);</script")
        Dim Message As String = ""
        Dim OHWBatchID As Long
        Dim selectedValues As List(Of Object)

        Dim fieldNames As List(Of String) = New List(Of String)()
        For Each column As GridViewColumn In ASPxGridView1.Columns
            If TypeOf column Is GridViewDataColumn Then
                fieldNames.Add((CType(column, GridViewDataColumn)).FieldName)
            End If
        Next column
        selectedValues = ASPxGridView1.GetSelectedFieldValues(fieldNames.ToArray())

        Dim glob As New Chilkat.Global
        Dim success As Boolean = glob.UnlockBundle("AYKRBS.CB1032020_sGdL35KunR1I")
        If (success <> True) Then
            Message = "Fout Chillkat\n\n" + glob.LastErrorText
            Response.Write("<script type=""text/javascript"">alert(""" + Message + """);</script")
            Exit Sub
        End If

        Dim tempFile As String = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName())
        Dim zip As New Chilkat.Zip
        success = zip.NewZip(tempFile)
        If (success <> True) Then
            Message = "Fout bij maken van zipbestand '" + tempFile + "'\n\n" + zip.LastErrorText
            Response.Write("<script type=""text/javascript"">alert(""" + Message + """);</script")
            Exit Sub
        End If

        Dim i As Integer = 0
        Dim aFile As String = ""
        If selectedValues.Count > 0 Then
            For Each selectedvalue As Object() In selectedValues
                If i = 0 Then
                    aFile = "ZH310_" + selectedvalue(4).ToString() + "_" + selectedvalue(5).ToString() + "_" + selectedvalue(7).ToString() + Format(selectedvalue(6).ToString(), "00") + "_" + selectedvalue(11).ToString() + ".zip"
                End If
                OHWBatchID = DirectCast(selectedvalue(0), Long)
                Dim memStream As MemoryStream = aVektisZH310.GetVektisZH310File(OHWBatchID)
                If memStream IsNot Nothing Then
                    Dim entry As Chilkat.ZipEntry = zip.AppendData("ZH310_" + OHWBatchID.ToString + ".txt", memStream.ToArray())
                    memStream.Close()
                End If
                i += 1
            Next
            Dim zipFileInMemory() As Byte

            zipFileInMemory = zip.WriteToMemory()
            Dim fac As New Chilkat.FileAccess
            success = fac.WriteEntireFile(tempFile, zipFileInMemory)
            If (success <> True) Then
                Message = "Fout bij het afronden van zipbestand '" + tempFile + "'\n\n" + zip.LastErrorText
                Response.Write("<script type=""text/javascript"">alert(""" + Message + """);</script")
                Exit Sub
            End If

            Dim HttpResponse As Web.HttpResponse = HttpContext.Current.Response
            Response.Clear()
            Response.ClearHeaders()
            Response.ClearContent()
            'Dim x As String = selectedValues(0)(4)
            '            Dim aFile As String = "ZH310_" + selectedValues(0)(4) + "_" + selectedValues(0)(5) + "_" + selectedValues(0)(7).ToString + Format(selectedValues(0)(6), "00") + "_" + selectedValues(0)(11) + ".zip"
            Response.AddHeader("content-disposition", ("attachment; filename=" + aFile))
            Response.ContentType = "application/zip"
            Response.TransmitFile(tempFile)
        Else
            Message = "Selecteer een regel."
            Response.Write("<script type=""text/javascript"">alert(""" + Message + """);</script")
        End If
    End Sub
End Class