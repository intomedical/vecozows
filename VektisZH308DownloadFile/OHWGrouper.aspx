﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="OHWGrouper.aspx.vb" Inherits="VektisZH308DownloadFile.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 199px;
            font-family:Calibri;
            font-size:small;
        }


        .auto-style2 {
            font-size: small;
        }
        .auto-style4 {
            height: 19px;
        }


        </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="width: 100%;" class="auto-style2">
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:IntomediFin %>" SelectCommand="SELECT        DISBatch.ID AS DISBatchID, DISBatch.Year AS Jaar, DISBatch.Month AS Maand, DISBatch.LocationID AS LocatieID, ClinicLocation.LocationName AS Locatie, DISBatch.CreateDate, DISBatch.CreateUser, DISBatch.OHWRunnummer
FROM            DISBatch INNER JOIN
                         ClinicLocation ON DISBatch.LocationID = ClinicLocation.ID
WHERE        (DISBatch.Type = 'OHW') AND (DISBatch.Deleted = 'F')
ORDER BY Jaar DESC, Maand DESC, LocatieID"></asp:SqlDataSource>
                        <br />
                        Stap 1: Selecteer DISBatchID</td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataKeyNames="DISBatchID" DataSourceID="SqlDataSource1" Width="1029px" AllowPaging="True" Font-Names="Calibri" Font-Size="Small" ShowHeaderWhenEmpty="True">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" />
                                <asp:BoundField DataField="DISBatchID" HeaderText="DISBatchID" InsertVisible="False" ReadOnly="True" SortExpression="DISBatchID" />
                                <asp:BoundField DataField="Jaar" HeaderText="Jaar" SortExpression="Jaar" />
                                <asp:BoundField DataField="Maand" HeaderText="Maand" SortExpression="Maand" />
                                <asp:BoundField DataField="LocatieID" HeaderText="LocatieID" SortExpression="LocatieID" />
                                <asp:BoundField DataField="Locatie" HeaderText="Locatie" SortExpression="Locatie" />
                                <asp:BoundField DataField="CreateDate" HeaderText="CreateDate" SortExpression="CreateDate" />
                                <asp:BoundField DataField="CreateUser" HeaderText="CreateUser" SortExpression="CreateUser" />
                                <asp:BoundField DataField="OHWRunnummer" HeaderText="OHWRunnummer" SortExpression="OHWRunnummer" />
                            </Columns>
                            <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                            <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                            <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#003399" />
                            <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                            <SortedAscendingCellStyle BackColor="#EDF6F6" />
                            <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
                            <SortedDescendingCellStyle BackColor="#D6DFDF" />
                            <SortedDescendingHeaderStyle BackColor="#002876" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Stap 2: Upload OHW Grouper retour bericht</td>
                </tr>
                <tr>
                    <td>
                    <asp:FileUpload ID="FileUpload1" runat="server" style="margin-bottom: 4px" Width="496px" CssClass="auto-style1" />
                &nbsp;</td>
                </tr>
                <tr>
                    <td><asp:Button ID="cmdUploadOHWG" runat="server" Text="Upload OHW Grouper" CssClass="auto-style1" />
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Stap 3: Bekijk retourbericht. Als deze &quot;goed&quot; is dan Create OHW Batch van geselecteerde DIS Batch</td>
                </tr>
                <tr>
                    <td><asp:Button ID="cmdOHWBatch" runat="server" Text="Create OHW Batch" style="height: 26px" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                                Stap 4 Open&nbsp; de regels om de batches te downloaden<br />
                                <asp:Button ID="cmdDownloadZH310" runat="server" CssClass="auto-style1" Text="Open OHW batch regels" />
                                <asp:Button ID="cmdDownloadZH310_Koepel" runat="server" CssClass="auto-style1" Text="Open OHW koepel" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style4">
                        </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
