﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="OHWGrouperKoepel.aspx.vb" Inherits="VektisZH308DownloadFile.OHWGrouperKoepel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">


        .auto-style1 {
            width: 199px;
            font-family:Calibri;
            font-size:small;
        }


        </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
                        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:IntomediFin %>" SelectCommand="SELECT LocationName, Koepel, AGBCode, Period, Bookyear, Count(*) Aantal, SUM(Totaal) AS Totaal
FROM   av_OHWBatchAll
GROUP BY LocationName, Koepel, AGBCode, Period, Bookyear"></asp:SqlDataSource>
        </div>
        <a href="OHWGrouper.aspx">OHWGrouper</a><br />
        <br />
        <br />
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" DataSourceID="SqlDataSource2" AutoGenerateColumns="False">
            <SettingsPager Visible="False" Mode="ShowAllRecords">
            </SettingsPager>
            <Settings HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto" />
            <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
            <Columns>
                <dx:GridViewDataTextColumn FieldName="LocationName" VisibleIndex="0">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Koepel" VisibleIndex="1">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="AGBCode" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Period" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Bookyear" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Aantal" VisibleIndex="5" ReadOnly="True">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Totaal" VisibleIndex="6" ReadOnly="True">
                </dx:GridViewDataTextColumn>
            </Columns>
        </dx:ASPxGridView>
        <p>
                                <asp:Button ID="cmdDownloadZH310" runat="server" CssClass="auto-style1" Text="Download ZH310" />
        </p>
    </form>
</body>
</html>
