﻿Option Strict Off

Imports System.ServiceModel
Imports System.Net
Imports System.IO
Imports System.IO.Compression
Imports Microsoft.VisualBasic.Information
Imports System.Data.SqlClient

'TODO Superclass overal toepassen
'TODO - Thumprint

'TODO vecozocheck

Public Class VSPEDP
    Inherits SuperClass
    'TODO validatieID
    'TODO Resultaatcode
    'TODO Bestandsnaam xxx

    Enum EndpointType
        IndienenDeclaratieV1Soap11 = 1
        DownloadenRetourinformatieV1Soap11 = 2
        IndienenValidatieV1Soap11 = 3
        DownloadenValidatieV1Soap11 = 4
        IndienenRetourinformatieV1Soap11 = 5
    End Enum

    Public Class VecozoCheck
        Inherits SuperClass
        Private mVerstuurDeclaratie As Resultaat.ResultaatcodeType      ' Verstuur declaratie
        Private mVerstuurValidatie As Resultaat.ResultaatcodeType       ' Verstuur validatie
        Private mOphalenDeclaratie As Resultaat.ResultaatcodeType       ' Ophalen declaratie
        Private mOphalenValidatie As Resultaat.ResultaatcodeType        ' Ophalen validatie
        Private mDownloadDeclaratie As Resultaat.ResultaatcodeType      ' Downloadan bericht declaratie
        Private mDownloadRetour As Resultaat.ResultaatcodeType          ' Downloadan bericht declaratie retour
        Private mDownloadValidatieRetour As Resultaat.ResultaatcodeType ' Downloadan bericht validatie retour

        Public Sub New(VecozoBatchID As Long)
            If Not CheckActions(VecozoBatchID) Then
                mVerstuurDeclaratie = Resultaat.ResultaatcodeType.None
                mVerstuurValidatie = Resultaat.ResultaatcodeType.None
                mOphalenDeclaratie = Resultaat.ResultaatcodeType.None
                mOphalenValidatie = Resultaat.ResultaatcodeType.None
                mDownloadDeclaratie = Resultaat.ResultaatcodeType.None
                mDownloadRetour = Resultaat.ResultaatcodeType.None
                mDownloadValidatieRetour = Resultaat.ResultaatcodeType.None
            End If
        End Sub

        Private Function CheckActions(VecozoBatchID As Long) As Boolean
            Dim dbCommand As New SqlCommand
            Dim bDoNotclose As Boolean = False

            Try
                If dbConnection.State = ConnectionState.Closed Then dbConnection.Open() Else bDoNotclose = True
                dbCommand.Connection = dbConnection

                sqlupdate = New System.Data.SqlClient.SqlCommand("sp_get_VecozoBatchActions", dbConnection)
                sqlupdate.CommandType = CommandType.StoredProcedure
                sqlupdate.Parameters.Add("@VecozoBatchID", SqlDbType.BigInt).Value = VecozoBatchID
                sqlupdate.Parameters.Add("@VerstuurDeclaratie", SqlDbType.Int)
                sqlupdate.Parameters("@VerstuurDeclaratie").Direction = ParameterDirection.Output
                sqlupdate.Parameters.Add("@VerstuurValidatie", SqlDbType.Int)
                sqlupdate.Parameters("@VerstuurValidatie").Direction = ParameterDirection.Output
                sqlupdate.Parameters.Add("@OphalenDeclaratie", SqlDbType.Int)
                sqlupdate.Parameters("@OphalenDeclaratie").Direction = ParameterDirection.Output
                sqlupdate.Parameters.Add("@OphalenValidatie", SqlDbType.Int)
                sqlupdate.Parameters("@OphalenValidatie").Direction = ParameterDirection.Output
                sqlupdate.Parameters.Add("@DownloadDeclaratie", SqlDbType.Int)
                sqlupdate.Parameters("@DownloadDeclaratie").Direction = ParameterDirection.Output
                sqlupdate.Parameters.Add("@DownloadRetour", SqlDbType.Int)
                sqlupdate.Parameters("@DownloadRetour").Direction = ParameterDirection.Output
                sqlupdate.Parameters.Add("@DownloadValidatieRetour", SqlDbType.Int)
                sqlupdate.Parameters("@DownloadValidatieRetour").Direction = ParameterDirection.Output
                sqlupdate.ExecuteNonQuery()
                mVerstuurDeclaratie = EvaluateFromSQL(sqlupdate.Parameters("@VerstuurDeclaratie").Value)
                mVerstuurValidatie = EvaluateFromSQL(sqlupdate.Parameters("@VerstuurValidatie").Value)
                mOphalenDeclaratie = EvaluateFromSQL(sqlupdate.Parameters("@OphalenDeclaratie").Value)
                mOphalenValidatie = EvaluateFromSQL(sqlupdate.Parameters("@OphalenValidatie").Value)
                mDownloadDeclaratie = EvaluateFromSQL(sqlupdate.Parameters("@DownloadDeclaratie").Value)
                mDownloadRetour = EvaluateFromSQL(sqlupdate.Parameters("@DownloadRetour").Value)
                mDownloadValidatieRetour = EvaluateFromSQL(sqlupdate.Parameters("@DownloadValidatieRetour").Value)
            Catch e As Exception
                mVerstuurDeclaratie = Resultaat.ResultaatcodeType.I2M_OTH
                mVerstuurValidatie = Resultaat.ResultaatcodeType.I2M_OTH
                mOphalenDeclaratie = Resultaat.ResultaatcodeType.I2M_OTH
                mOphalenValidatie = Resultaat.ResultaatcodeType.I2M_OTH
                mDownloadDeclaratie = Resultaat.ResultaatcodeType.I2M_OTH
                mDownloadRetour = Resultaat.ResultaatcodeType.I2M_OTH
                mDownloadValidatieRetour = Resultaat.ResultaatcodeType.I2M_OTH
            Finally
                If dbConnection.State = ConnectionState.Open And Not bDoNotclose Then dbConnection.Close()
            End Try
        End Function

        Public ReadOnly Property VerstuurDeclaratie As Resultaat.ResultaatcodeType
            Get
                Return mVerstuurDeclaratie
            End Get
        End Property

        Public ReadOnly Property VerstuurValidatie As Resultaat.ResultaatcodeType
        Get
            Return mVerstuurValidatie
        End Get
    End Property

    Public ReadOnly Property OphalenDeclaratie As Resultaat.ResultaatcodeType
        Get
            Return mOphalenDeclaratie
        End Get
    End Property

    Public ReadOnly Property OphalenValidatie As Resultaat.ResultaatcodeType
        Get
            Return mOphalenValidatie
        End Get
    End Property

    Public ReadOnly Property DownloadDeclaratie As Resultaat.ResultaatcodeType
        Get
            Return mDownloadDeclaratie
        End Get
    End Property

    Public ReadOnly Property DownloadRetour As Resultaat.ResultaatcodeType
        Get
            Return mDownloadRetour
        End Get
    End Property

    Public ReadOnly Property DownloadValidatieRetour As Resultaat.ResultaatcodeType
        Get
            Return mDownloadValidatieRetour
        End Get
    End Property

End Class


Public Class Resultaat
        Public Class Melding
            Enum MeldingSoortType
                None = -1
                Vektis = 0
                Vecozo = 1
            End Enum

            Private mMeldingSoort As MeldingSoortType
            Private mCode As String
            Private mOmschrijving As String
            Private mVektisMeldingKenmerkRecord As Integer
            Private mVektisMeldingIdentificatieDetailRecord As Long
            Private mVektisMeldingRegelnummer As Integer
            Private mVektisMeldingRubrieknummer As Integer
            Private mVektisMeldingVecozoBatchLinenummer As Long

            Public Property MeldingSoort() As MeldingSoortType
                Get
                    Return mMeldingSoort
                End Get
                Set(ByVal value As MeldingSoortType)
                    mMeldingSoort = value
                End Set
            End Property

            Public Property Code() As String
                Get
                    Return mCode
                End Get
                Set(ByVal value As String)
                    mCode = value
                End Set
            End Property

            Public Property Omschrijving() As String
                Get
                    Return mOmschrijving
                End Get
                Set(ByVal value As String)
                    mOmschrijving = value
                End Set
            End Property

            Public Property VektisMeldingKenmerkRecord() As Integer
                Get
                    Return mVektisMeldingKenmerkRecord
                End Get
                Set(ByVal value As Integer)
                    mVektisMeldingKenmerkRecord = value
                End Set
            End Property

            Public Property VektisMeldingIdentificatieDetailRecord() As Long
                Get
                    Return mVektisMeldingIdentificatieDetailRecord
                End Get
                Set(ByVal value As Long)
                    mVektisMeldingIdentificatieDetailRecord = value
                End Set
            End Property

            Public Property VektisMeldingRegelnummer() As Integer
                Get
                    Return mVektisMeldingRegelnummer
                End Get
                Set(ByVal value As Integer)
                    mVektisMeldingRegelnummer = value
                End Set
            End Property

            Public Property VektisMeldingRubrieknummer() As Integer
                Get
                    Return mVektisMeldingRubrieknummer
                End Get
                Set(ByVal value As Integer)
                    mVektisMeldingRubrieknummer = value
                End Set
            End Property

            Public Property VektisMeldingVecozoBatchLinenummer As Long
                Get
                    Return mVektisMeldingVecozoBatchLinenummer
                End Get
                Set(value As Long)
                    mVektisMeldingVecozoBatchLinenummer = value
                End Set
            End Property

            Sub New()
                mMeldingSoort = MeldingSoortType.None
            End Sub
        End Class

        Enum StatusType
            None = -1
            Alive = 0
            NotAlive = 1
            Technisch = 2
            Succes = 3
            SuccesMetOpmerking = 4
            Fout = 5
            Waarschuwing = 6
        End Enum

        Enum MeldingBronType
            None = -1
            I2M = 0
            Vektis = 1
            Vecozo = 2
        End Enum

        Enum ResultaatcodeType
            None = -1
            VSPEDP000 = 0
            VSPEDP001 = 1
            VSPEDP999 = 999
            VSPEDP101 = 101
            VSPEDP102 = 102
            VSPEDP103 = 103
            VSPEDP104 = 104
            VSPEDP105 = 105
            VSPEDP106 = 105
            VSPEDP107 = 107
            VSPEDP108 = 108
            VSPEDP109 = 109
            VSPEDP110 = 110
            VSPEDP111 = 111
            VSPEDP112 = 112
            VSPEDP113 = 113
            VSPEDP114 = 114
            VSPEDP115 = 115
            VSPEDP116 = 116
            VSPEDP201 = 201
            VSPEDP301 = 301
            VSPEDP303 = 303
            VSPEDP304 = 304
            VSPEDP305 = 305
            VSPEDP307 = 307
            VSPEDP308 = 308
            VSPEDP310 = 310
            VSPEDP311 = 311
            VSPEDP312 = 312
            VSPEDP313 = 313
            VSPEDP314 = 314
            VSPEDP315 = 315
            VSPEDP316 = 316
            VSPEDP317 = 317
            VSPEDP318 = 318
            VSPEDP319 = 319
            VSPEDP320 = 320
            VSPEDP321 = 321
            VSPEDP322 = 322
            VSPEDP323 = 323
            VSPEDP324 = 324
            VSPEDP325 = 325
            VSPEDP326 = 326
            VSPEDP327 = 327
            VSPEDP329 = 329
            VSPEDP330 = 330
            VSPEDP331 = 331
            VSPEDP332 = 332
            VSPEDP333 = 333
            VSPEDP334 = 334
            VSPEDP335 = 335
            VSPEDP336 = 336
            VSPEDP337 = 337
            VSPEDP338 = 338
            VSPEDP341 = 341
            VSPEDP344 = 344
            VSPEDP346 = 346
            VSPEDP347 = 347
            VSPEDP348 = 348
            VSPEDP350 = 350
            VSPEDP353 = 353
            VSPEDP354 = 354
            VSPEDP356 = 356
            VSPEDP357 = 357
            VSPEDP358 = 358
            VSPEDP359 = 359
            VSPEDP360 = 360
            VSPEDP361 = 361
            VSPEDP362 = 362
            VSPEDP363 = 363
            VSPEDP364 = 364
            VSPEDP365 = 365
            I2M_DEL = 1000
            I2M_VERW = 1001
            I2M_SEND = 1002
            I2M_PROC = 1003
            I2M_VAL = 1004
            I2M_NFOUND = 1005
            I2M_NCON = 1006
            I2M_MSG = 1007
            I2M_OTH = 1008
            I2M_VALG = 1009
            I2M_NVSP = 1010
            I2M_VALID = 1011
            I2M_VALONTV = 1011
            I2M_VALVERPL = 1012
            I2M_VALAFGK = 1013
            I2M_DECLBEST = 1014
            I2M_VFNFOUND = 1015
            I2M_VFERR = 1016
            I2M_DALG = 1017
            I2M_DALID = 1018
            I2M_MODVSPEDP = 1019
            I2M_VRNFOUND = 1020
            I2M_DRNFOUND = 1021
        End Enum

        Private mVecozoBatchID As Long
        Private mResultaatCode As ResultaatcodeType
        Private mResultaatExtra As String
        Private mStatus As StatusType
        Private mMelding As MeldingBronType
        Dim mMeldingen As List(Of Melding)

        Public ReadOnly Property Meldingen() As List(Of Melding)
            Get
                Return mMeldingen
            End Get
        End Property

        Public Property VecozoBatchID As Long
            Get
                Return mVecozoBatchID
            End Get
            Set(value As Long)
                mVecozoBatchID = value
            End Set
        End Property

        Public ReadOnly Property ResultaatCodeText() As String
            Get
                Select Case mResultaatCode
                    Case ResultaatcodeType.I2M_PROC
                        Return "I2M_PROC - Batch is al verwerkt."
                    Case ResultaatcodeType.I2M_DEL
                        Return "I2M_DEL - Batch is verwijderd."
                    Case ResultaatcodeType.I2M_VERW
                        Return "I2M_VERW - Batch in verwerking."
                    Case ResultaatcodeType.I2M_VALG
                        Return "I2M_VALG - Batch niet voor validatie verzonden."
                    Case ResultaatcodeType.I2M_DALG
                        Return "I2M_DALG - Batch niet verzonden."
                    Case ResultaatcodeType.I2M_SEND
                        Return "I2M_SEND - Batch is handmatig verzonden."
                    Case ResultaatcodeType.I2M_NVSP
                        Return "I2M_NVSP - Batch is niet via VSP aangeboden ter verificatie."
                    Case ResultaatcodeType.I2M_VAL
                        Return "I2M_VAL - Batch is al aangeboden ter verificatie."
                    Case ResultaatcodeType.I2M_NFOUND
                        Return "I2M_NFOUND - Batch niet gevonden."
                    Case ResultaatcodeType.I2M_NCON
                        Return "I2M_NCON - Fout bij het maken van de databaseconnectie."
                    Case ResultaatcodeType.I2M_MSG
                        Return "I2M_MSG - Messagestandard van batch wordt niet ondersteund."
                    Case ResultaatcodeType.I2M_VALID
                        Return "I2M_VALID - Vecozo ValidatieID ontbreekt."
                    Case ResultaatcodeType.I2M_DALID
                        Return "I2M_DALID - Vecozo DeclaratieID ontbreekt."
                    Case ResultaatcodeType.I2M_VALONTV
                        Return "I2M_VALONTV - Validatieretour nog niet verwerkt door Vecozo."
                    Case ResultaatcodeType.I2M_VALVERPL
                        Return "I2M_VALVERPL - Validatie voor verzenden verplicht."
                    Case ResultaatcodeType.I2M_VALAFGK
                        Return "I2M_VALAFGK - Validatie afgekeurd."
                    Case ResultaatcodeType.I2M_DECLBEST
                        Return "I2M_DECLBEST - Fout bij maken van declaratiebestand."
                    Case ResultaatcodeType.I2M_VFNFOUND
                        Return "I2M_VFNFOUND - Declaratiebestand verwacht maar niet gevonden."
                    Case ResultaatcodeType.I2M_VFERR
                        Return "I2M_VFERR - Fout bij ophalen van declaratiebestand."
                    Case ResultaatcodeType.I2M_MODVSPEDP
                        Return "I2M_OTH - Module VSPEDP niet actief."
                    Case ResultaatcodeType.I2M_VRNFOUND
                        Return "I2M_VFNFOUND - Validatieretour bestand verwacht maar niet gevonden."
                    Case ResultaatcodeType.I2M_DRNFOUND
                        Return "I2M_VFNFOUND - Declaratieretour bestand verwacht maar niet gevonden."
                    Case ResultaatcodeType.I2M_OTH
                        Return "I2M_OTH - Onbekende fout: "
                    Case ResultaatcodeType.VSPEDP000
                        Return "VSPEDP000 - Niet geauthenticeerd."
                    Case ResultaatcodeType.VSPEDP001
                        Return "VSPEDP001 - Niet geautoriseerd."
                    Case ResultaatcodeType.VSPEDP999
                        Return "VSPEDP999 - Onbekende technische fout."
                    Case ResultaatcodeType.VSPEDP101
                        Return "VSPEDP101 - EI-bestand succesvol ontvangen voor validatie."
                    Case ResultaatcodeType.VSPEDP102
                        Return "VSPEDP102 - Declaratiebestand(en) succesvol ontvangen."
                    Case ResultaatcodeType.VSPEDP103
                        Return "VSPEDP103 - Declaratiebestand(en) succesvol ontvangen door zorgverzekeraar."
                    Case ResultaatcodeType.VSPEDP104
                        Return "VSPEDP104 - PDF succesvol ontvangen."
                    Case ResultaatcodeType.VSPEDP105
                        Return "VSPEDP105 - Retourbestand succesvol ontvangen."
                    Case ResultaatcodeType.VSPEDP106
                        Return "VSPEDP106 - Statuswijzigingsmelding succesvol ontvangen."
                    Case ResultaatcodeType.VSPEDP107
                        Return "VSPEDP107 - Succesvol EI-retourbestand aangeboden. Status EI-retourbestand veranderd naar ""Opgehaald""."
                    Case ResultaatcodeType.VSPEDP108
                        Return "VSPEDP108 - EI-retourbestand succesvol ontvangen."
                    Case ResultaatcodeType.VSPEDP109
                        Return "VSPEDP109 - PDF-bestand succesvol ontvangen."
                    Case ResultaatcodeType.VSPEDP110
                        Return "VSPEDP110 - Lijstopvraag succesvol."
                    Case ResultaatcodeType.VSPEDP111
                        Return "VSPEDP111 - Validatieresultaat op vraag succesvol."
                    Case ResultaatcodeType.VSPEDP112
                        Return "VSPEDP112 - Validatieresultaat succesvol ontvangen door ontvangende partij."
                    Case ResultaatcodeType.VSPEDP113
                        Return "VSPEDP113 - Succesvol PDF-bestand aangeboden. Status PDF-bestand veranderd naar ""Teruggekoppeld aan zorgaanbieder""."
                    Case ResultaatcodeType.VSPEDP114
                        Return "VSPEDP114 - Lijstopvraag succesvol."
                    Case ResultaatcodeType.VSPEDP115
                        Return "VSPEDP115 - Status succesvol opgevraagd van EDP EI-declaratiebestand."
                    Case ResultaatcodeType.VSPEDP116
                        Return "VSPEDP116 - Declaratie is reeds ontvangen door zorgverzekeraar."
                    Case ResultaatcodeType.VSPEDP201
                        Return "VSPEDP201 - Succesvol EI-retourbestand aangeboden echter is het EI-retourbestand inmiddels al door iemand anders opgehaald."
                    Case ResultaatcodeType.VSPEDP301
                        Return "VSPEDP301 - Validatieresultaat niet correct ontvangen door extern systeem."
                    Case ResultaatcodeType.VSPEDP303
                        Return "VSPEDP303 - Het declaratiebestand is groter dan 20MB en kan niet worden verwerkt."
                    Case ResultaatcodeType.VSPEDP304
                        Return "VSPEDP304 - Het aangeleverde bestand is groter dan 20MB en kan niet worden verwerkt."
                    Case ResultaatcodeType.VSPEDP305
                        Return "VSPEDP305 - Declaratiebestand(en) konden niet worden ontvangen door zorgverzekeraar."
                    Case ResultaatcodeType.VSPEDP307
                        Return "VSPEDP307 - Er is geen declaratie gevonden voor het gegeven ID waarvoor een PDF bestand kan worden ingediend."
                    Case ResultaatcodeType.VSPEDP308
                        Return "VSPEDP308 - PDF kon niet worden ontvangen."
                    Case ResultaatcodeType.VSPEDP310
                        Return "VSPEDP310 - Het aangeleverde retourbestand is groter dan 20MB en kan niet worden verwerkt."
                    Case ResultaatcodeType.VSPEDP311
                        Return "VSPEDP311 - Het of één van de retourbestanden is groter dan 300MB en kan niet worden verwerkt."
                    Case ResultaatcodeType.VSPEDP312
                        Return "VSPEDP312 - Opgegeven grootte van het retourbestand komt niet overeen met de gegeven data."
                    Case ResultaatcodeType.VSPEDP313
                        Return "VSPEDP313 - Er is geen declaratie gevonden voor het gegeven ID waarvoor een retourbestand kan worden ingediend."
                    Case ResultaatcodeType.VSPEDP314
                        Return "VSPEDP314 - Statuswijzigingsmelding kon niet worden ontvangen."
                    Case ResultaatcodeType.VSPEDP315
                        Return "VSPEDP315 - Fout: Bestand is inmiddels niet meer beschikbaar."
                    Case ResultaatcodeType.VSPEDP316
                        Return "VSPEDP316 - Fout: Bestand niet opvraagbaar."
                    Case ResultaatcodeType.VSPEDP317
                        Return "VSPEDP317 - EI-retourbestand kon niet worden ontvangen."
                    Case ResultaatcodeType.VSPEDP318
                        Return "VSPEDP318 - PDF-bestand kon niet worden ontvangen."
                    Case ResultaatcodeType.VSPEDP319
                        Return "VSPEDP319 - Er mag of moet maar één EI-retourbestand aanwezig zijn in de aanlevering."
                    Case ResultaatcodeType.VSPEDP320
                        Return "VSPEDP320 - Het aangeleverde ZIP-bestand kan niet worden gelezen."
                    Case ResultaatcodeType.VSPEDP321
                        Return "VSPEDP321 - Het aangeleverde bestand moet een EI-bestand betreffen."
                    Case ResultaatcodeType.VSPEDP322
                        Return "VSPEDP322 - Er mag maar één EI bestand per declaratie worden ingediend"
                    Case ResultaatcodeType.VSPEDP323
                        Return "VSPEDP323 - Er is geen declaratie gevonden waarvoor retourbestanden kunnen worden opgevraagd."
                    Case ResultaatcodeType.VSPEDP324
                        Return "VSPEDP324 - Er is geen retourbestand gevonden voor dit ID dat kan worden gebruik voor de lijstopvraag."
                    Case ResultaatcodeType.VSPEDP325
                        Return "VSPEDP325 - Gegeven PDF bestand heeft niet de correcte extensie."
                    Case ResultaatcodeType.VSPEDP326
                        Return "VSPEDP326 - Er is geen PDF-bestand gevonden voor het gegeven ID."
                    Case ResultaatcodeType.VSPEDP327
                        Return "VSPEDP327 - Opgegeven grootte van het EI-bestand komt niet overeen met de gegeven data."
                    Case ResultaatcodeType.VSPEDP329
                        Return "VSPEDP329 - Er mag maar één EI-bestand ter validatie worden aangeboden."
                    Case ResultaatcodeType.VSPEDP330
                        Return "VSPEDP330 - Validatieresultaat kon niet worden ontvangen."
                    Case ResultaatcodeType.VSPEDP331
                        Return "VSPEDP331 - Met het gegeven validatieID kan geen validatieresultaat worden gevonden."
                    Case ResultaatcodeType.VSPEDP332
                        Return "VSPEDP332 - Het e-mailadres ontbreekt of is onjuist."
                    Case ResultaatcodeType.VSPEDP333
                        Return "VSPEDP333 - De bestandsgrootte is niet gelijk aan de grootte van het bestand."
                    Case ResultaatcodeType.VSPEDP334
                        Return "VSPEDP334 - Het declaratieID ontbreekt of is niet numeriek."
                    Case ResultaatcodeType.VSPEDP335
                        Return "VSPEDP335 - Het volgnummer ontbreekt of is niet numeriek."
                    Case ResultaatcodeType.VSPEDP336
                        Return "VSPEDP336 - Het e-mailadres ontbreekt of is onjuist."
                    Case ResultaatcodeType.VSPEDP337
                        Return "VSPEDP337 - Het declaratieID ontbreekt of is niet numeriek."
                    Case ResultaatcodeType.VSPEDP338
                        Return "VSPEDP338 - De bestandsgrootte is niet gelijk aan de grootte van het bestand."
                    Case ResultaatcodeType.VSPEDP341
                        Return "VSPEDP341 - De StandaardCode wordt niet ondersteund door VSP-EDP."
                    Case ResultaatcodeType.VSPEDP344
                        Return "VSPEDP344 - De combinatie StandaardCode, StandaardVersie en StandaardSubVersie wordt niet ondersteund door VSP-EDP."
                    Case ResultaatcodeType.VSPEDP346
                        Return "VSPEDP346 - Het retourbestandID of declaratieID moet aanwezig zijn. Allebei mogen niet aanwezig zijn. Allebei mogen niet afwezig zijn."
                    Case ResultaatcodeType.VSPEDP347
                        Return "VSPEDP347 - Één van de e-mailadressen is onjuist."
                    Case ResultaatcodeType.VSPEDP348
                        Return "VSPEDP348 - De bestandsgrootte is niet gelijk aan de grootte van het bestand."
                    Case ResultaatcodeType.VSPEDP350
                        Return "VSPEDP350 - In het ZIP-bestand zijn geen declaratiebestanden aangetroffen of meer dan 30 en zal niet worden verwerkt."
                    Case ResultaatcodeType.VSPEDP353
                        Return "VSPEDP353 - De maximale grootte van de aanlevering mag maar 20MB zijn."
                    Case ResultaatcodeType.VSPEDP354
                        Return "VSPEDP354 - Een ZIP-bestand mag maximaal 30 EI-bestanden bevatten."
                    Case ResultaatcodeType.VSPEDP356
                        Return "VSPEDP356 - Status kon niet worden opgevraagd voor EDP EI-declaratiebestand."
                    Case ResultaatcodeType.VSPEDP357
                        Return "VSPEDP357 - De bestandsnaam (van het EI-bestand in de ZIP) mag maximaal 150 karakters bevatten."
                    Case ResultaatcodeType.VSPEDP358
                        Return "VSPEDP358 - De bestandsnaam (van het EI-bestand in de ZIP) mag maximaal 150 karakters bevatten."
                    Case ResultaatcodeType.VSPEDP359
                        Return "VSPEDP359 - De bestandsnaam mag maximaal 150 karakters bevatten."
                    Case ResultaatcodeType.VSPEDP360
                        Return "VSPEDP360 - De bestandsnaam (van het EI-bestand in de ZIP) mag maximaal 150 karakters bevatten."
                    Case ResultaatcodeType.VSPEDP361
                        Return "VSPEDP361 - Er mogen niet meer dan 100 declaratiestatussen worden opgevraagd."
                    Case ResultaatcodeType.VSPEDP362
                        Return "VSPEDP362 - De bestandsnaam (van het EI-bestand in de ZIP) mag alleen cijfers, letters, koppeltekens, liggende streepjes en punten bevatten."
                    Case ResultaatcodeType.VSPEDP363
                        Return "VSPEDP363 - De bestandsnaam (van het EI-bestand in de ZIP) mag alleen cijfers, letters, koppeltekens, liggende streepjes en punten bevatten."
                    Case ResultaatcodeType.VSPEDP364
                        Return "VSPEDP364 - De bestandsnaam mag alleen cijfers, letters, koppeltekens, liggende streepjes en punten bevatten."
                    Case ResultaatcodeType.VSPEDP365
                        Return "VSPEDP365 - De bestandsnaam (van het EI-bestand in de ZIP) mag alleen cijfers, letters, koppeltekens, liggende streepjes en punten bevatten."
                    Case ResultaatcodeType.None
                        Return ""
                End Select
                Return "Onbekende fout"
            End Get
        End Property

        Public Property Status() As StatusType
            Get
                Return mStatus
            End Get
            Set(ByVal value As StatusType)
                mStatus = value
            End Set
        End Property

        Public ReadOnly Property StatusText() As String
            Get
                Select Case mStatus
                    Case StatusType.None
                        Return ""
                    Case StatusType.Alive
                        Return "0 - Alive"
                    Case StatusType.NotAlive
                        Return "1 - NotAlive"
                    Case StatusType.Technisch
                        Return "2 - Technisch"
                    Case StatusType.Succes
                        Return "3 - Success"
                    Case StatusType.SuccesMetOpmerking
                        Return "4 - Succes met opmerking"
                    Case StatusType.Fout
                        Return "5 - Fout"
                    Case StatusType.Fout
                        Return "6 - Waarschuwing"
                    Case Else
                        Return "Onbekende status"
                End Select
            End Get

        End Property

        Public Property MeldingBron() As MeldingBronType
            Get
                Return mMeldingBron
            End Get
            Set(ByVal value As MeldingBronType)
                mMeldingBron = value
            End Set
        End Property

        Public ReadOnly Property MeldingBronText() As String
            Get
                Select Case mMelding
                    Case MeldingBronType.None
                        Return mMelding.ToString + " - " + "None"
                    Case MeldingBronType.Vecozo
                        Return mMelding.ToString + " - " + "Vecozo"
                    Case MeldingBronType.Vektis
                        Return mMelding.ToString + " - " + "Vektis"
                    Case MeldingBronType.I2M
                        Return mMelding.ToString + " - " + "I2M"
                    Case Else
                        Return "Onbekende MeldingBron"
                End Select
            End Get

        End Property

        Public Sub SetResultaatcode(aResultaatCode As String)
            Dim value As Integer = DirectCast([Enum].Parse(GetType(ResultaatcodeType), aResultaatCode), Integer)
            mResultaatCode = value

            If mResultaatCode > 0 And mResultaatCode < 100 Or mResultaatCode = 999 Then
                mStatus = StatusType.Technisch
            ElseIf mResultaatCode >= 100 And mResultaatCode < 200 Then
                mStatus = StatusType.Succes
            ElseIf mResultaatCode >= 200 And mResultaatCode < 300 Then
                mStatus = StatusType.SuccesMetOpmerking
            ElseIf mResultaatCode >= 300 And mResultaatCode < 400 Then
                mStatus = StatusType.Fout
            ElseIf mResultaatCode > 1000 Then
                mStatus = StatusType.Fout
            End If
        End Sub

        Public Property ResultaatCode() As ResultaatcodeType
            Get
                Return mResultaatCode
            End Get
            Set(ByVal value As ResultaatcodeType)
                mResultaatCode = value
            End Set
        End Property

        Public Property ResultaatExtra() As String
            Get
                Return mResultaatExtra
            End Get
            Set(ByVal value As String)
                mResultaatExtra = value
            End Set
        End Property

        Public Overridable Function DebugText() As String
            Dim aMessage As String = ""

            aMessage = aMessage + "VecozoBatchID = " + VecozoBatchID + vbCrLf
            aMessage = aMessage + "MeldingBron = " + MeldingBron + vbCrLf
            aMessage = aMessage + "Status = " + StatusText + vbCrLf
            aMessage = aMessage + "ResultaatCode = " + ResultaatCodeText + vbCrLf
            aMessage = aMessage + "ResultaatExtra = " + ResultaatExtra + vbCrLf

            Return aMessage
        End Function

        Sub New()
            mResultaatCode = ResultaatcodeType.None
            mStatus = StatusType.None
            mMeldingBron = MeldingBronType.None
            mResultaatExtra = ""
        End Sub
    End Class

    Friend mLastResultaat As Resultaat
    Dim mWSaddress As EndpointAddress
    Dim mBinding As BasicHttpBinding
    Dim mClientCredentials As System.ServiceModel.Description.ClientCredentials
    Dim mThumbprintCertificate As String

    Public ReadOnly Property Binding() As BasicHttpBinding
        Get
            Return mBinding
        End Get
    End Property

    Public ReadOnly Property WSaddress() As EndpointAddress
        Get
            Return mWSaddress
        End Get
    End Property

    Public ReadOnly Property ClientCredentials() As System.ServiceModel.Description.ClientCredentials
        Get
            Return mClientCredentials
        End Get
    End Property

    Public ReadOnly Property ThumbprintCertificate As String
        Get
            Return mThumbprintCertificate
        End Get
    End Property

    Public ReadOnly Property LastResultaat As Resultaat
        Get
            Return mLastResultaat
        End Get
    End Property

    Private Function getEndPoint(Endpoint As EndpointType) As String
        Dim aEnvironment As String = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString()
        aEnvironment = "P"
        Select Case Endpoint
            Case EndpointType.IndienenDeclaratieV1Soap11
                Return System.Configuration.ConfigurationManager.AppSettings("IndienenDeclaratieV1Soap11_" & aEnvironment).ToString()
            Case EndpointType.DownloadenRetourinformatieV1Soap11
                Return System.Configuration.ConfigurationManager.AppSettings("DownloadenRetourV1Soap11_" & aEnvironment).ToString()
            Case EndpointType.IndienenValidatieV1Soap11
                Return System.Configuration.ConfigurationManager.AppSettings("IndienenValidatieV1Soap11_" & aEnvironment).ToString()
            Case EndpointType.DownloadenValidatieV1Soap11
                Return System.Configuration.ConfigurationManager.AppSettings("DownloadenValidatieV1Soap11_" & aEnvironment).ToString()
            Case EndpointType.IndienenRetourinformatieV1Soap11
                Return System.Configuration.ConfigurationManager.AppSettings("IndienenRetourV1Soap11_" & aEnvironment).ToString()
            Case Else
                Return ""
        End Select
    End Function

    Public Sub New(Endpoint As EndpointType)
        MyBase.New()
        mLastResultaat = New Resultaat
        Dim mVSEPDIsAlive As wsVSPEDPIsAlive.IsAliveClient
        Try
            'mThumbprintCertificate = "edb5d849a73e033baaa31ae9d20e1ad80dc8abd7" ' ACC
            mThumbprintCertificate = "625A8C485A8A6FF02A7B69259C444B45405175F8" ' PROD
            mWSaddress = New EndpointAddress(getEndPoint(Endpoint))

            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
            mBinding = New BasicHttpBinding()
            mBinding.Security.Mode = BasicHttpSecurityMode.Transport
            mBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate
            mBinding.MaxReceivedMessageSize = 1000000000
            mClientCredentials = New System.ServiceModel.Description.ClientCredentials
            '            mClientCredentials.ClientCertificate.SetCertificate(System.Security.Cryptography.X509Certificates.StoreLocation.CurrentUser, System.Security.Cryptography.X509Certificates.StoreName.TrustedPeople, System.Security.Cryptography.X509Certificates.X509FindType.FindByThumbprint, ThumbprintCertificate)
            mClientCredentials.ClientCertificate.SetCertificate(System.Security.Cryptography.X509Certificates.StoreLocation.CurrentUser, System.Security.Cryptography.X509Certificates.StoreName.My, System.Security.Cryptography.X509Certificates.X509FindType.FindByThumbprint, ThumbprintCertificate)

            mVSEPDIsAlive = New wsVSPEDPIsAlive.IsAliveClient(mBinding, mWSaddress)
            For counter As Integer = mVSEPDIsAlive.ChannelFactory.Endpoint.Behaviors.Count - 1 To 0 Step -1
                If TypeOf mVSEPDIsAlive.ChannelFactory.Endpoint.Behaviors(counter) Is System.ServiceModel.Description.ClientCredentials Then
                    mVSEPDIsAlive.ChannelFactory.Endpoint.Behaviors.RemoveAt(counter)
                    Exit For
                End If
            Next
            mVSEPDIsAlive.ChannelFactory.Endpoint.Behaviors.Add(ClientCredentials)
            mVSEPDIsAlive.Open()
            Dim aliveRequest As New wsVSPEDPIsAlive.IsAliveRequest
            Dim aliveResponse As wsVSPEDPIsAlive.IsAliveResponse = mVSEPDIsAlive.IsAlive(aliveRequest)
            If Not aliveResponse.Resultaat Then
                mLastResultaat.Status = Resultaat.StatusType.NotAlive
            Else
                mLastResultaat = Nothing
            End If
        Catch ex As Exception
            mLastResultaat.MeldingBron = Resultaat.MeldingBronType.I2M
            mLastResultaat.Status = Resultaat.StatusType.Fout
            mLastResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_OTH
            mLastResultaat.ResultaatExtra = ex.Message
        End Try
        If Not mVSEPDIsAlive Is Nothing Then mVSEPDIsAlive = Nothing
    End Sub

    Public Function GetVecozoFile(VecozoBatchID As Long, ByRef VecozoFileID As Long, ByRef Bestandsnaam As String, ByRef Bestand As Byte(), Optional VBMessageStandardID As Integer = -1, Optional Force As Boolean = False) As Resultaat
        Dim dbCommand As New SqlCommand
        Dim dbReader As SqlDataReader
        Dim aFile As String = ""
        Dim aResultaat As New Resultaat
        aResultaat.Status = Resultaat.StatusType.Succes
        Try
            VecozoFileID = -1
            Bestandsnaam = ""
            Bestand = Nothing

            'TODO Check op resultaat van oVektisZH308.GetVektisZH308File en GetVektisDGMFile(VecozoBatchID) fout gaat
            If Force Then
                Dim oVektis As New VektisGenerateFile
                If VBMessageStandardID = 1 Then
                    memStream = oVektis.GetVektisZH308File(VecozoBatchID)
                    Bestandsnaam = "ZH308_TEST" & VecozoBatchID.ToString
                ElseIf VBMessageStandardID = 2 Then
                    memStream = oVektis.GetVektisDG301File(VecozoBatchID)
                    Bestandsnaam = "DG301_TEST" & VecozoBatchID.ToString
                Else
                End If
                oVektis = Nothing
                If memStream Is Nothing Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.MeldingBron = Resultaat.MeldingBronType.I2M
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_DECLBEST
                Else
                    Bestand = memStream.ToArray()
                End If
            Else
                If dbConnection.State = ConnectionState.Closed Then dbConnection.Open()
                dbCommand.Connection = dbConnection

                'Get vecozobatchdetails
                dbCommand = New SqlCommand("SELECT [ID], [File], [Filename] FROM [VecozoFile] WHERE ([VecozoFileTypeID] = 0) AND ([Deleted] = 'F') AND ([VecozoBatchID] = " & VecozoBatchID.ToString & ")", dbConnection)
                dbReader = dbCommand.ExecuteReader()
                If dbReader.Read() Then
                    VecozoFileID = EvaluateFromSQL(dbReader.GetValue(dbReader.GetOrdinal("ID")))
                    Bestandsnaam = EvaluateFromSQL(dbReader.GetValue(dbReader.GetOrdinal("Filename")))
                    aFile = EvaluateFromSQL(dbReader.GetValue(dbReader.GetOrdinal("File")))
                    If aFile <> "" Then
                        Dim x() As Byte = System.Text.Encoding.ASCII.GetBytes(aFile)
                        memStream = New MemoryStream(x)
                        Bestand = memStream
                    Else
                        aResultaat.Status = Resultaat.StatusType.Fout
                        aResultaat.MeldingBron = Resultaat.MeldingBronType.I2M
                        aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_VFERR
                    End If
                Else
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.MeldingBron = Resultaat.MeldingBronType.I2M
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_VFNFOUND
                End If
                dbReader.Close()
            End If
        Catch e As Exception
            aResultaat.Status = Resultaat.StatusType.Fout
            aResultaat.MeldingBron = Resultaat.MeldingBronType.I2M
            aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_OTH
            aResultaat.ResultaatExtra = e.Message
        End Try
        Return aResultaat
    End Function
End Class

Public Class VSPEDPDeclaratieIndienen
    Inherits VSPEDP

    Dim mVSEPDDeclaratieIndienen As wsVSPEDPDeclaratieIndienen.VspEdpIncomingZvlIndienenClient

    Public Class DeclaratieIndienenResultaat
        Inherits VSPEDP.Resultaat

        Private mDeclaratieID As Long
        Private mBestandsnaam As String
        Private mBestand As Byte()
        Private mVecozoFileID As Long

        Public Overrides Function DebugText() As String
            Dim aMessage As String = ""

            aMessage = MyBase.DebugText & "Declaratie = " & mDeclaratieID.ToString & vbCrLf
            aMessage = aMessage & "Bestandsnaam = " & mBestandsnaam & vbCrLf

            Return aMessage
        End Function

        Public Property DeclaratieID() As Long
            Get
                Return mDeclaratieID
            End Get
            Set(ByVal value As Long)
                mDeclaratieID = value
            End Set
        End Property

        Public Property Bestandsnaam() As String
            Get
                Return mBestandsnaam
            End Get
            Set(ByVal value As String)
                mBestandsnaam = value
            End Set
        End Property

        Public Property Bestand As Byte()
            Get
                Return mBestand
            End Get
            Set(value As Byte())
                mBestand = value
            End Set
        End Property

        Public Property VecozoFileID As Long
            Get
                Return mVecozoFileID
            End Get
            Set(value As Long)
                mVecozoFileID = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()

            mVecozoFileID = -1
            mBestand = Nothing
            mBestandsnaam = ""
            mDeclaratieID = -1
        End Sub
    End Class

    Public Sub New()
        MyBase.New(EndpointType.IndienenDeclaratieV1Soap11)
        If mLastResultaat IsNot Nothing Then
            Exit Sub
        End If

        Try
            mVSEPDDeclaratieIndienen = New wsVSPEDPDeclaratieIndienen.VspEdpIncomingZvlIndienenClient(Binding, WSaddress)

            For counter As Integer = mVSEPDDeclaratieIndienen.ChannelFactory.Endpoint.Behaviors.Count - 1 To 0 Step -1
                If TypeOf mVSEPDDeclaratieIndienen.ChannelFactory.Endpoint.Behaviors(counter) Is System.ServiceModel.Description.ClientCredentials Then
                    mVSEPDDeclaratieIndienen.ChannelFactory.Endpoint.Behaviors.RemoveAt(counter)
                    Exit For
                End If
            Next
            mVSEPDDeclaratieIndienen.ChannelFactory.Endpoint.Behaviors.Add(ClientCredentials)
        Catch ex As Exception
            mLastResultaat = New Resultaat
            mLastResultaat.MeldingBron = Resultaat.MeldingBronType.I2M
            mLastResultaat.Status = Resultaat.StatusType.Fout
            mLastResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_OTH
            mLastResultaat.ResultaatExtra = ex.Message
        End Try
    End Sub

    'TODO: ValidatieVerplicht
    Public Function DeclaratieIndienen(VecozoBatchID As Long) As DeclaratieIndienenResultaat
        Dim dbCommand As New SqlCommand
        Dim dbReader As SqlDataReader
        Dim aResultaat As New DeclaratieIndienenResultaat
        Dim Validatieverplicht = True
        Dim VBSendmethodID As Long
        Dim VBMessageStandardID As Long
        Dim VBStatus As Long
        Dim VBDeleted As String
        Dim VBProcessed As String

        Try
            If dbConnection.State = ConnectionState.Closed Then dbConnection.Open()
            dbCommand.Connection = dbConnection

            aResultaat.VecozoBatchID = VecozoBatchID

            'Get vecozobatchdetails
            dbCommand = New SqlCommand("SELECT [Batchnummer], [VecozoBatchSendmethodID], [MessageStandardID], [Status], [Processed], [Deleted] FROM [VecozoBatch] WHERE VecozoBatchID = " & VecozoBatchID.ToString, dbConnection)
            dbReader = dbCommand.ExecuteReader()
            If dbReader.Read() Then
                VBSendmethodID = EvaluateFromSQL(dbReader.GetValue(dbReader.GetOrdinal("VecozoBatchSendmethodID")))
                VBMessageStandardID = EvaluateFromSQL(dbReader.GetValue(dbReader.GetOrdinal("MessageStandardID")))
                VBStatus = EvaluateFromSQL(dbReader.GetValue(dbReader.GetOrdinal("Status")))
                VBDeleted = EvaluateFromSQL(dbReader.GetValue(dbReader.GetOrdinal("Deleted")))
                VBProcessed = EvaluateFromSQL(dbReader.GetValue(dbReader.GetOrdinal("Processed")))

                'check if batch is processed
                If VBProcessed = "T" Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_PROC
                ElseIf Not (VBMessageStandardID = "1" Or VBMessageStandardID = "2") Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_MSG
                ElseIf VBDeleted = "T" Or VBStatus = 6 Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_DEL
                ElseIf VBSendmethodID = 0 Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_SEND
                ElseIf (VBSendmethodID = 1 Or VBSendmethodID = 9) And VBStatus = 1 And Validatieverplicht Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_VALVERPL
                ElseIf VBStatus >= 2 And VBStatus <= 6 Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_VERW
                ElseIf VBStatus = 8 Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_VALAFGK
                ElseIf VBStatus = 9 Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_VAL
                End If
            Else
                aResultaat.Status = Resultaat.StatusType.Fout
                aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_NFOUND
            End If
            dbReader.Close()
            If aResultaat.Status <> Resultaat.StatusType.None Then
                aResultaat.MeldingBron = Resultaat.MeldingBronType.I2M
                Return aResultaat
            End If
            Dim VecozoFileResultaat As Resultaat = GetVecozoFile(VecozoBatchID, aResultaat.VecozoFileID, aResultaat.Bestandsnaam, aResultaat.Bestand, VBMessageStandardID, VBStatus <> 1)
            If VecozoFileResultaat.Status <> Resultaat.StatusType.Succes Then
                aResultaat.Status = VecozoFileResultaat.Status
                aResultaat.MeldingBron = VecozoFileResultaat.MeldingBron
                aResultaat.ResultaatCode = VecozoFileResultaat.ResultaatCode
                aResultaat.ResultaatExtra = VecozoFileResultaat.ResultaatExtra
                Return aResultaat
            End If

            Dim request As New wsVSPEDPDeclaratieIndienen.IndienenRequest
            request.Declaratie = New wsVSPEDPDeclaratieIndienen.Declaratie
            request.Declaratie.ReferentieZorgaanbieder = ""
            request.Declaratie.IndienerEmailadres = "adnan.yokaribas@mcvnederland.nl"
            request.Declaratie.EmailNotificaties = New wsVSPEDPDeclaratieIndienen.EmailNotificaties
            request.Declaratie.EmailNotificaties.IndicatieAfkeuringResultaat = True
            request.Declaratie.EmailNotificaties.IndicatieAfkeuringResultaat = False
            request.Declaratie.DeclaratieBestand = New wsVSPEDPDeclaratieIndienen.Bestand
            request.Declaratie.DeclaratieBestand.Data = aResultaat.Bestand.ToArray
            request.Declaratie.DeclaratieBestand.Bestandsnaam = aResultaat.Bestandsnaam
            request.Declaratie.DeclaratieBestand.Bestandsgrootte = aResultaat.Bestand.Length
            mVSEPDDeclaratieIndienen.Open()

            Dim respons As wsVSPEDPDeclaratieIndienen.IndienenResponse = mVSEPDDeclaratieIndienen.Indienen(request)
            If Not respons Is Nothing Then
                If Not respons.Resultaten Is Nothing Then
                    Dim aIndienResultaat As wsVSPEDPDeclaratieIndienen.DeclaratieResultaat = respons.Resultaten(0)
                    aResultaat.SetResultaatcode(aIndienResultaat.Resultaatcode.ToString)
                    If aResultaat.Status = Resultaat.StatusType.Succes Or Resultaat.StatusType.SuccesMetOpmerking Then
                        aResultaat.DeclaratieID = aIndienResultaat.DeclaratieId
                    Else
                        'TODO
                    End If
                Else
                    'TODO
                End If
            Else
                'TODO
            End If

        Catch e As Exception
            aResultaat.Status = Resultaat.StatusType.Fout
            aResultaat.MeldingBron = Resultaat.MeldingBronType.I2M
            aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_OTH
            aResultaat.ResultaatExtra = e.Message
        Finally
            If dbConnection.State = ConnectionState.Open Then dbConnection.Close()
        End Try
        Return aResultaat
    End Function

    'TODO deze
    Public Function DeclaratieIndienen(x As Byte(), Bestandsnaam As String) As DeclaratieIndienenResultaat
        Dim aResultaat As New DeclaratieIndienenResultaat

        Try
            Dim request As New wsVSPEDPDeclaratieIndienen.IndienenRequest
            request.Declaratie = New wsVSPEDPDeclaratieIndienen.Declaratie
            request.Declaratie.ReferentieZorgaanbieder = ""
            request.Declaratie.IndienerEmailadres = "adnan.yokaribas@mcdeveluwe.nl"
            request.Declaratie.EmailNotificaties = New wsVSPEDPDeclaratieIndienen.EmailNotificaties
            request.Declaratie.EmailNotificaties.IndicatieAfkeuringResultaat = True
            request.Declaratie.EmailNotificaties.IndicatieAfkeuringResultaat = False
            request.Declaratie.DeclaratieBestand = New wsVSPEDPDeclaratieIndienen.Bestand
            request.Declaratie.DeclaratieBestand.Data = x.ToArray
            request.Declaratie.DeclaratieBestand.Bestandsnaam = Bestandsnaam
            request.Declaratie.DeclaratieBestand.Bestandsgrootte = x.Length
            mVSEPDDeclaratieIndienen.Open()

            'Dim serxml = New System.Xml.Serialization.XmlSerializer(request.GetType())
            'Dim MS As MemoryStream = New MemoryStream()
            'serxml.Serialize(MS, request)
            'Dim xml As String = Encoding.ASCII.GetString(MS.ToArray())
            Dim respons As wsVSPEDPDeclaratieIndienen.IndienenResponse = mVSEPDDeclaratieIndienen.Indienen(request)
            If Not respons Is Nothing Then
                If Not respons.Resultaten Is Nothing Then
                    Dim aIndienResultaat As wsVSPEDPDeclaratieIndienen.DeclaratieResultaat = respons.Resultaten(0)
                    aResultaat.SetResultaatcode(aIndienResultaat.Resultaatcode.ToString)
                    aResultaat.DeclaratieID = aIndienResultaat.DeclaratieId
                    aResultaat.Bestandsnaam = aIndienResultaat.Bestandsnaam
                End If
            End If
        Catch ex As Exception
            aResultaat = Nothing
        End Try

        Return aResultaat
    End Function

    Public Function ProcessDeclaratieIndienen(aDeclaratieIndienenResultaat As DeclaratieIndienenResultaat) As Resultaat
        Dim dbCommand As New SqlCommand
        Dim dbReader As SqlDataReader
        Dim aResultaat As New Resultaat

        Try
            If dbConnection.State = ConnectionState.Closed Then dbConnection.Open()
            dbCommand.Connection = dbConnection

            'dbCommand = New SqlCommand("SELECT [VecozoBatchID], [LocationID], [Batchdate], [VecozoBatchSendmethodID], [MessageStandardID], [Status], [VecozoDeclaratieID] , [Period], [Bookyear], [Senddate], [Returndate], [Remark], [Processed], [ReturnFileURL], [ValidateDate], [ValidateReturnDate], [LocationAGB], [VecozoValidatieID] FROM [VecozoBatch] WHERE VecozoBatchID = " & aDeclaratieIndienenResultaat.VecozoBatchID.ToString, dbConnection)
            dbCommand = New SqlCommand("SELECT [VecozoBatchID], [LocationID], [Batchdate], [MessageStandardID], [Period], [Bookyear], [Remark], [Processed], [LocationAGB], [ValidateDate], [ValidateReturnDate], [VecozoValidatieID] FROM [VecozoBatch] WHERE VecozoBatchID = " & aDeclaratieIndienenResultaat.VecozoBatchID.ToString, dbConnection)
            dbReader = dbCommand.ExecuteReader()
            If dbReader.Read() Then
                Dim sqlupdate As System.Data.SqlClient.SqlCommand
                sqlupdate = New System.Data.SqlClient.SqlCommand("sp_update_VecozoBatch", dbConnection)
                sqlupdate.CommandType = CommandType.StoredProcedure
                sqlupdate.Parameters.Add("@VecozoBatchID", SqlDbType.BigInt).Value = aDeclaratieIndienenResultaat.VecozoBatchID
                sqlupdate.Parameters.Add("@LocationID", SqlDbType.BigInt).Value = dbReader.GetValue(dbReader.GetOrdinal("LocationID"))
                sqlupdate.Parameters.Add("@Batchdate", SqlDbType.Date).Value = dbReader.GetValue(dbReader.GetOrdinal("Batchdate"))
                sqlupdate.Parameters.Add("@VecozoBatchSendmethodID", SqlDbType.BigInt).Value = 1
                sqlupdate.Parameters.Add("@MessageStandardID", SqlDbType.BigInt).Value = dbReader.GetValue(dbReader.GetOrdinal("MessageStandardID"))
                sqlupdate.Parameters.Add("@Status", SqlDbType.BigInt).Value = 2
                sqlupdate.Parameters.Add("@VecozoDeclaratieID", SqlDbType.NVarChar, 50).Value = aDeclaratieIndienenResultaat.DeclaratieID
                sqlupdate.Parameters.Add("@Period", SqlDbType.BigInt).Value = dbReader.GetValue(dbReader.GetOrdinal("Period"))
                sqlupdate.Parameters.Add("@Bookyear", SqlDbType.BigInt).Value = dbReader.GetValue(dbReader.GetOrdinal("Bookyear"))
                sqlupdate.Parameters.Add("@Senddate", SqlDbType.Date).Value = Date.Now
                sqlupdate.Parameters.Add("@Returndate", SqlDbType.Date).Value = DBNull.Value
                sqlupdate.Parameters.Add("@Remarks", SqlDbType.NVarChar, -1).Value = dbReader.GetValue(dbReader.GetOrdinal("Remark"))
                sqlupdate.Parameters.Add("@Processed", SqlDbType.NVarChar, 1).Value = dbReader.GetValue(dbReader.GetOrdinal("Processed"))
                sqlupdate.Parameters.Add("@ReturnFileURL", SqlDbType.NVarChar, -1).Value = DBNull.Value
                sqlupdate.Parameters.Add("@UpdateUserID", SqlDbType.NVarChar, 8).Value = "YOKARIBA"
                sqlupdate.Parameters.Add("@ValidateDate", SqlDbType.Date).Value = dbReader.GetValue(dbReader.GetOrdinal("ValidateDate"))
                sqlupdate.Parameters.Add("@ValidateReturnDate", SqlDbType.Date).Value = dbReader.GetValue(dbReader.GetOrdinal("ValidateReturnDate"))
                sqlupdate.Parameters.Add("@LocationAGB", SqlDbType.NVarChar, 8).Value = dbReader.GetValue(dbReader.GetOrdinal("LocationAGB"))
                sqlupdate.Parameters.Add("@VecozoValidatieID", SqlDbType.NVarChar, 50).Value = dbReader.GetValue(dbReader.GetOrdinal("VecozoValidatieID"))
                rows = sqlupdate.ExecuteNonQuery()

                If aDeclaratieIndienenResultaat.VecozoFileID = -1 Then
                    sqlupdate = New System.Data.SqlClient.SqlCommand("sp_add_VecozoFile", dbConnection)
                    sqlupdate.CommandType = CommandType.StoredProcedure
                    sqlupdate.Parameters.Add("@VecozoBatchID", SqlDbType.BigInt).Value = aDeclaratieIndienenResultaat.VecozoBatchID
                    sqlupdate.Parameters.Add("@VecozoFileTypeID", SqlDbType.BigInt).Value = 0
                    sqlupdate.Parameters.Add("@Filename", SqlDbType.NVarChar, 200).Value = aDeclaratieIndienenResultaat.Bestandsnaam
                    sqlupdate.Parameters.Add("@Filedate", SqlDbType.DateTime).Value = DateTime.Now
                    sqlupdate.Parameters.Add("@File", SqlDbType.NVarChar, -1).Value = System.Text.Encoding.ASCII.GetString(aDeclaratieIndienenResultaat.Bestand)
                    sqlupdate.Parameters.Add("@FileID", SqlDbType.BigInt).Value = DBNull.Value
                    sqlupdate.Parameters.Add("@Remark", SqlDbType.NVarChar, -1).Value = DBNull.Value
                    sqlupdate.Parameters.Add("@UpdateUserID", SqlDbType.NVarChar, 8).Value = "YOKARIBA"
                    sqlupdate.Parameters.Add("@VecozoFileID", SqlDbType.BigInt)
                    sqlupdate.Parameters("@VecozoFileID").Direction = ParameterDirection.Output
                    rows = sqlupdate.ExecuteNonQuery()
                End If

                sqlupdate = New System.Data.SqlClient.SqlCommand("sp_delete_VecozoNotification", dbConnection)
                sqlupdate.CommandType = CommandType.StoredProcedure
                sqlupdate.Parameters.Add("@VecozoBatchID", SqlDbType.BigInt).Value = aDeclaratieIndienenResultaat.VecozoBatchID
                sqlupdate.Parameters.Add("@VecozoFileTypeID", SqlDbType.BigInt).Value = 1
                sqlupdate.Parameters.Add("@UpdateUserID", SqlDbType.NVarChar, 8).Value = "YOKARIBA"
                rows = sqlupdate.ExecuteNonQuery()

                aResultaat.Status = Resultaat.StatusType.Succes
            Else
                aResultaat.Status = Resultaat.StatusType.Fout
                aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_NFOUND
            End If
            dbReader.Close()
        Catch e As Exception
            aResultaat.Status = Resultaat.StatusType.Fout
            aResultaat.MeldingBron = Resultaat.MeldingBronType.I2M
            aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_OTH
            aResultaat.ResultaatExtra = e.Message
        Finally
            If dbConnection.State = ConnectionState.Open Then dbConnection.Close()
        End Try

        Return aResultaat
    End Function


    Public Sub Dispose()
        Try
            mVSEPDDeclaratieIndienen.Close()
        Finally
            mVSEPDDeclaratieIndienen = Nothing
        End Try
    End Sub
End Class

Public Class VSPEDPValidatieIndienen
    Inherits VSPEDP

    Dim mVSEPDValidatieIndienen As wsVSPEDPValidatieIndienen.VspEdpIncomingIndienenValidatieClient

    Public Class ValidatieIndienenResultaat
        Inherits VSPEDP.Resultaat

        Private mValidatieID As Long
        Private mBestandsnaam As String
        Private mBestand As Byte()


        Public Overrides Function DebugText() As String
            Dim aMessage As String = ""

            aMessage = MyBase.DebugText & "ValidatieID = " & mValidatieID.ToString & vbCrLf
            aMessage = aMessage & "Bestandsnaam = " & mBestandsnaam & vbCrLf

            Return aMessage
        End Function

        Public Property ValidatieID() As Long
            Get
                Return mValidatieID
            End Get
            Set(ByVal value As Long)
                mValidatieID = value
            End Set
        End Property

        Public Property Bestandsnaam() As String
            Get
                Return mBestandsnaam
            End Get
            Set(ByVal value As String)
                mBestandsnaam = value
            End Set
        End Property

        Public Property Bestand As Byte()
            Get
                Return mBestand
            End Get
            Set(value As Byte())
                mBestand = value
            End Set
        End Property
    End Class

    ' TODO Error afhandeling
    Public Sub New()
        MyBase.New(EndpointType.IndienenValidatieV1Soap11)
        If mLastResultaat Is Nothing Then
            Exit Sub
        End If

        Try
            mVSEPDValidatieIndienen = New wsVSPEDPValidatieIndienen.VspEdpIncomingIndienenValidatieClient(Binding, WSaddress)

            For counter As Integer = mVSEPDValidatieIndienen.ChannelFactory.Endpoint.Behaviors.Count - 1 To 0 Step -1
                If TypeOf mVSEPDValidatieIndienen.ChannelFactory.Endpoint.Behaviors(counter) Is System.ServiceModel.Description.ClientCredentials Then
                    mVSEPDValidatieIndienen.ChannelFactory.Endpoint.Behaviors.RemoveAt(counter)
                    Exit For
                End If
            Next
            mVSEPDValidatieIndienen.ChannelFactory.Endpoint.Behaviors.Add(ClientCredentials)

        Catch ex As Exception
            mLastResultaat = New Resultaat
            mLastResultaat.MeldingBron = Resultaat.MeldingBronType.I2M
            mLastResultaat.Status = Resultaat.StatusType.Fout
            mLastResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_OTH
            mLastResultaat.ResultaatExtra = ex.Message
        End Try
    End Sub

    'TODO - bestandsnaam uit instellingen formaat
    'TODO - Kenmerk
    'TODO - Email adressen
    'TODO - Error in Catch
    Public Function ValidatieIndienen(VecozoBatchID As Long) As ValidatieIndienenResultaat
        Dim dbConnection As SqlConnection
        Dim dbCommand As New SqlCommand
        Dim dbReader As SqlDataReader
        Dim aResultaat As New ValidatieIndienenResultaat

        Dim VBSendmethodID As Long
        Dim VBMessageStandardID As Long
        Dim VBStatus As Long
        Dim VBDeleted As String
        Dim VBProcessed As String

        Try
            If ConfigurationManager.ConnectionStrings("IntomediFin") Is Nothing Then
                aResultaat.Status = Resultaat.StatusType.Fout
                aResultaat.MeldingBron = Resultaat.MeldingBronType.I2M
                aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_NCON
                Return aResultaat
            End If
            dbConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("IntomediFin").ToString())
            dbConnection.Open()

            dbCommand.Connection = dbConnection

            'Get vecozobatchdetails
            dbCommand = New SqlCommand("SELECT [Batchnummer], [VecozoBatchSendmethodID], [MessageStandardID], [Status], [Processed], [Deleted] FROM [VecozoBatch] WHERE VecozoBatchID = " & VecozoBatchID.ToString, dbConnection)
            dbReader = dbCommand.ExecuteReader()
            If dbReader.Read() Then
                VBSendmethodID = EvaluateFromSQL(dbReader.GetValue(dbReader.GetOrdinal("VecozoBatchSendmethodID")))
                VBMessageStandardID = EvaluateFromSQL(dbReader.GetValue(dbReader.GetOrdinal("MessageStandardID")))
                VBStatus = EvaluateFromSQL(dbReader.GetValue(dbReader.GetOrdinal("Status")))
                VBDeleted = EvaluateFromSQL(dbReader.GetValue(dbReader.GetOrdinal("Deleted")))
                VBProcessed = EvaluateFromSQL(dbReader.GetValue(dbReader.GetOrdinal("Processed")))

                'check if batch is processed
                If VBProcessed = "T" Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_PROC
                ElseIf Not (VBMessageStandardID = "1" Or VBMessageStandardID = "2") Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_MSG
                ElseIf VBDeleted = "T" Or VBStatus = 6 Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_DEL
                ElseIf VBSendmethodID = 0 Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_SEND
                ElseIf VBStatus >= 2 And VBStatus <= 6 Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_VERW
                ElseIf VBStatus = 9 Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_VAL
                End If
            Else
                aResultaat.Status = Resultaat.StatusType.Fout
                aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_NFOUND
            End If
            dbReader.Close()
            If aResultaat.Status <> Resultaat.StatusType.None Then
                aResultaat.MeldingBron = Resultaat.MeldingBronType.I2M
                Return aResultaat
            End If
            Dim memStream As MemoryStream

            Dim Bestandsnaam As String = ""
            Dim VecozoFileID As Long = -1
            Dim Bestand As Byte() = Nothing

            Dim VecozoFileResultaat As Resultaat = GetVecozoFile(VecozoBatchID, VecozoFileID, Bestandsnaam, Bestand, VBMessageStandardID, True)
            If VecozoFileResultaat.Status <> Resultaat.StatusType.Succes Then
                aResultaat.Status = VecozoFileResultaat.Status
                aResultaat.MeldingBron = VecozoFileResultaat.MeldingBron
                aResultaat.ResultaatCode = VecozoFileResultaat.ResultaatCode
                aResultaat.ResultaatExtra = VecozoFileResultaat.ResultaatExtra
                Return aResultaat
            End If

            Dim request As New wsVSPEDPValidatieIndienen.ValiderenEIBestandRequest
            request.Validatie = New wsVSPEDPValidatieIndienen.Validatie
            request.Validatie.Referentie = ""
            request.Validatie.IndienerEmailadressen = New wsVSPEDPValidatieIndienen.ArrayOfEmail
            request.Validatie.IndienerEmailadressen.Add("adnan.yokaribas@mcvnederland.nl")
            request.Validatie.Bestand = New wsVSPEDPValidatieIndienen.Bestand
            request.Validatie.Bestand.Data = Bestand
            request.Validatie.Bestand.Bestandsnaam = "ZH308_TEST" & VecozoBatchID.ToString
            request.Validatie.Bestand.Bestandsgrootte = Bestand.Length
            mVSEPDValidatieIndienen.Open()

            Dim respons As wsVSPEDPValidatieIndienen.ValiderenEIBestandResponse = mVSEPDValidatieIndienen.ValiderenEIBestand(request)
            If Not respons Is Nothing Then
                If Not respons.Resultaten Is Nothing Then
                    Dim aIndienResultaat As wsVSPEDPValidatieIndienen.IndienenValidatieresultaat = respons.Resultaten(0)
                    aResultaat.SetResultaatcode(aIndienResultaat.Resultaatcode.ToString)
                    If aResultaat.Status = Resultaat.StatusType.Succes Or Resultaat.StatusType.SuccesMetOpmerking Then
                        aResultaat.VecozoBatchID = VecozoBatchID
                        aResultaat.Bestand = Bestand
                        aResultaat.ValidatieID = aIndienResultaat.ValidatieId
                        aResultaat.Bestandsnaam = aIndienResultaat.Bestandsnaam
                    Else
                        'TODO
                    End If
                Else
                    'TODO
                End If
            Else
                'TODO
            End If

        Catch e As Exception
            aResultaat.Status = Resultaat.StatusType.Fout
            aResultaat.MeldingBron = Resultaat.MeldingBronType.I2M
            aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_OTH
            aResultaat.ResultaatExtra = e.Message
        End Try
        Return aResultaat
    End Function

    'TODO Connection close en misschien alle objecten
    'TODO Username

    Public Function ProcessValidatieIndienen(aValidatieIndienenResultaat As ValidatieIndienenResultaat) As Resultaat
        Dim dbConnection As SqlConnection
        Dim dbCommand As New SqlCommand
        Dim dbReader As SqlDataReader
        Dim aResultaat As New Resultaat

        Try
            If ConfigurationManager.ConnectionStrings("IntomediFin") Is Nothing Then
                aResultaat.MeldingBron = Resultaat.MeldingBronType.I2M
                aResultaat.Status = Resultaat.StatusType.Fout
                aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_NCON
                Return aResultaat
            End If
            dbConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("IntomediFin").ToString())
            dbConnection.Open()

            'dbCommand = New SqlCommand("SELECT [VecozoBatchID], [LocationID], [Batchdate], [VecozoBatchSendmethodID], [MessageStandardID], [Status], [VecozoDeclaratieID] , [Period], [Bookyear], [Senddate], [Returndate], [Remark], [Processed], [ReturnFileURL], [ValidateDate], [ValidateReturnDate], [LocationAGB], [VecozoValidatieID] FROM [VecozoBatch] WHERE VecozoBatchID = " & aValidatieIndienenResultaat.VecozoBatchID.ToString, dbConnection)
            dbCommand = New SqlCommand("SELECT [VecozoBatchID], [LocationID], [Batchdate], [MessageStandardID], [Period], [Bookyear], [Remark], [Processed], [LocationAGB] FROM [VecozoBatch] WHERE VecozoBatchID = " & aValidatieIndienenResultaat.VecozoBatchID.ToString, dbConnection)
            dbReader = dbCommand.ExecuteReader()
            If dbReader.Read() Then
                Dim sqlupdate As System.Data.SqlClient.SqlCommand
                sqlupdate = New System.Data.SqlClient.SqlCommand("sp_update_VecozoBatch", dbConnection)
                sqlupdate.CommandType = CommandType.StoredProcedure
                sqlupdate.Parameters.Add("@VecozoBatchID", SqlDbType.BigInt).Value = aValidatieIndienenResultaat.VecozoBatchID
                sqlupdate.Parameters.Add("@LocationID", SqlDbType.BigInt).Value = dbReader.GetValue(dbReader.GetOrdinal("LocationID"))
                sqlupdate.Parameters.Add("@Batchdate", SqlDbType.Date).Value = dbReader.GetValue(dbReader.GetOrdinal("Batchdate"))
                sqlupdate.Parameters.Add("@VecozoBatchSendmethodID", SqlDbType.BigInt).Value = 1
                sqlupdate.Parameters.Add("@MessageStandardID", SqlDbType.BigInt).Value = dbReader.GetValue(dbReader.GetOrdinal("MessageStandardID"))
                sqlupdate.Parameters.Add("@Status", SqlDbType.BigInt).Value = 9
                sqlupdate.Parameters.Add("@VecozoDeclaratieID", SqlDbType.NVarChar, 50).Value = DBNull.Value
                sqlupdate.Parameters.Add("@Period", SqlDbType.BigInt).Value = dbReader.GetValue(dbReader.GetOrdinal("Period"))
                sqlupdate.Parameters.Add("@Bookyear", SqlDbType.BigInt).Value = dbReader.GetValue(dbReader.GetOrdinal("Bookyear"))
                sqlupdate.Parameters.Add("@Senddate", SqlDbType.Date).Value = DBNull.Value
                sqlupdate.Parameters.Add("@Returndate", SqlDbType.Date).Value = DBNull.Value
                sqlupdate.Parameters.Add("@Remarks", SqlDbType.NVarChar, -1).Value = dbReader.GetValue(dbReader.GetOrdinal("Remark"))
                sqlupdate.Parameters.Add("@Processed", SqlDbType.NVarChar, 1).Value = dbReader.GetValue(dbReader.GetOrdinal("Processed"))
                sqlupdate.Parameters.Add("@ReturnFileURL", SqlDbType.NVarChar, -1).Value = DBNull.Value
                sqlupdate.Parameters.Add("@UpdateUserID", SqlDbType.NVarChar, 8).Value = "YOKARIBA"
                sqlupdate.Parameters.Add("@ValidateDate", SqlDbType.Date).Value = Date.Now
                sqlupdate.Parameters.Add("@ValidateReturnDate", SqlDbType.Date).Value = DBNull.Value
                sqlupdate.Parameters.Add("@LocationAGB", SqlDbType.NVarChar, 8).Value = dbReader.GetValue(dbReader.GetOrdinal("LocationAGB"))
                sqlupdate.Parameters.Add("@VecozoValidatieID", SqlDbType.NVarChar, 50).Value = aValidatieIndienenResultaat.ValidatieID
                rows = sqlupdate.ExecuteNonQuery()

                sqlupdate = New System.Data.SqlClient.SqlCommand("sp_add_VecozoFile", dbConnection)
                sqlupdate.CommandType = CommandType.StoredProcedure
                sqlupdate.Parameters.Add("@VecozoBatchID", SqlDbType.BigInt).Value = aValidatieIndienenResultaat.VecozoBatchID
                sqlupdate.Parameters.Add("@VecozoFileTypeID", SqlDbType.BigInt).Value = 0
                sqlupdate.Parameters.Add("@Filename", SqlDbType.NVarChar, 200).Value = "ZH308_" & aValidatieIndienenResultaat.VecozoBatchID.ToString
                sqlupdate.Parameters.Add("@Filedate", SqlDbType.DateTime).Value = DateTime.Now
                sqlupdate.Parameters.Add("@File", SqlDbType.NVarChar, -1).Value = System.Text.Encoding.ASCII.GetString(aValidatieIndienenResultaat.Bestand)
                sqlupdate.Parameters.Add("@FileID", SqlDbType.BigInt).Value = DBNull.Value
                sqlupdate.Parameters.Add("@Remark", SqlDbType.NVarChar, -1).Value = DBNull.Value
                sqlupdate.Parameters.Add("@UpdateUserID", SqlDbType.NVarChar, 8).Value = "YOKARIBA"
                sqlupdate.Parameters.Add("@VecozoFileID", SqlDbType.BigInt)
                sqlupdate.Parameters("@VecozoFileID").Direction = ParameterDirection.Output
                rows = sqlupdate.ExecuteNonQuery()

                sqlupdate = New System.Data.SqlClient.SqlCommand("sp_delete_VecozoFileByType", dbConnection)
                sqlupdate.CommandType = CommandType.StoredProcedure
                sqlupdate.Parameters.Add("@VecozoBatchID", SqlDbType.BigInt).Value = aValidatieIndienenResultaat.VecozoBatchID
                sqlupdate.Parameters.Add("@VecozoFileTypeID", SqlDbType.BigInt).Value = 1
                sqlupdate.Parameters.Add("@UpdateUserID", SqlDbType.NVarChar, 8).Value = "YOKARIBA"
                rows = sqlupdate.ExecuteNonQuery()

                sqlupdate = New System.Data.SqlClient.SqlCommand("sp_delete_VecozoFileByType", dbConnection)
                sqlupdate.CommandType = CommandType.StoredProcedure
                sqlupdate.Parameters.Add("@VecozoBatchID", SqlDbType.BigInt).Value = aValidatieIndienenResultaat.VecozoBatchID
                sqlupdate.Parameters.Add("@VecozoFileTypeID", SqlDbType.BigInt).Value = 2
                sqlupdate.Parameters.Add("@UpdateUserID", SqlDbType.NVarChar, 8).Value = "YOKARIBA"
                rows = sqlupdate.ExecuteNonQuery()

                'Verwijder Meldingen 
                sqlupdate = New System.Data.SqlClient.SqlCommand("sp_delete_VecozoNotification", dbConnection)
                sqlupdate.CommandType = CommandType.StoredProcedure
                sqlupdate.Parameters.Add("@VecozoBatchID", SqlDbType.BigInt).Value = aValidatieIndienenResultaat.VecozoBatchID
                sqlupdate.Parameters.Add("@VecozoFileTypeID", SqlDbType.BigInt).Value = 1
                sqlupdate.Parameters.Add("@UpdateUserID", SqlDbType.NVarChar, 8).Value = "YOKARIBA"
                rows = sqlupdate.ExecuteNonQuery()

                'Verwijder Meldingen 
                sqlupdate = New System.Data.SqlClient.SqlCommand("sp_delete_VecozoNotification", dbConnection)
                sqlupdate.CommandType = CommandType.StoredProcedure
                sqlupdate.Parameters.Add("@VecozoBatchID", SqlDbType.BigInt).Value = aValidatieIndienenResultaat.VecozoBatchID
                sqlupdate.Parameters.Add("@VecozoFileTypeID", SqlDbType.BigInt).Value = 2
                sqlupdate.Parameters.Add("@UpdateUserID", SqlDbType.NVarChar, 8).Value = "YOKARIBA"
                rows = sqlupdate.ExecuteNonQuery()
            Else
                aResultaat.Status = Resultaat.StatusType.Fout
                aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_NFOUND
                Return aResultaat
            End If
        Catch e As Exception
            aResultaat.Status = Resultaat.StatusType.Fout
            aResultaat.MeldingBron = Resultaat.MeldingBronType.I2M
            aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_OTH
            aResultaat.ResultaatExtra = e.Message
            Return aResultaat
        End Try

        aResultaat.Status = Resultaat.StatusType.Succes
        Return aResultaat
    End Function

    Public Sub Dispose()
        Try
            mVSEPDValidatieIndienen.Close()
        Finally
            mVSEPDValidatieIndienen = Nothing
        End Try
    End Sub
End Class

Public Class VSEDPValidatieDownloaden
    Inherits VSPEDP

    Dim mVSEPDValidatieDownloaden As wsVSPEDPValidatieDownloaden.VspEdpIncomingDownloadenValidatiePortClient

    Public Class ValidatieDownloadenResultaat
        Inherits VSPEDP.Resultaat

        Enum ValidatieStatusType
            Ontvangen = 0
            CorrectBevonden = 1
            Afgekeurd = 2
        End Enum

        Private mValidatieStatus As ValidatieStatusType
        Private mReferentie As String
        Private mValidatieID As Long
        Private mRetourBestandsgrootte As Long
        Private mRetourBestandsnaam As String
        Private mRetourData As Byte()
        Private mStandaardCEI As Integer
        Private mStandaardcode As String
        Private mStandaardsubversie As Integer
        Private mStandaardversie As Integer

        Public ReadOnly Property ValidatieStatusText() As String
            Get
                Select Case mValidatieStatus
                    Case ValidatieStatusType.Ontvangen
                        Return "Ontvangen"
                    Case ValidatieStatusType.CorrectBevonden
                        Return "Correct bevonden"
                    Case ValidatieStatusType.Afgekeurd
                        Return "Afgekeurd"
                End Select
                Return ""
            End Get
        End Property

        Public Overrides Function DebugText() As String
            Dim aMessage As String = ""

            aMessage = MyBase.DebugText & "Validatiestatus = " & ValidatieStatusText & vbCrLf
            aMessage = aMessage & "Referentie = " & mReferentie & vbCrLf
            aMessage = aMessage & "ValidatieID = " & mValidatieID.ToString & vbCrLf
            aMessage = aMessage & "RetourBestandsgrootte = " & mRetourBestandsgrootte.ToString & vbCrLf
            aMessage = aMessage & "RetourBestandsnaam = " & mRetourBestandsnaam & vbCrLf
            aMessage = aMessage & "RetourData = " & If(mRetourData Is Nothing, "", System.Text.Encoding.ASCII.GetString(mRetourData)) & vbCrLf
            aMessage = aMessage & "StandaardCEI = " & mStandaardCEI.ToString & vbCrLf
            aMessage = aMessage & "Standaardcode = " & mStandaardcode & vbCrLf
            aMessage = aMessage & "mStandaardversie = " & mStandaardversie.ToString & vbCrLf
            aMessage = aMessage & "mStandaardsubversie = " & mStandaardsubversie.ToString & vbCrLf
            Dim i As Integer = 1
            For Each item As Resultaat.Melding In Meldingen
                If item.MeldingSoort <> Resultaat.MeldingBronType.None Then
                    aMessage = aMessage & "Melding " & i.ToString & "\" & Meldingen.Count.ToString & ": " & If(item.MeldingSoort = Resultaat.MeldingBronType.Vecozo, "Vecozo", "Vektis") & vbCrLf
                    aMessage = aMessage & "Code = " & item.Code & vbCrLf
                    aMessage = aMessage & "Omschrijving = " & item.Omschrijving & vbCrLf
                    aMessage = aMessage & "VektisMeldingKenmerkRecord = " & item.VektisMeldingKenmerkRecord.ToString & vbCrLf
                    aMessage = aMessage & "VektisMeldingIdentificatieDetailRecord = " & item.VektisMeldingIdentificatieDetailRecord.ToString & vbCrLf
                    aMessage = aMessage & "VektisMeldingRegelnummer = " & item.VektisMeldingRegelnummer.ToString & vbCrLf
                    aMessage = aMessage & "VektisMeldingRubrieknummer = " & item.VektisMeldingRubrieknummer.ToString & vbCrLf
                    aMessage = aMessage & "VektisMeldingVecozobatchLinenummer = " & item.VektisMeldingVecozoBatchLinenummer.ToString & vbCrLf
                End If
                i += 1
            Next
            Return aMessage
        End Function

        Public Property ValidatieStatus() As ValidatieStatusType
            Get
                Return mValidatieStatus
            End Get
            Set(ByVal value As ValidatieStatusType)
                mValidatieStatus = value
            End Set
        End Property

        Public Property Referentie() As String
            Get
                Return mReferentie
            End Get
            Set(ByVal value As String)
                mReferentie = value
            End Set
        End Property

        Public Property ValidatieID() As Long
            Get
                Return mValidatieID
            End Get
            Set(ByVal value As Long)
                mValidatieID = value
            End Set
        End Property

        Public Property RetourBestandsgrootte() As Long
            Get
                Return mRetourBestandsgrootte
            End Get
            Set(ByVal value As Long)
                mRetourBestandsgrootte = value
            End Set
        End Property

        Public Property RetourBestandsnaam() As String
            Get
                Return mRetourBestandsnaam
            End Get
            Set(ByVal value As String)
                mRetourBestandsnaam = value
            End Set
        End Property

        Public Property RetourData() As Byte()
            Get
                Return mRetourData
            End Get
            Set(ByVal value As Byte())
                mRetourData = value
            End Set
        End Property

        Public Property StandaardCEI() As Integer
            Get
                Return mStandaardCEI
            End Get
            Set(ByVal value As Integer)
                mStandaardCEI = value
            End Set
        End Property

        Public Property StandaardCode() As String
            Get
                Return mStandaardcode
            End Get
            Set(ByVal value As String)
                mStandaardcode = value
            End Set
        End Property

        Public Property Standaardsubversie() As Integer
            Get
                Return mStandaardsubversie
            End Get
            Set(ByVal value As Integer)
                mStandaardsubversie = value
            End Set
        End Property

        Public Property Standaardversie() As Integer
            Get
                Return mStandaardversie
            End Get
            Set(ByVal value As Integer)
                mStandaardversie = value
            End Set
        End Property
    End Class

    Public Class ValidatieOpvragenResultaat
        Inherits VSPEDP.Resultaat

        Enum ValidatieStatusType
            Ontvangen = 0
            CorrectBevonden = 1
            Afgekeurd = 2
        End Enum

        Private mValidatieID As Long
        Private mValidatieStatus As ValidatieStatusType
        Private mReferentie As String
        Private mAantalResultaten As Integer

        Public ReadOnly Property ValidatieStatusText() As String
            Get
                Select Case mValidatieStatus
                    Case ValidatieStatusType.Ontvangen
                        Return "Ontvangen"
                    Case ValidatieStatusType.CorrectBevonden
                        Return "Correct bevonden"
                    Case ValidatieStatusType.Afgekeurd
                        Return "Afgekeurd"
                End Select
                Return ""
            End Get
        End Property

        Public Overrides Function DebugText() As String
            Dim aMessage As String = ""

            aMessage = MyBase.DebugText
            aMessage = aMessage & "ValidatieID = " & mValidatieID.ToString & vbCrLf
            aMessage = aMessage & "Validatiestatus = " & ValidatieStatusText & vbCrLf
            aMessage = aMessage & "Referentie = " & mReferentie & vbCrLf
            aMessage = aMessage & "AantalResultaten = " & mAantalResultaten.ToString & vbCrLf
            Return aMessage
        End Function

        Public Property ValidatieID() As Long
            Get
                Return mValidatieID
            End Get
            Set(ByVal value As Long)
                mValidatieID = value
            End Set
        End Property

        Public Property ValidatieStatus() As ValidatieStatusType
            Get
                Return mValidatieStatus
            End Get
            Set(ByVal value As ValidatieStatusType)
                mValidatieStatus = value
            End Set
        End Property

        Public Property Referentie() As String
            Get
                Return mReferentie
            End Get
            Set(ByVal value As String)
                mReferentie = value
            End Set
        End Property

        Public Property AantalResultaten() As Integer
            Get
                Return mAantalResultaten
            End Get
            Set(ByVal value As Integer)
                mAantalResultaten = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class

    'TODO Error afhandeling
    Public Sub New()
        MyBase.New(EndpointType.DownloadenValidatieV1Soap11)
        If mLastResultaat Is Nothing Then
            Exit Sub
        End If

        Try
            mVSEPDValidatieDownloaden = New wsVSPEDPValidatieDownloaden.VspEdpIncomingDownloadenValidatiePortClient(Binding, WSaddress)

            For counter As Integer = mVSEPDValidatieDownloaden.ChannelFactory.Endpoint.Behaviors.Count - 1 To 0 Step -1
                If TypeOf mVSEPDValidatieDownloaden.ChannelFactory.Endpoint.Behaviors(counter) Is System.ServiceModel.Description.ClientCredentials Then
                    mVSEPDValidatieDownloaden.ChannelFactory.Endpoint.Behaviors.RemoveAt(counter)
                    Exit For
                End If
            Next
            mVSEPDValidatieDownloaden.ChannelFactory.Endpoint.Behaviors.Add(ClientCredentials)
        Catch ex As Exception
            mLastResultaat = New Resultaat
            mLastResultaat.MeldingBron = Resultaat.MeldingBronType.I2M
            mLastResultaat.Status = Resultaat.StatusType.Fout
            mLastResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_OTH
            mLastResultaat.ResultaatExtra = ex.Message
        End Try
    End Sub

    'TODO Unieke nummer identificeren aan de hand van regelnummer
    'TODO Dowloaden meteen na indienen
    Public Function ValidatieDownloaden(VecozoBatchID As Long) As ValidatieDownloadenResultaat
        Dim dbCommand As New SqlCommand
        Dim dbReader As SqlDataReader
        Dim aResultaat As New ValidatieDownloadenResultaat

        Dim VBSendmethodID As Long
        Dim VBMessageStandardID As Long
        Dim VBStatus As Long
        Dim VBDeleted As String
        Dim VBProcessed As String
        Dim VecozoValidatieID As String
        Try
            If dbConnection.State = ConnectionState.Closed Then dbConnection.Open()

            'Get vecozobatchdetails
            dbCommand.Connection = dbConnection
            dbCommand = New SqlCommand("SELECT [VecozoValidatieID], [Batchnummer], [VecozoBatchSendmethodID], [MessageStandardID], [Status], [Processed], [Deleted] FROM [VecozoBatch] WHERE VecozoBatchID = " & VecozoBatchID.ToString, dbConnection)
            dbReader = dbCommand.ExecuteReader()
            If dbReader.Read() Then
                VecozoValidatieID = EvaluateFromSQL(dbReader.GetValue(dbReader.GetOrdinal("VecozoValidatieID")))
                VBSendmethodID = EvaluateFromSQL(dbReader.GetValue(dbReader.GetOrdinal("VecozoBatchSendmethodID")))
                VBMessageStandardID = EvaluateFromSQL(dbReader.GetValue(dbReader.GetOrdinal("MessageStandardID")))
                VBStatus = EvaluateFromSQL(dbReader.GetValue(dbReader.GetOrdinal("Status")))
                VBDeleted = EvaluateFromSQL(dbReader.GetValue(dbReader.GetOrdinal("Deleted")))
                VBProcessed = EvaluateFromSQL(dbReader.GetValue(dbReader.GetOrdinal("Processed")))

                'check if batch is processed
                If VBProcessed = "T" Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_PROC
                ElseIf VBDeleted = "T" Or VBStatus = 6 Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_DEL
                ElseIf VBStatus <> 9 Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_VALG
                ElseIf VBSendmethodID <> 1 Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_NVSP
                ElseIf VecozoValidatieID = "" Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_VALID
                ElseIf Not (VBMessageStandardID = "1" Or VBMessageStandardID = "2") Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_MSG
                End If
            Else
                aResultaat.Status = Resultaat.StatusType.Fout
                aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_NFOUND
            End If
            dbReader.Close()
            If aResultaat.Status <> Resultaat.StatusType.None Then
                aResultaat.MeldingBron = Resultaat.MeldingBronType.I2M
                Return aResultaat
            End If

            Dim request As New wsVSPEDPValidatieDownloaden.DownloadValidatieresultaatRequest
            request.ValidatieId = CType(VecozoValidatieID, Long)
            mVSEPDValidatieDownloaden.Open()

            Dim respons As wsVSPEDPValidatieDownloaden.DownloadValidatieresultaatResponse = mVSEPDValidatieDownloaden.DownloadValidatieresultaat(request)
            If Not respons Is Nothing Then
                aResultaat.SetResultaatcode(respons.Resultaatcode.ToString)
                If aResultaat.Status = Resultaat.StatusType.Succes Or Resultaat.StatusType.SuccesMetOpmerking Then
                    aResultaat.VecozoBatchID = VecozoBatchID
                    If Not respons.Resultaat Is Nothing Then
                        aResultaat.ValidatieStatus = respons.Resultaat.Status
                        aResultaat.Referentie = respons.Resultaat.Referentie
                        aResultaat.ValidatieID = respons.Resultaat.ValidatieId
                        If Not respons.Resultaat.EIRetourBestand Is Nothing Then
                            aResultaat.RetourBestandsgrootte = respons.Resultaat.EIRetourBestand.Bestandsgrootte
                            aResultaat.RetourBestandsnaam = respons.Resultaat.EIRetourBestand.Bestandsnaam
                            aResultaat.RetourData = respons.Resultaat.EIRetourBestand.Data
                        End If
                        If Not respons.Resultaat.EIStandaard Is Nothing Then
                            aResultaat.StandaardCEI = respons.Resultaat.EIStandaard.StandaardCEI
                            aResultaat.StandaardCode = respons.Resultaat.EIStandaard.StandaardCode
                            aResultaat.Standaardsubversie = respons.Resultaat.EIStandaard.StandaardSubVersie
                            aResultaat.Standaardversie = respons.Resultaat.EIStandaard.StandaardVersie
                        End If

                        For Each item As Object In respons.Resultaat.Meldingen
                            Dim aMelding As New ValidatieDownloadenResultaat.Melding
                            If TypeName(item) = "VecozoMelding" Then
                                Dim aVecozoMelding As wsVSPEDPValidatieDownloaden.VecozoMelding
                                aVecozoMelding = CType(item, wsVSPEDPValidatieDownloaden.VecozoMelding)
                                aMelding.MeldingSoort = Resultaat.MeldingBronType.Vecozo
                                aMelding.Code = aVecozoMelding.Code
                                aMelding.Omschrijving = aVecozoMelding.Omschrijving
                                aResultaat.Meldingen.Add(aMelding)

                            ElseIf TypeName(item) = "VektisMelding" Then
                                Dim aVektisMelding As wsVSPEDPValidatieDownloaden.VektisMelding
                                aVektisMelding = CType(item, wsVSPEDPValidatieDownloaden.VektisMelding)
                                aMelding.MeldingSoort = Resultaat.MeldingBronType.Vektis
                                aMelding.Code = aVektisMelding.Code
                                aMelding.Omschrijving = aVektisMelding.Omschrijving
                                aMelding.VektisMeldingKenmerkRecord = aVektisMelding.KenmerkRecord
                                aMelding.VektisMeldingIdentificatieDetailRecord = aVektisMelding.IdentificatieDetailRecord
                                aMelding.VektisMeldingRegelnummer = aVektisMelding.Regelnummer
                                aMelding.VektisMeldingRubrieknummer = aVektisMelding.Rubrieknummer
                                Dim aVektis As New Vektis()
                                Dim stream As New MemoryStream(aResultaat.RetourData)
                                aMelding.VektisMeldingVecozoBatchLinenummer = aVektis.GetVecozoBatchLineID(VecozoBatchID, stream, aVektisMelding.Regelnummer)
                                aResultaat.Meldingen.Add(aMelding)
                            Else
                                'TODO
                                Throw New System.Exception("Foute cast")
                            End If
                        Next
                    Else
                        'TODO
                    End If
                Else
                    'TODO
                End If
            Else
                'TODO
            End If
        Catch e As Exception
            aResultaat.Status = Resultaat.StatusType.Fout
            aResultaat.MeldingBron = Resultaat.MeldingBronType.I2M
            aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_OTH
            aResultaat.ResultaatExtra = e.Message
        Finally
            If dbConnection.State = ConnectionState.Open Then dbConnection.Close()
        End Try

        Return aResultaat
    End Function

    'TODO encoding UTF-8?
    Public Function ProcessValidatieDownloaden(aValidatieDownloadenResultaat As ValidatieDownloadenResultaat) As Resultaat
        Dim dbConnection As SqlConnection
        Dim dbCommand As New SqlCommand
        Dim dbReader As SqlDataReader
        Dim aResultaat As New Resultaat

        If aValidatieDownloadenResultaat.ValidatieStatus = ValidatieDownloadenResultaat.ValidatieStatusType.Ontvangen Then
            aResultaat.MeldingBron = Resultaat.MeldingBronType.I2M
            aResultaat.Status = Resultaat.StatusType.SuccesMetOpmerking
            aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_VALONTV
            Return aResultaat
        End If

        Try
            If ConfigurationManager.ConnectionStrings("IntomediFin") Is Nothing Then
                aResultaat.MeldingBron = Resultaat.MeldingBronType.I2M
                aResultaat.Status = Resultaat.StatusType.Fout
                aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_NCON
                Return aResultaat
            End If
            dbConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("IntomediFin").ToString())
            dbConnection.Open()

            'dbCommand = New SqlCommand("SELECT [VecozoBatchID], [LocationID], [Batchdate], [VecozoBatchSendmethodID], [MessageStandardID], [Status], [VecozoDeclaratieID] , [Period], [Bookyear], [Senddate], [Returndate], [Remark], [Processed], [ReturnFileURL], [ValidateDate], [ValidateReturnDate], [LocationAGB], [VecozoValidatieID] FROM [VecozoBatch] WHERE VecozoBatchID = " & aValidatieIndienenResultaat.VecozoBatchID.ToString, dbConnection)
            dbCommand = New SqlCommand("SELECT [VecozoBatchID], [LocationID], [Batchdate], [MessageStandardID], [Period], [Bookyear], [Remark], [Processed], [LocationAGB], [ValidateDate], [VecozoValidatieID] FROM [VecozoBatch] WHERE [VecozoBatchID] = " & aValidatieDownloadenResultaat.VecozoBatchID.ToString, dbConnection)
            dbReader = dbCommand.ExecuteReader()
            If dbReader.Read() Then
                Dim sqlupdate As System.Data.SqlClient.SqlCommand
                sqlupdate = New System.Data.SqlClient.SqlCommand("sp_update_VecozoBatch", dbConnection)
                sqlupdate.CommandType = CommandType.StoredProcedure
                sqlupdate.Parameters.Add("@VecozoBatchID", SqlDbType.BigInt).Value = aValidatieDownloadenResultaat.VecozoBatchID
                sqlupdate.Parameters.Add("@LocationID", SqlDbType.BigInt).Value = dbReader.GetValue(dbReader.GetOrdinal("LocationID"))
                sqlupdate.Parameters.Add("@Batchdate", SqlDbType.Date).Value = dbReader.GetValue(dbReader.GetOrdinal("Batchdate"))
                sqlupdate.Parameters.Add("@VecozoBatchSendmethodID", SqlDbType.BigInt).Value = 1
                sqlupdate.Parameters.Add("@MessageStandardID", SqlDbType.BigInt).Value = dbReader.GetValue(dbReader.GetOrdinal("MessageStandardID"))
                If aValidatieDownloadenResultaat.ValidatieStatus = ValidatieDownloadenResultaat.ValidatieStatusType.Afgekeurd Then
                    sqlupdate.Parameters.Add("@Status", SqlDbType.BigInt).Value = 8
                Else
                    sqlupdate.Parameters.Add("@Status", SqlDbType.BigInt).Value = 7
                End If
                sqlupdate.Parameters.Add("@VecozoDeclaratieID", SqlDbType.NVarChar, 50).Value = DBNull.Value
                sqlupdate.Parameters.Add("@Period", SqlDbType.BigInt).Value = dbReader.GetValue(dbReader.GetOrdinal("Period"))
                sqlupdate.Parameters.Add("@Bookyear", SqlDbType.BigInt).Value = dbReader.GetValue(dbReader.GetOrdinal("Bookyear"))
                sqlupdate.Parameters.Add("@Senddate", SqlDbType.Date).Value = DBNull.Value
                sqlupdate.Parameters.Add("@Returndate", SqlDbType.Date).Value = DBNull.Value
                sqlupdate.Parameters.Add("@Remarks", SqlDbType.NVarChar, -1).Value = dbReader.GetValue(dbReader.GetOrdinal("Remark"))
                sqlupdate.Parameters.Add("@Processed", SqlDbType.NVarChar, 1).Value = dbReader.GetValue(dbReader.GetOrdinal("Processed"))
                sqlupdate.Parameters.Add("@ReturnFileURL", SqlDbType.NVarChar, -1).Value = DBNull.Value
                sqlupdate.Parameters.Add("@UpdateUserID", SqlDbType.NVarChar, 8).Value = "YOKARIBA"
                sqlupdate.Parameters.Add("@ValidateDate", SqlDbType.Date).Value = dbReader.GetValue(dbReader.GetOrdinal("ValidateDate"))
                sqlupdate.Parameters.Add("@ValidateReturnDate", SqlDbType.Date).Value = Date.Now()
                sqlupdate.Parameters.Add("@LocationAGB", SqlDbType.NVarChar, 8).Value = dbReader.GetValue(dbReader.GetOrdinal("LocationAGB"))
                sqlupdate.Parameters.Add("@VecozoValidatieID", SqlDbType.NVarChar, 50).Value = dbReader.GetValue(dbReader.GetOrdinal("VecozoValidatieID"))
                rows = sqlupdate.ExecuteNonQuery()

                If aValidatieDownloadenResultaat.RetourBestandsgrootte > 0 Then
                    'Update/insert ValidatieRetour

                    sqlupdate = New System.Data.SqlClient.SqlCommand("sp_add_VecozoFile", dbConnection)
                    sqlupdate.CommandType = CommandType.StoredProcedure
                    sqlupdate.Parameters.Add("@VecozoBatchID", SqlDbType.BigInt).Value = aValidatieDownloadenResultaat.VecozoBatchID
                    sqlupdate.Parameters.Add("@VecozoFileTypeID", SqlDbType.BigInt).Value = 2
                    sqlupdate.Parameters.Add("@Filename", SqlDbType.NVarChar, 200).Value = aValidatieDownloadenResultaat.RetourBestandsnaam
                    sqlupdate.Parameters.Add("@Filedate", SqlDbType.DateTime).Value = DateTime.Now
                    sqlupdate.Parameters.Add("@File", SqlDbType.NVarChar, -1).Value = System.Text.Encoding.ASCII.GetString(aValidatieDownloadenResultaat.RetourData)
                    sqlupdate.Parameters.Add("@FileID", SqlDbType.BigInt).Value = DBNull.Value
                    sqlupdate.Parameters.Add("@Remark", SqlDbType.NVarChar, -1).Value = DBNull.Value
                    sqlupdate.Parameters.Add("@UpdateUserID", SqlDbType.NVarChar, 8).Value = "YOKARIBA"
                    sqlupdate.Parameters.Add("@VecozoFileID", SqlDbType.BigInt)
                    sqlupdate.Parameters("@VecozoFileID").Direction = ParameterDirection.Output
                    rows = sqlupdate.ExecuteNonQuery()
                Else
                    'Delete ValidatieRetour
                    sqlupdate = New System.Data.SqlClient.SqlCommand("sp_delete_VecozoFileByType", dbConnection)
                    sqlupdate.CommandType = CommandType.StoredProcedure
                    sqlupdate.Parameters.Add("@VecozoBatchID", SqlDbType.BigInt).Value = aValidatieDownloadenResultaat.VecozoBatchID
                    sqlupdate.Parameters.Add("@VecozoFileTypeID", SqlDbType.BigInt).Value = 2
                    sqlupdate.Parameters.Add("@UpdateUserID", SqlDbType.NVarChar, 8).Value = "YOKARIBA"
                    rows = sqlupdate.ExecuteNonQuery()
                End If

                'Delete DeclaratieRetour
                sqlupdate = New System.Data.SqlClient.SqlCommand("sp_delete_VecozoFileByType", dbConnection)
                sqlupdate.CommandType = CommandType.StoredProcedure
                sqlupdate.Parameters.Add("@VecozoBatchID", SqlDbType.BigInt).Value = aValidatieDownloadenResultaat.VecozoBatchID
                sqlupdate.Parameters.Add("@VecozoFileTypeID", SqlDbType.BigInt).Value = 1
                sqlupdate.Parameters.Add("@UpdateUserID", SqlDbType.NVarChar, 8).Value = "YOKARIBA"
                rows = sqlupdate.ExecuteNonQuery()

                'Verwijder Meldingen van DeclaratieRetour
                sqlupdate = New System.Data.SqlClient.SqlCommand("sp_delete_VecozoNotification", dbConnection)
                sqlupdate.CommandType = CommandType.StoredProcedure
                sqlupdate.Parameters.Add("@VecozoBatchID", SqlDbType.BigInt).Value = aValidatieDownloadenResultaat.VecozoBatchID
                sqlupdate.Parameters.Add("@VecozoFileTypeID", SqlDbType.BigInt).Value = 1
                sqlupdate.Parameters.Add("@UpdateUserID", SqlDbType.NVarChar, 8).Value = "YOKARIBA"
                rows = sqlupdate.ExecuteNonQuery()

                'Insert Meldingen
                'Verwijder eerst oude
                sqlupdate = New System.Data.SqlClient.SqlCommand("sp_delete_VecozoNotification", dbConnection)
                sqlupdate.CommandType = CommandType.StoredProcedure
                sqlupdate.Parameters.Add("@VecozoBatchID", SqlDbType.BigInt).Value = aValidatieDownloadenResultaat.VecozoBatchID
                sqlupdate.Parameters.Add("@VecozoFileTypeID", SqlDbType.BigInt).Value = 2
                sqlupdate.Parameters.Add("@UpdateUserID", SqlDbType.NVarChar, 8).Value = "YOKARIBA"
                rows = sqlupdate.ExecuteNonQuery()

                For Each item As ValidatieDownloadenResultaat.Melding In aValidatieDownloadenResultaat.Meldingen
                    If item.MeldingSoort <> Resultaat.MeldingBronType.None Then
                        sqlupdate = New System.Data.SqlClient.SqlCommand("sp_add_VecozoNotification", dbConnection)
                        sqlupdate.CommandType = CommandType.StoredProcedure
                        sqlupdate.Parameters.Add("@VecozoBatchID", SqlDbType.BigInt).Value = aValidatieDownloadenResultaat.VecozoBatchID
                        sqlupdate.Parameters.Add("@VecozoFileTypeID", SqlDbType.BigInt).Value = 2
                        sqlupdate.Parameters.Add("@VecozoBatchLinenumber", SqlDbType.Int).Value = item.VektisMeldingVecozoBatchLinenummer
                        sqlupdate.Parameters.Add("@VecozoNotificationSource", SqlDbType.NVarChar, 10).Value = If(item.MeldingSoort = ValidatieDownloadenResultaat.MeldingBronType.Vecozo, "Vecozo", "Vektis")
                        sqlupdate.Parameters.Add("@Code", SqlDbType.NVarChar, 10).Value = item.Code
                        sqlupdate.Parameters.Add("@Description", SqlDbType.NVarChar, 255).Value = item.Omschrijving
                        sqlupdate.Parameters.Add("@KenmerkRecord", SqlDbType.Int).Value = item.VektisMeldingKenmerkRecord
                        sqlupdate.Parameters.Add("@IdentificatieDetailRecord", SqlDbType.BigInt).Value = item.VektisMeldingIdentificatieDetailRecord
                        sqlupdate.Parameters.Add("@Regelnummer", SqlDbType.Int).Value = item.VektisMeldingRegelnummer
                        sqlupdate.Parameters.Add("@Rubrieknummer", SqlDbType.Int).Value = item.VektisMeldingRubrieknummer
                        sqlupdate.Parameters.Add("@Remark", SqlDbType.NVarChar, -1).Value = DBNull.Value
                        sqlupdate.Parameters.Add("@UpdateUserID", SqlDbType.NVarChar, 8).Value = "YOKARIBA"
                        sqlupdate.Parameters.Add("@VecozoNotificationID", SqlDbType.BigInt)
                        sqlupdate.Parameters("@VecozoNotificationID").Direction = ParameterDirection.Output
                        rows = sqlupdate.ExecuteNonQuery()
                    End If
                Next
            Else
                aResultaat.Status = Resultaat.StatusType.Fout
                aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_NFOUND
                Return aResultaat
            End If
        Catch e As Exception
            aResultaat.Status = Resultaat.StatusType.Fout
            aResultaat.MeldingBron = Resultaat.MeldingBronType.I2M
            aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_OTH
            aResultaat.ResultaatExtra = e.Message
            Return aResultaat
        End Try

        aResultaat.Status = Resultaat.StatusType.Succes
        Return aResultaat
    End Function

    Public Function ValidatieResultaat(VecozoBatchID As Long) As ValidatieOpvragenResultaat
        Dim dbConnection As SqlConnection
        Dim dbCommand As New SqlCommand
        Dim dbReader As SqlDataReader
        Dim aResultaat As New ValidatieOpvragenResultaat
        Dim VecozoValidatieID As String
        Try
            If ConfigurationManager.ConnectionStrings("IntomediFin") Is Nothing Then
                aResultaat.Status = Resultaat.StatusType.Fout
                aResultaat.MeldingBron = Resultaat.MeldingBronType.I2M
                aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_NCON
                Return aResultaat
            End If
            dbConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("IntomediFin").ToString())
            dbConnection.Open()

            'Get vecozobatchdetails
            dbCommand.Connection = dbConnection
            dbCommand = New SqlCommand("SELECT [VecozoValidatieID] FROM [VecozoBatch] WHERE VecozoBatchID = " & VecozoBatchID.ToString, dbConnection)
            dbReader = dbCommand.ExecuteReader()
            If dbReader.Read() Then
                VecozoValidatieID = EvaluateFromSQL(dbReader.GetValue(dbReader.GetOrdinal("VecozoValidatieID")))

                'check if batch is processed
                If VecozoValidatieID = "" Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_VALID
                End If
            Else
                aResultaat.Status = Resultaat.StatusType.Fout
                aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_NFOUND
            End If
            dbReader.Close()
            If aResultaat.Status <> Resultaat.StatusType.None Then
                aResultaat.MeldingBron = Resultaat.MeldingBronType.I2M
                Return aResultaat
            End If

            Dim request As New wsVSPEDPValidatieDownloaden.OpvragenValidatieresultatenRequest
            request.ValidatieId = CType(VecozoValidatieID, Long)
            mVSEPDValidatieDownloaden.Open()

            Dim respons As wsVSPEDPValidatieDownloaden.OpvragenValidatieresultatenResponse = mVSEPDValidatieDownloaden.OpvragenValidatieresultaten(request)
            If Not respons Is Nothing Then
                aResultaat.SetResultaatcode(respons.Resultaatcode.ToString)
                If aResultaat.Status = Resultaat.StatusType.Succes Or Resultaat.StatusType.SuccesMetOpmerking Then
                    aResultaat.VecozoBatchID = VecozoBatchID
                    aResultaat.AantalResultaten = respons.AantalResultaten
                    For Each item As Object In respons.Resultaten
                        Dim aValidatieResultaat As wsVSPEDPValidatieDownloaden.Validatieresultaat
                        aValidatieResultaat = CType(item, wsVSPEDPValidatieDownloaden.Validatieresultaat)
                        aResultaat.ValidatieID = aValidatieResultaat.ValidatieId
                        aResultaat.Referentie = aValidatieResultaat.Referentie
                        aResultaat.ValidatieStatus = aValidatieResultaat.Referentie
                        Exit For
                    Next
                Else
                    'TODO
                End If
            Else
                'TODO
            End If
        Catch e As Exception
            aResultaat.Status = Resultaat.StatusType.Fout
            aResultaat.MeldingBron = Resultaat.MeldingBronType.I2M
            aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_OTH
            aResultaat.ResultaatExtra = e.Message
        End Try

        Return aResultaat
    End Function

    Public Sub Dispose()
        Try
            mVSEPDValidatieDownloaden.Close()
        Finally
            mVSEPDValidatieDownloaden = Nothing
        End Try
    End Sub
End Class

Public Class VSEDPDeclaratieDownloaden
    Inherits VSPEDP

    Dim mVSEPDDeclaratieDownloaden As wsVSPEDPDeclaratieDownloaden.VspEdpIncomingZvlDownloadenClient

    Public Class DeclaratieStatusResultaat
        Inherits VSPEDP.Resultaat

        Enum DeclaratieStatusType
            Geen = -1
            OntvangenDoorVecozo = 0
            CorrectBevondenDoorVecozo = 1
            AfgekeurdDoorVecozo = 2
            KlaarVoorVerzendingNaarZorgverzekeraar = 3
            SuccesvolOntvangenDoorZorgverzekeraarGemeente = 4
            AfgehandeldDoorZorgverzekeraarGemeente = 5
        End Enum

        Private mDeclaratieStatus As DeclaratieStatusType
        Private mDeclaratieID As Long
        Private mMeldingSoort As MeldingBronType
        Private mMeldingCode As String
        Private mmeldingOmschrijving As String
        Private mMeldingVektisKenmerkRecord As Integer
        Private mMeldingVektisIdentificatieDetailRecord As Long

        Public ReadOnly Property DeclaratieStatusText() As String
            Get
                Select Case mDeclaratieStatus
                    Case DeclaratieStatusType.OntvangenDoorVecozo
                        Return "Ontvangen door vecozo"
                    Case DeclaratieStatusType.CorrectBevondenDoorVecozo
                        Return "Correct bevonden door vecozo"
                    Case DeclaratieStatusType.AfgekeurdDoorVecozo
                        Return "Afgekeur door vecozo"
                    Case DeclaratieStatusType.AfgehandeldDoorZorgverzekeraarGemeente
                        Return "Klaar voor verzending naar zorgverzekeraar/gemeente"
                    Case DeclaratieStatusType.SuccesvolOntvangenDoorZorgverzekeraarGemeente
                        Return "Succesvol ontvangen door zorgverzekeraar/gemeente"
                    Case DeclaratieStatusType.AfgehandeldDoorZorgverzekeraarGemeente
                        Return "Afgehandeld door zorgverzekeraar/gemeente"
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Public Overrides Function DebugText() As String
            Dim aMessage As String = ""

            aMessage = MyBase.DebugText & "Declaratiestatus = " & DeclaratieStatusText & vbCrLf
            aMessage = aMessage & "DeclaratieID = " & mDeclaratieID.ToString & vbCrLf
            If mMeldingSoort <> MeldingBronType.None Then
                aMessage = aMessage & "MeldingCode = " & mMeldingCode & vbCrLf
                aMessage = aMessage & "MeldingOmschrijving = " & mmeldingOmschrijving & vbCrLf
                aMessage = aMessage & "MeldingVektisKenmerkRecord = " & mMeldingVektisKenmerkRecord.ToString & vbCrLf
                aMessage = aMessage & "MeldingVektisIdentificatieDetailRecord = " & mMeldingVektisIdentificatieDetailRecord.ToString & vbCrLf
            End If
            Return aMessage
        End Function

        Public Property DeclaratieStatus() As DeclaratieStatusType
            Get
                Return mDeclaratieStatus
            End Get
            Set(ByVal value As DeclaratieStatusType)
                mDeclaratieStatus = value
            End Set
        End Property

        Public Property DeclaratieID() As Long
            Get
                Return mDeclaratieID
            End Get
            Set(ByVal value As Long)
                mDeclaratieID = value
            End Set
        End Property

        Public Property MeldingSoort() As MeldingBronType
            Get
                Return mMeldingSoort
            End Get
            Set(ByVal value As MeldingBronType)
                mMeldingSoort = value
            End Set
        End Property

        Public Property MeldingCode() As String
            Get
                Return mMeldingCode
            End Get
            Set(ByVal value As String)
                mMeldingCode = value
            End Set
        End Property

        Public Property MeldingOmschrijving() As String
            Get
                Return mmeldingOmschrijving
            End Get
            Set(ByVal value As String)
                mmeldingOmschrijving = value
            End Set
        End Property

        Public Property MeldingVektisKenmerkRecord() As Integer
            Get
                Return mMeldingVektisKenmerkRecord
            End Get
            Set(ByVal value As Integer)
                mMeldingVektisKenmerkRecord = value
            End Set
        End Property

        Public Property MeldingVektisIdentificatieDetailRecord() As Long
            Get
                Return mMeldingVektisIdentificatieDetailRecord
            End Get
            Set(ByVal value As Long)
                mMeldingVektisIdentificatieDetailRecord = value
            End Set
        End Property

        Sub New()
            mDeclaratieStatus = DeclaratieStatusType.Geen
            mMeldingSoort = MeldingBronType.None
        End Sub
    End Class

    Public Class DeclaratieDownloadenResultaat
        Inherits DeclaratieStatusResultaat

        Enum RetourbestandStatusType
            Onbekend = 0
            TeruggestuurdDoorZorgverzekeraar = 1
            CorrectBevondenDoorVecozo = 2
            AfgekeurdDoorVecozo = 3
            GedownloadDoorZorgaanbieder = 4
            Verwijderd = 5
        End Enum

        Private mRetourBestandsgrootte As Long
        Private mRetourBestandsnaam As String
        Private mRetourData As Byte()
        Private mStandaardCEI As Integer
        Private mStandaardcode As String
        Private mStandaardsubversie As Integer
        Private mStandaardversie As Integer
        Private mEmailAdresZorgverzekeraar As String
        Private mGoedgekeurdOp As Date
        Private mIngediendOp As Date
        Private mReferentieZorgverzekeraar As String
        Private mRetourbestandId As Long
        Private mRetourbestandStatus As RetourbestandStatusType

        Public ReadOnly Property RetourbestandStatusText() As String
            Get
                Select Case RetourbestandStatus
                    Case RetourbestandStatusType.Onbekend
                        Return "Onbekend"
                    Case RetourbestandStatusType.TeruggestuurdDoorZorgverzekeraar
                        Return "Teruggestuurd door zorgverzekeraar"
                    Case RetourbestandStatusType.CorrectBevondenDoorVecozo
                        Return "Correct bevonden door vecozo"
                    Case RetourbestandStatusType.AfgekeurdDoorVecozo
                        Return "Afgekeur door vecozo"
                    Case RetourbestandStatusType.GedownloadDoorZorgaanbieder
                        Return "Gedownload door zorgaanbieder"
                    Case RetourbestandStatusType.Verwijderd
                        Return "Verwijderd"
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Public Overrides Function DebugText() As String
            Dim aMessage As String = ""

            aMessage = MyBase.DebugText & "RetourBestandsgrootte = " & mRetourBestandsgrootte.ToString & vbCrLf
            aMessage = aMessage & "RetourBestandsnaam = " & mRetourBestandsnaam & vbCrLf
            aMessage = aMessage & "RetourData = " & If(mRetourData Is Nothing, "", System.Text.Encoding.ASCII.GetString(mRetourData)) & vbCrLf
            aMessage = aMessage & "StandaardCEI = " & mStandaardCEI.ToString & vbCrLf
            aMessage = aMessage & "Standaardcode = " & mStandaardcode & vbCrLf
            aMessage = aMessage & "mStandaardversie = " & mStandaardversie.ToString & vbCrLf
            aMessage = aMessage & "mStandaardsubversie = " & mStandaardsubversie.ToString & vbCrLf
            aMessage = aMessage & "EmailAdresZorgverzekeraar = " & mEmailAdresZorgverzekeraar & vbCrLf
            aMessage = aMessage & "GoedgekeurdOp = " & mGoedgekeurdOp.ToString & vbCrLf
            aMessage = aMessage & "IngediendOp = " & mIngediendOp.ToString & vbCrLf
            aMessage = aMessage & "ReferentieZorgverzekeraar = " & mReferentieZorgverzekeraar & vbCrLf
            aMessage = aMessage & "RetourbestandId = " & mRetourbestandId.ToString & vbCrLf
            aMessage = aMessage & "RetourbestandStatus = " & RetourbestandStatusText & vbCrLf
            Return aMessage
        End Function

        Public Property RetourBestandsgrootte() As Long
            Get
                Return mRetourBestandsgrootte
            End Get
            Set(ByVal value As Long)
                mRetourBestandsgrootte = value
            End Set
        End Property

        Public Property RetourBestandsnaam() As String
            Get
                Return mRetourBestandsnaam
            End Get
            Set(ByVal value As String)
                mRetourBestandsnaam = value
            End Set
        End Property

        Public Property RetourData() As Byte()
            Get
                Return mRetourData
            End Get
            Set(ByVal value As Byte())
                mRetourData = value
            End Set
        End Property

        Public Property StandaardCEI() As Integer
            Get
                Return mStandaardCEI
            End Get
            Set(ByVal value As Integer)
                mStandaardCEI = value
            End Set
        End Property

        Public Property StandaardCode() As String
            Get
                Return mStandaardcode
            End Get
            Set(ByVal value As String)
                mStandaardcode = value
            End Set
        End Property

        Public Property Standaardsubversie() As Integer
            Get
                Return mStandaardsubversie
            End Get
            Set(ByVal value As Integer)
                mStandaardsubversie = value
            End Set
        End Property

        Public Property Standaardversie() As Integer
            Get
                Return mStandaardversie
            End Get
            Set(ByVal value As Integer)
                mStandaardversie = value
            End Set
        End Property

        Public Property EmailAdresZorgverzekeraar() As String
            Get
                Return mEmailAdresZorgverzekeraar
            End Get
            Set(ByVal value As String)
                mEmailAdresZorgverzekeraar = value
            End Set
        End Property

        Public Property GoedgekeurdOp() As String
            Get
                Return mGoedgekeurdOp
            End Get
            Set(ByVal value As String)
                mGoedgekeurdOp = value
            End Set
        End Property

        Public Property IngediendOp() As String
            Get
                Return mIngediendOp
            End Get
            Set(ByVal value As String)
                mIngediendOp = value
            End Set
        End Property

        Public Property ReferentieZorgverzekeraar() As String
            Get
                Return mReferentieZorgverzekeraar
            End Get
            Set(ByVal value As String)
                mReferentieZorgverzekeraar = value
            End Set
        End Property

        Public Property RetourbestandId() As Long
            Get
                Return mRetourbestandId
            End Get
            Set(ByVal value As Long)
                mRetourbestandId = value
            End Set
        End Property

        Public Property RetourbestandStatus() As RetourbestandStatusType
            Get
                Return mRetourbestandStatus
            End Get
            Set(ByVal value As RetourbestandStatusType)
                mRetourbestandStatus = value
            End Set
        End Property
    End Class

    Public Sub New()
        MyBase.New(EndpointType.DownloadenRetourinformatieV1Soap11)
        If mLastResultaat IsNot Nothing Then
            Exit Sub
        End If

        Try
            mVSEPDDeclaratieDownloaden = New wsVSPEDPDeclaratieDownloaden.VspEdpIncomingZvlDownloadenClient(Binding, WSaddress)

            For counter As Integer = mVSEPDDeclaratieDownloaden.ChannelFactory.Endpoint.Behaviors.Count - 1 To 0 Step -1
                If TypeOf mVSEPDDeclaratieDownloaden.ChannelFactory.Endpoint.Behaviors(counter) Is System.ServiceModel.Description.ClientCredentials Then
                    mVSEPDDeclaratieDownloaden.ChannelFactory.Endpoint.Behaviors.RemoveAt(counter)
                    Exit For
                End If
            Next
            mVSEPDDeclaratieDownloaden.ChannelFactory.Endpoint.Behaviors.Add(ClientCredentials)
        Catch ex As Exception
            mLastResultaat = New Resultaat
            mLastResultaat.MeldingBron = Resultaat.MeldingBronType.I2M
            mLastResultaat.Status = Resultaat.StatusType.Fout
            mLastResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_OTH
            mLastResultaat.ResultaatExtra = ex.Message
        End Try
    End Sub

    Public Function DeclaratieStatus(VecozoBatchID As Long) As DeclaratieStatusResultaat
        Dim dbCommand As New SqlCommand
        Dim dbReader As SqlDataReader
        Dim aResultaat As New DeclaratieStatusResultaat

        Dim VBSendmethodID As Long
        Dim VBMessageStandardID As Long
        Dim VBStatus As Long
        Dim VBDeleted As String
        Dim VBProcessed As String
        Dim VecozoDeclaratieID As String
        Try
            If dbConnection.State = ConnectionState.Closed Then dbConnection.Open()

            'Get vecozobatchdetails
            dbCommand.Connection = dbConnection
            dbCommand = New SqlCommand("SELECT [VecozoDeclaratieID], [Batchnummer], [VecozoBatchSendmethodID], [MessageStandardID], [Status], [Processed], [Deleted] FROM [VecozoBatch] WHERE VecozoBatchID = " & VecozoBatchID.ToString, dbConnection)
            dbReader = dbCommand.ExecuteReader()
            If dbReader.Read() Then
                VecozoDeclaratieID = EvaluateFromSQL(dbReader.GetValue(dbReader.GetOrdinal("VecozoDeclaratieID")))
                VBSendmethodID = EvaluateFromSQL(dbReader.GetValue(dbReader.GetOrdinal("VecozoBatchSendmethodID")))
                VBMessageStandardID = EvaluateFromSQL(dbReader.GetValue(dbReader.GetOrdinal("MessageStandardID")))
                VBStatus = EvaluateFromSQL(dbReader.GetValue(dbReader.GetOrdinal("Status")))
                VBDeleted = EvaluateFromSQL(dbReader.GetValue(dbReader.GetOrdinal("Deleted")))
                VBProcessed = EvaluateFromSQL(dbReader.GetValue(dbReader.GetOrdinal("Processed")))

                'check if batch is processed
                If VBProcessed = "T" Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_PROC
                ElseIf VBDeleted = "T" Or VBStatus = 6 Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_DEL
                ElseIf VBStatus <> 2 Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_DALG
                ElseIf VBSendmethodID <> 1 Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_NVSP
                ElseIf VecozoDeclaratieID = "" Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_DALID
                ElseIf Not (VBMessageStandardID = "1" Or VBMessageStandardID = "2") Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_MSG
                End If
            Else
                aResultaat.Status = Resultaat.StatusType.Fout
                aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_NFOUND
            End If
            dbReader.Close()
            If aResultaat.Status <> Resultaat.StatusType.None Then
                aResultaat.MeldingBron = Resultaat.MeldingBronType.I2M
                Return aResultaat
            End If

            Dim request As New wsVSPEDPDeclaratieDownloaden.RaadplegenDeclaratiestatusRequest
            request.Declaraties = New wsVSPEDPDeclaratieDownloaden.ArrayOfDeclaratieId
            request.Declaraties.Add(CType(VecozoDeclaratieID, Long))
            mVSEPDDeclaratieDownloaden.Open()
            Dim respons As wsVSPEDPDeclaratieDownloaden.RaadplegenDeclaratiestatusResponse = mVSEPDDeclaratieDownloaden.RaadplegenDeclaratiestatus(request)
            If Not respons Is Nothing Then
                If Not respons.ArrayOfDeclaratiestatusResultaat Is Nothing Then
                    Dim x As wsVSPEDPDeclaratieDownloaden.DeclaratiestatusResultaat = respons.ArrayOfDeclaratiestatusResultaat(0)
                    aResultaat.DeclaratieID = x.DeclaratieId
                    aResultaat.SetResultaatcode(x.Resultaatcode.ToString)
                    If x.Declaratie IsNot Nothing Then
                        aResultaat.DeclaratieStatus = x.Declaratie.Status
                        If x.Declaratie.Melding IsNot Nothing Then
                            If TypeName(x.Declaratie.Melding) = "VecozoMelding" Then
                                Dim aVecozoMelding As wsVSPEDPDeclaratieDownloaden.VecozoMelding
                                aVecozoMelding = CType(x.Declaratie.Melding, wsVSPEDPDeclaratieDownloaden.VecozoMelding)
                                aResultaat.MeldingSoort = DeclaratieDownloadenResultaat.MeldingBronType.Vecozo
                                aResultaat.MeldingCode = aVecozoMelding.Code
                                aResultaat.MeldingOmschrijving = aVecozoMelding.Omschrijving
                            ElseIf TypeName(x.Declaratie.Melding) = "VektisMelding" Then
                                Dim aVektisMelding As wsVSPEDPDeclaratieDownloaden.VektisMelding
                                aVektisMelding = CType(x.Declaratie.Melding, wsVSPEDPDeclaratieDownloaden.VektisMelding)
                                aResultaat.MeldingSoort = DeclaratieDownloadenResultaat.MeldingBronType.Vektis
                                aResultaat.MeldingCode = aVektisMelding.Code
                                aResultaat.MeldingOmschrijving = aVektisMelding.Omschrijving
                                aResultaat.MeldingVektisKenmerkRecord = aVektisMelding.KenmerkRecord
                                aResultaat.MeldingVektisIdentificatieDetailRecord = aVektisMelding.IdentificatieDetailRecord
                            Else
                                'TODO XXX
                                Throw New System.Exception("Foute cast")
                            End If
                        End If

                    Else
                            'TODO
                        End If
                    Else
                        'TODO
                    End If
            Else
                'TODO
            End If
        Catch e As Exception
            aResultaat.Status = Resultaat.StatusType.Fout
            aResultaat.MeldingBron = Resultaat.MeldingBronType.I2M
            aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_OTH
            aResultaat.ResultaatExtra = e.Message
        Finally
            If dbConnection.State = ConnectionState.Open Then dbConnection.Close()
        End Try

        Return aResultaat
    End Function

    'retourbericht downloaden
    Public Function DeclaratieDownloaden(DeclaratieID As Long) As DeclaratieDownloadenResultaat
        Dim aResultaat As New DeclaratieDownloadenResultaat
        Try
            Dim request As New wsVSPEDPDeclaratieDownloaden.DownloadRequest
            request.DeclaratieId = DeclaratieID
            If mVSEPDDeclaratieDownloaden.State <> CommunicationState.Opened Then mVSEPDDeclaratieDownloaden.Open()
            Dim respons As wsVSPEDPDeclaratieDownloaden.DownloadResponse = mVSEPDDeclaratieDownloaden.Download(request)
            If Not respons Is Nothing Then
                aResultaat.SetResultaatcode(respons.Resultaatcode.ToString)
                If Not respons.EIRetourbestand Is Nothing Then
                    Dim x As wsVSPEDPDeclaratieDownloaden.EIRetourbestand = respons.EIRetourbestand
                    If Not x.Bestand Is Nothing Then
                        aResultaat.RetourBestandsgrootte = x.Bestand.Bestandsgrootte
                        aResultaat.RetourBestandsnaam = x.Bestand.Bestandsnaam

                        Dim archive As New MemoryStream(x.Bestand.Data)
                        If Ionic.Zip.ZipFile.IsZipFile(archive, True) Then
                            Dim mems As MemoryStream = New MemoryStream()
                            Dim oZipArchive As ZipArchive = New ZipArchive(archive)
                            For Each oZipArchiveEntry As ZipArchiveEntry In oZipArchive.Entries
                                Dim memsXX As Stream = oZipArchiveEntry.Open()
                                memsXX.CopyTo(mems, x.Bestand.Bestandsgrootte)
                                mems.Position = 0
                            Next
                            aResultaat.RetourData = mems.ToArray
                        End If
                    End If
                    If Not x.EIStandaard Is Nothing Then
                        aResultaat.StandaardCEI = x.EIStandaard.StandaardCEI
                        aResultaat.StandaardCode = x.EIStandaard.StandaardCode
                        aResultaat.Standaardsubversie = x.EIStandaard.StandaardSubVersie
                        aResultaat.Standaardversie = x.EIStandaard.StandaardVersie
                    End If

                    aResultaat.EmailAdresZorgverzekeraar = x.EmailAdresZorgverzekeraar
                    aResultaat.GoedgekeurdOp = x.GoedgekeurdOp
                    aResultaat.IngediendOp = x.IngediendOp
                    aResultaat.ReferentieZorgverzekeraar = x.ReferentieZorgverzekeraar
                    aResultaat.RetourbestandId = x.RetourbestandId
                    aResultaat.RetourbestandStatus = x.RetourbestandStatus
                End If
            End If
        Catch ex As Exception
            aResultaat = Nothing
        End Try

        Return aResultaat
    End Function

    Public Function ProcessDeclaratieDownloaden(aDeclaratieDownloadenResultaat As DeclaratieDownloadenResultaat) As Resultaat
        Dim dbCommand As New SqlCommand
        Dim dbReader As SqlDataReader
        Dim aResultaat As New Resultaat

        Try
            If dbConnection.State = ConnectionState.Closed Then dbConnection.Open()
            dbCommand.Connection = dbConnection

            'dbCommand = New SqlCommand("SELECT [VecozoBatchID], [LocationID], [Batchdate], [VecozoBatchSendmethodID], [MessageStandardID], [Status], [VecozoDeclaratieID] , [Period], [Bookyear], [Senddate], [Returndate], [Remark], [Processed], [ReturnFileURL], [ValidateDate], [ValidateReturnDate], [LocationAGB], [VecozoValidatieID] FROM [VecozoBatch] WHERE VecozoBatchID = " & aDeclaratieIndienenResultaat.VecozoBatchID.ToString, dbConnection)
            dbCommand = New SqlCommand("SELECT [VecozoBatchID], [LocationID], [Batchdate], [MessageStandardID], [Period], [Bookyear], [Remark], [Processed], [LocationAGB], [ValidateDate], [ValidateReturnDate], [VecozoValidatieID], [Senddate] FROM [VecozoBatch] WHERE VecozoBatchID = " & aDeclaratieDownloadenResultaat.VecozoBatchID.ToString, dbConnection)
            dbReader = dbCommand.ExecuteReader()
            If dbReader.Read() Then
                Dim sqlupdate As System.Data.SqlClient.SqlCommand
                sqlupdate = New System.Data.SqlClient.SqlCommand("sp_update_VecozoBatch", dbConnection)
                sqlupdate.CommandType = CommandType.StoredProcedure
                sqlupdate.Parameters.Add("@VecozoBatchID", SqlDbType.BigInt).Value = aDeclaratieDownloadenResultaat.VecozoBatchID
                sqlupdate.Parameters.Add("@LocationID", SqlDbType.BigInt).Value = dbReader.GetValue(dbReader.GetOrdinal("LocationID"))
                sqlupdate.Parameters.Add("@Batchdate", SqlDbType.Date).Value = dbReader.GetValue(dbReader.GetOrdinal("Batchdate"))
                sqlupdate.Parameters.Add("@VecozoBatchSendmethodID", SqlDbType.BigInt).Value = 1
                sqlupdate.Parameters.Add("@MessageStandardID", SqlDbType.BigInt).Value = dbReader.GetValue(dbReader.GetOrdinal("MessageStandardID"))
                'TODO Status
                sqlupdate.Parameters.Add("@Status", SqlDbType.BigInt).Value = 2
                sqlupdate.Parameters.Add("@VecozoDeclaratieID", SqlDbType.NVarChar, 50).Value = dbReader.GetValue(dbReader.GetOrdinal("VecozoDeclaratieID"))
                sqlupdate.Parameters.Add("@Period", SqlDbType.BigInt).Value = dbReader.GetValue(dbReader.GetOrdinal("Period"))
                sqlupdate.Parameters.Add("@Bookyear", SqlDbType.BigInt).Value = dbReader.GetValue(dbReader.GetOrdinal("Bookyear"))
                sqlupdate.Parameters.Add("@Senddate", SqlDbType.Date).Value = dbReader.GetValue(dbReader.GetOrdinal("Senddate"))
                sqlupdate.Parameters.Add("@Returndate", SqlDbType.Date).Value = Date.Now()
                sqlupdate.Parameters.Add("@Remarks", SqlDbType.NVarChar, -1).Value = dbReader.GetValue(dbReader.GetOrdinal("Remark"))
                sqlupdate.Parameters.Add("@Processed", SqlDbType.NVarChar, 1).Value = dbReader.GetValue(dbReader.GetOrdinal("Processed"))
                sqlupdate.Parameters.Add("@ReturnFileURL", SqlDbType.NVarChar, -1).Value = DBNull.Value
                sqlupdate.Parameters.Add("@UpdateUserID", SqlDbType.NVarChar, 8).Value = "YOKARIBA"
                sqlupdate.Parameters.Add("@ValidateDate", SqlDbType.Date).Value = dbReader.GetValue(dbReader.GetOrdinal("ValidateDate"))
                sqlupdate.Parameters.Add("@ValidateReturnDate", SqlDbType.Date).Value = dbReader.GetValue(dbReader.GetOrdinal("ValidateReturnDate"))
                sqlupdate.Parameters.Add("@LocationAGB", SqlDbType.NVarChar, 8).Value = dbReader.GetValue(dbReader.GetOrdinal("LocationAGB"))
                sqlupdate.Parameters.Add("@VecozoValidatieID", SqlDbType.NVarChar, 50).Value = dbReader.GetValue(dbReader.GetOrdinal("VecozoValidatieID"))
                rows = sqlupdate.ExecuteNonQuery()

                If aDeclaratieDownloadenResultaat.RetourbestandStatus = DeclaratieDownloadenResultaat.RetourbestandStatusType.AfgekeurdDoorVecozo Then
                    sqlupdate = New System.Data.SqlClient.SqlCommand("sp_add_VecozoFile", dbConnection)
                    sqlupdate.CommandType = CommandType.StoredProcedure
                    sqlupdate.Parameters.Add("@VecozoBatchID", SqlDbType.BigInt).Value = aDeclaratieDownloadenResultaat.VecozoBatchID
                    sqlupdate.Parameters.Add("@VecozoFileTypeID", SqlDbType.BigInt).Value = 1
                    sqlupdate.Parameters.Add("@Filename", SqlDbType.NVarChar, 200).Value = aDeclaratieDownloadenResultaat.RetourBestandsnaam
                    sqlupdate.Parameters.Add("@Filedate", SqlDbType.DateTime).Value = DateTime.Now
                    sqlupdate.Parameters.Add("@File", SqlDbType.NVarChar, -1).Value = System.Text.Encoding.ASCII.GetString(aDeclaratieDownloadenResultaat.RetourData)
                    sqlupdate.Parameters.Add("@FileID", SqlDbType.BigInt).Value = DBNull.Value
                    sqlupdate.Parameters.Add("@Remark", SqlDbType.NVarChar, -1).Value = DBNull.Value
                    sqlupdate.Parameters.Add("@UpdateUserID", SqlDbType.NVarChar, 8).Value = "YOKARIBA"
                    sqlupdate.Parameters.Add("@VecozoFileID", SqlDbType.BigInt)
                    sqlupdate.Parameters("@VecozoFileID").Direction = ParameterDirection.Output
                    rows = sqlupdate.ExecuteNonQuery()
                Else
                    sqlupdate = New System.Data.SqlClient.SqlCommand("sp_delete_VecozoNotification", dbConnection)
                    sqlupdate.CommandType = CommandType.StoredProcedure
                    sqlupdate.Parameters.Add("@VecozoBatchID", SqlDbType.BigInt).Value = aDeclaratieDownloadenResultaat.VecozoBatchID
                    sqlupdate.Parameters.Add("@VecozoFileTypeID", SqlDbType.BigInt).Value = 1
                    sqlupdate.Parameters.Add("@UpdateUserID", SqlDbType.NVarChar, 8).Value = "YOKARIBA"
                    rows = sqlupdate.ExecuteNonQuery()
                End If

                'Insert Meldingen
                'Verwijder eerst oude
                sqlupdate = New System.Data.SqlClient.SqlCommand("sp_delete_VecozoNotification", dbConnection)
                sqlupdate.CommandType = CommandType.StoredProcedure
                sqlupdate.Parameters.Add("@VecozoBatchID", SqlDbType.BigInt).Value = aDeclaratieDownloadenResultaat.VecozoBatchID
                sqlupdate.Parameters.Add("@VecozoFileTypeID", SqlDbType.BigInt).Value = 1
                sqlupdate.Parameters.Add("@UpdateUserID", SqlDbType.NVarChar, 8).Value = "YOKARIBA"
                rows = sqlupdate.ExecuteNonQuery()

                For Each item As DeclaratieStatusResultaat.Melding In aDeclaratieDownloadenResultaat.Meldingen
                    If item.MeldingSoort <> Resultaat.MeldingBronType.None Then
                        sqlupdate = New System.Data.SqlClient.SqlCommand("sp_add_VecozoNotification", dbConnection)
                        sqlupdate.CommandType = CommandType.StoredProcedure
                        sqlupdate.Parameters.Add("@VecozoBatchID", SqlDbType.BigInt).Value = aDeclaratieDownloadenResultaat.VecozoBatchID
                        sqlupdate.Parameters.Add("@VecozoFileTypeID", SqlDbType.BigInt).Value = 1
                        sqlupdate.Parameters.Add("@VecozoBatchLinenumber", SqlDbType.Int).Value = item.VektisMeldingVecozoBatchLinenummer
                        sqlupdate.Parameters.Add("@VecozoNotificationSource", SqlDbType.NVarChar, 10).Value = If(item.MeldingSoort = Resultaat.MeldingBronType.Vecozo, "Vecozo", "Vektis")
                        sqlupdate.Parameters.Add("@Code", SqlDbType.NVarChar, 10).Value = item.Code
                        sqlupdate.Parameters.Add("@Description", SqlDbType.NVarChar, 255).Value = item.Omschrijving
                        sqlupdate.Parameters.Add("@KenmerkRecord", SqlDbType.Int).Value = item.VektisMeldingKenmerkRecord
                        sqlupdate.Parameters.Add("@IdentificatieDetailRecord", SqlDbType.BigInt).Value = item.VektisMeldingIdentificatieDetailRecord
                        sqlupdate.Parameters.Add("@Regelnummer", SqlDbType.Int).Value = item.VektisMeldingRegelnummer
                        sqlupdate.Parameters.Add("@Rubrieknummer", SqlDbType.Int).Value = item.VektisMeldingRubrieknummer
                        sqlupdate.Parameters.Add("@Remark", SqlDbType.NVarChar, -1).Value = DBNull.Value
                        sqlupdate.Parameters.Add("@UpdateUserID", SqlDbType.NVarChar, 8).Value = "YOKARIBA"
                        sqlupdate.Parameters.Add("@VecozoNotificationID", SqlDbType.BigInt)
                        sqlupdate.Parameters("@VecozoNotificationID").Direction = ParameterDirection.Output
                        rows = sqlupdate.ExecuteNonQuery()
                    End If
                Next
                aResultaat.Status = Resultaat.StatusType.Succes
            Else
                aResultaat.Status = Resultaat.StatusType.Fout
                aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_NFOUND
            End If
            dbReader.Close()
        Catch e As Exception
            aResultaat.Status = Resultaat.StatusType.Fout
            aResultaat.MeldingBron = Resultaat.MeldingBronType.I2M
            aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_OTH
            aResultaat.ResultaatExtra = e.Message
        Finally
            If dbConnection.State = ConnectionState.Open Then dbConnection.Close()
        End Try

        Return aResultaat
    End Function

    Public Sub Dispose()
        Try
            mVSEPDDeclaratieDownloaden.Close()
        Finally
            mVSEPDDeclaratieDownloaden = Nothing
        End Try
    End Sub
End Class