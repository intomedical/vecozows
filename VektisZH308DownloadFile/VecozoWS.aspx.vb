﻿Imports System
Imports System.IO
Imports System.IO.Compression
Imports Ionic.Zip
Imports DevExpress.Web

Public Class VecozoWS
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    End Sub

    'Valideer Validatie
    Protected Sub btnWSValideerZH308_Click(sender As Object, e As EventArgs) Handles btnWSValideerZH308.Click
        Dim Message As String = ""
        Dim VecozoBatchID As Long
        Dim selectedValues As List(Of Object)

        'TODO - controle rol
        'TODO - Audittrail
        'Vecozobatchid of batchnummer?

        Dim oVSPEDPValidatieIndienen As New VSPEDPValidatieIndienen()
        If oVSPEDPValidatieIndienen.LastResultaat Is NotNothing Then
            Message += VecozoBatchID.ToString + oVSPEDPValidatieIndienen.LastResultaat.StatusText + oVSPEDPValidatieIndienen.LastResultaat.ResultaatCodeText + " " + oVSPEDPValidatieIndienen.LastResultaat.ResultaatExtra + vbCrLf
            Response.Write("<script type=""text/javascript"">alert(""" + Message + """);</script")
            Exit Sub
        End If

        Dim fieldNames As List(Of String) = New List(Of String)()
        For Each column As GridViewColumn In ASPxGridView1.Columns
            If TypeOf column Is GridViewDataColumn Then
                fieldNames.Add((CType(column, GridViewDataColumn)).FieldName)
            End If
        Next column
        selectedValues = ASPxGridView1.GetSelectedFieldValues(fieldNames.ToArray())

        Dim i As Integer = 0
        Dim aFile As String = ""
        If selectedValues.Count > 0 Then
            Message = ""
            For Each selectedvalue As Object() In selectedValues
                VecozoBatchID = DirectCast(selectedvalue(0), Long)
                Dim respons As VSPEDPValidatieIndienen.ValidatieIndienenResultaat = oVSPEDPValidatieIndienen.ValidatieIndienen(VecozoBatchID)
                If respons.Status = VSPEDP.Resultaat.StatusType.Succes Then
                    Dim responsProcessValidatieIndienen As VSPEDPValidatieIndienen.Resultaat = oVSPEDPValidatieIndienen.ProcessValidatieIndienen(respons)
                    If responsProcessValidatieIndienen.Status = VSPEDP.Resultaat.StatusType.Succes Then
                        Message += VecozoBatchID.ToString & responsProcessValidatieIndienen.ResultaatCodeText + " " + responsProcessValidatieIndienen.ResultaatExtra + vbCrLf
                    Else
                        Message += VecozoBatchID.ToString + responsProcessValidatieIndienen.StatusText + responsProcessValidatieIndienen.ResultaatCodeText + " " + responsProcessValidatieIndienen.ResultaatExtra + vbCrLf
                    End If
                Else
                    Message += VecozoBatchID.ToString + respons.StatusText + respons.ResultaatCodeText + " " + respons.ResultaatExtra + vbCrLf
                End If
                i += 1
            Next
        Else
            Message = "Selecteer minstens een regel."
        End If

        If Message <> "" Then
            Response.Write("<script type=""text/javascript"">alert(""" + Message + """);</script")
        End If




        'Dim memStream As MemoryStream = VektisZH308.GetVektisZH308File(Integer.Parse(txtVecozoBatchID.Text))
        'Dim aFile As String = "AY12112.txt"
        'Dim bData As Byte()
        'Dim br As BinaryReader = New BinaryReader(System.IO.File.OpenRead("c:\temp\" & aFile))
        'bData = br.ReadBytes(CInt(br.BaseStream.Length))
        'Dim memStream As MemoryStream = New MemoryStream(bData, 0, bData.Length)
        'memStream.Write(bData, 0, bData.Length)
        'br.Close()

        'If memStream IsNot Nothing Then
        '    Dim respons As VSPEDPValidatieIndienen.ValidatieIndienenResultaat = oVSPEDPValidatieIndienen.ValidatieIndienen(memStream, aFile)
        '    'Dim file As FileStream = New FileStream("c:\temp\" + txtVecozoBatchID.Text + ".txt", FileMode.Create, FileAccess.Write)
        '    txtResultaatIndienenValidatie.Text = respons.DebugText
        '    memStream.Close()
        'End If
    End Sub

    'TODO: Messages beter formateren
    'Download Validatie
    Protected Sub btnWSValideerZH309_Click(sender As Object, e As EventArgs) Handles btnWSValideerZH309.Click
        Dim Message As String = ""
        Dim VecozoBatchID As Long
        Dim selectedValues As List(Of Object)

        Dim oVSPEDPValidatieDownloaden As New VSEDPValidatieDownloaden()
        If oVSPEDPValidatieDownloaden.LastResultaat IsNot Nothing Then
            Message += "VecozoBatchID: " + VecozoBatchID.ToString + " - "
            Message += VecozoBatchID.ToString + oVSPEDPValidatieDownloaden.LastResultaat.StatusText + oVSPEDPValidatieDownloaden.LastResultaat.ResultaatCodeText + " " + oVSPEDPValidatieDownloaden.LastResultaat.ResultaatExtra + vbCrLf
            Response.Write("<script type=""text/javascript"">alert(""" + Message + """);</script")
            Exit Sub
        End If

        Dim fieldNames As List(Of String) = New List(Of String)()
        For Each column As GridViewColumn In ASPxGridView1.Columns
            If TypeOf column Is GridViewDataColumn Then
                fieldNames.Add((CType(column, GridViewDataColumn)).FieldName)
            End If
        Next column
        selectedValues = ASPxGridView1.GetSelectedFieldValues(fieldNames.ToArray())

        Dim i As Integer = 0
        Dim aFile As String = ""
        If selectedValues.Count > 0 Then
            Message = ""
            For Each selectedvalue As Object() In selectedValues
                VecozoBatchID = DirectCast(selectedvalue(0), Long)
                Dim responsValidatieDownloaden As VSEDPValidatieDownloaden.ValidatieDownloadenResultaat = oVSPEDPValidatieDownloaden.ValidatieDownloaden(VecozoBatchID)
                Message += "VecozoBatchID: " + VecozoBatchID.ToString + " - "
                If responsValidatieDownloaden.Status = VSPEDP.Resultaat.StatusType.Succes Then
                    Message += "Validatiestatus: " + responsValidatieDownloaden.ValidatieStatusText + " " + vbCrLf
                    If responsValidatieDownloaden.ValidatieStatus <> VSEDPValidatieDownloaden.ValidatieDownloadenResultaat.ValidatieStatusType.Ontvangen Then
                        Dim responsProcessValidatieDownloaden As VSEDPValidatieDownloaden.Resultaat = oVSPEDPValidatieDownloaden.ProcessValidatieDownloaden(responsValidatieDownloaden)
                    End If
                Else
                    Message += responsValidatieDownloaden.ResultaatCodeText + vbCrLf
                End If
                i += 1
            Next
        Else
            Message = "Selecteer minstens een regel."
        End If

        txtResultaatIDowloadenValidatie.Text = Message

        'If Message <> "" Then
        '    Response.Write("<script type=""text/javascript"">alert(""" + Message + """);</script")
        'End If
    End Sub

    Protected Sub btnWSIndienenDeclaratie_Click(sender As Object, e As EventArgs) Handles btnWSIndienenDeclaratie.Click
        Dim Message As String = ""
        Dim VecozoBatchID As Long
        Dim selectedValues As List(Of Object)

        'TODO - controle rol
        'TODO - Audittrail
        'Vecozobatchid of batchnummer?

        Dim oVSPEDPDeclaratieIndienen As New VSPEDPDeclaratieIndienen()
        If oVSPEDPDeclaratieIndienen.LastResultaat IsNot Nothing Then
            Message += oVSPEDPDeclaratieIndienen.LastResultaat.StatusText + " - " + oVSPEDPDeclaratieIndienen.LastResultaat.ResultaatCodeText + " " + If(oVSPEDPDeclaratieIndienen.LastResultaat.ResultaatExtra = "", "", " - ") + oVSPEDPDeclaratieIndienen.LastResultaat.ResultaatExtra + vbCrLf
            txtResultaatIndienenValidatie.Text = Message
            Exit Sub
        End If

        Dim fieldNames As List(Of String) = New List(Of String)()
        For Each column As GridViewColumn In ASPxGridView1.Columns
            If TypeOf column Is GridViewDataColumn Then
                fieldNames.Add((CType(column, GridViewDataColumn)).FieldName)
            End If
        Next column
        selectedValues = ASPxGridView1.GetSelectedFieldValues(fieldNames.ToArray())

        Dim i As Integer = 0
        Dim aFile As String = ""
        If selectedValues.Count > 0 Then
            Message = ""
            For Each selectedvalue As Object() In selectedValues
                VecozoBatchID = DirectCast(selectedvalue(0), Long)
                Message += "VecozoBatchID: " + VecozoBatchID.ToString + " - "
                Dim responsDeclaratieIndienenResultaat As VSPEDPDeclaratieIndienen.DeclaratieIndienenResultaat = oVSPEDPDeclaratieIndienen.DeclaratieIndienen(VecozoBatchID)
                If responsDeclaratieIndienenResultaat.Status = VSPEDP.Resultaat.StatusType.Succes Then
                    Message += "Status: " + responsDeclaratieIndienenResultaat.StatusText + " - Resultaat: " + responsDeclaratieIndienenResultaat.ResultaatCodeText + If(responsDeclaratieIndienenResultaat.ResultaatExtra = "", "", " - ") + responsDeclaratieIndienenResultaat.ResultaatExtra + vbCrLf
                    Dim responsProcessDeclaratieIndienen As VSPEDPDeclaratieIndienen.Resultaat = oVSPEDPDeclaratieIndienen.ProcessDeclaratieIndienen(responsDeclaratieIndienenResultaat)
                    If responsProcessDeclaratieIndienen.Status = VSPEDP.Resultaat.StatusType.Succes Then
                        Message += VecozoBatchID.ToString & responsProcessDeclaratieIndienen.ResultaatCodeText + " " + responsProcessDeclaratieIndienen.ResultaatExtra + vbCrLf
                    Else
                        Message += VecozoBatchID.ToString + responsProcessDeclaratieIndienen.StatusText + responsProcessDeclaratieIndienen.ResultaatCodeText + " " + responsProcessDeclaratieIndienen.ResultaatExtra + vbCrLf
                    End If
                Else
                    Message += "Status: " + responsDeclaratieIndienenResultaat.StatusText + " - Resultaat: " + responsDeclaratieIndienenResultaat.ResultaatCodeText + If(responsDeclaratieIndienenResultaat.ResultaatExtra = "", "", " - ") + responsDeclaratieIndienenResultaat.ResultaatExtra + vbCrLf
                End If
                i += 1
            Next
        Else
            Message = "Selecteer minstens een regel."
        End If

        txtResultaatIndienenValidatie.Text = Message

        'Dim aFile As String = "ZH308-basisbestand-correct.txt"
        'Dim bData As Byte()
        'Dim br As BinaryReader = New BinaryReader(System.IO.File.OpenRead("C:\Dropbox\Intomedical\Vecozo\ZH308\" & aFile))
        'bData = br.ReadBytes(CInt(br.BaseStream.Length))
        'Dim memStream As MemoryStream = New MemoryStream(bData, 0, bData.Length)
        'memStream.Write(bData, 0, bData.Length)
        'br.Close()

        'If memStream IsNot Nothing Then
        '    Dim respons As VSPEDPDeclaratieIndienen.DeclaratieIndienenResultaat = oVSPEDPDeclaratieIndienen.DeclaratieIndienen(memStream, aFile)
        '    txtResultaatIDowloadenValidatie.Text = respons.DebugText
        '    memStream.Close()
        'End If
    End Sub

    Protected Sub btnWSDownloadDeclaratie_Click(sender As Object, e As EventArgs) Handles btnWSDownloadDeclaratie.Click
        Dim oVSPEDPDeclaratie As New VSEDPDeclaratieDownloaden()
        'AY     Dim respons As VSEDPDeclaratieDownloaden.DeclaratieDownloadenResultaat = oVSPEDPDeclaratie.DeclaratieDownloaden(Integer.Parse(txtValidatieIDforWS.Text))
        'If respons IsNot Nothing Then
        'txtResultaatIDowloadenValidatie.Text = respons.DebugText
        'Else
        'txtResultaatIDowloadenValidatie.Text = "ONBEKENDE FOUT"
        'End If
    End Sub

    Protected Sub btnWSDeclaratieStatus_Click(sender As Object, e As EventArgs) Handles btnWSDeclaratieStatus.Click
        Dim Message As String = ""
        Dim VecozoBatchID As Long
        Dim selectedValues As List(Of Object)

        'TODO - controle rol
        'TODO - Audittrail
        'Vecozobatchid of batchnummer?

        Dim oVSPEDPDeclaratieDownloaden As New VSEDPDeclaratieDownloaden()
        If oVSPEDPDeclaratieDownloaden.LastResultaat IsNot Nothing Then
            Message += oVSPEDPDeclaratieDownloaden.LastResultaat.StatusText + " - " + oVSPEDPDeclaratieDownloaden.LastResultaat.ResultaatCodeText + " " + If(oVSPEDPDeclaratieDownloaden.LastResultaat.ResultaatExtra = "", "", " - ") + oVSPEDPDeclaratieDownloaden.LastResultaat.ResultaatExtra + vbCrLf
            txtResultaatIndienenValidatie.Text = Message
            Exit Sub
        End If

        Dim fieldNames As List(Of String) = New List(Of String)()
        For Each column As GridViewColumn In ASPxGridView1.Columns
            If TypeOf column Is GridViewDataColumn Then
                fieldNames.Add((CType(column, GridViewDataColumn)).FieldName)
            End If
        Next column
        selectedValues = ASPxGridView1.GetSelectedFieldValues(fieldNames.ToArray())

        Dim i As Integer = 0
        Dim aFile As String = ""
        If selectedValues.Count > 0 Then
            Message = ""
            For Each selectedvalue As Object() In selectedValues
                VecozoBatchID = DirectCast(selectedvalue(0), Long)
                Message += "VecozoBatchID: " + VecozoBatchID.ToString + " - "
                Dim responsDeclaratieStatusResultaat As VSEDPDeclaratieDownloaden.DeclaratieStatusResultaat = oVSPEDPDeclaratieDownloaden.DeclaratieStatus(VecozoBatchID)
                Message += "Declaratiestatus: " + responsDeclaratieStatusResultaat.DeclaratieStatusText + " " + vbCrLf
                If responsDeclaratieStatusResultaat.Status = VSPEDP.Resultaat.StatusType.Succes Then
                    If responsDeclaratieStatusResultaat.DeclaratieStatus = VSEDPDeclaratieDownloaden.DeclaratieStatusResultaat.DeclaratieStatusType.AfgehandeldDoorZorgverzekeraarGemeente Or
                        responsDeclaratieStatusResultaat.DeclaratieStatus = VSEDPDeclaratieDownloaden.DeclaratieStatusResultaat.DeclaratieStatusType.AfgekeurdDoorVecozo Then
                        Dim responsDeclaratieDownloadenResultaat As VSEDPDeclaratieDownloaden.DeclaratieDownloadenResultaat = oVSPEDPDeclaratieDownloaden.DeclaratieDownloaden(responsDeclaratieStatusResultaat.DeclaratieID)
                        If responsDeclaratieDownloadenResultaat.Status = VSPEDP.Resultaat.StatusType.Succes Then
                            Message += VecozoBatchID.ToString & responsDeclaratieDownloadenResultaat.ResultaatCodeText + " " + responsDeclaratieDownloadenResultaat.ResultaatExtra + vbCrLf
                            Dim responsProcessDeclaratieDownloaden As VSEDPDeclaratieDownloaden.Resultaat = oVSPEDPDeclaratieDownloaden.ProcessDeclaratieDownloaden(responsDeclaratieDownloadenResultaat)
                            If responsProcessDeclaratieDownloaden.Status = VSPEDP.Resultaat.StatusType.Succes Then
                                Message += VecozoBatchID.ToString & responsProcessDeclaratieDownloaden.ResultaatCodeText + " " + responsProcessDeclaratieDownloaden.ResultaatExtra + vbCrLf
                            Else
                                Message += VecozoBatchID.ToString + responsProcessDeclaratieDownloaden.StatusText + responsProcessDeclaratieDownloaden.ResultaatCodeText + " " + responsProcessDeclaratieDownloaden.ResultaatExtra + vbCrLf
                            End If
                        Else
                            Message += VecozoBatchID.ToString + responsDeclaratieDownloadenResultaat.StatusText + responsDeclaratieDownloadenResultaat.ResultaatCodeText + " " + responsDeclaratieDownloadenResultaat.ResultaatExtra + vbCrLf
                        End If

                    End If

                        '                    Dim responsProcessDeclaratieIndienen As VSPEDPDeclaratieIndienen.Resultaat = oVSPEDPDeclaratieIndienen.ProcessDeclaratieIndienen(responsDeclaratieIndienenResultaat)
                        '                    If responsProcessDeclaratieIndienen.Status = VSPEDP.Resultaat.StatusType.Succes Then
                        '                    Message += VecozoBatchID.ToString & responsProcessDeclaratieIndienen.ResultaatCodeText + " " + responsProcessDeclaratieIndienen.ResultaatExtra + vbCrLf
                        '               Else
                        '                   Message += VecozoBatchID.ToString + responsProcessDeclaratieIndienen.StatusText + responsProcessDeclaratieIndienen.ResultaatCodeText + " " + responsProcessDeclaratieIndienen.ResultaatExtra + vbCrLf
                        '               End If
                    Else
                    Message += "Status: " + responsDeclaratieStatusResultaat.StatusText + " - Resultaat: " + responsDeclaratieStatusResultaat.ResultaatCodeText + If(responsDeclaratieStatusResultaat.ResultaatExtra = "", "", " - ") + responsDeclaratieStatusResultaat.ResultaatExtra + vbCrLf
                End If
                i += 1
            Next
        Else
            Message = "Selecteer minstens een regel."
        End If

        txtResultaatIndienenValidatie.Text = Message
    End Sub
End Class