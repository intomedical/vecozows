﻿Option Strict Off

Imports System.ServiceModel
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Drawing
Imports System
Imports System.Data
Imports System.Configuration

Public Class Grouper
    Inherits SuperClass

    Dim dbTransaction As SqlTransaction
    Dim mClientID As Integer
    Dim mClinicID As Integer
    Dim mUser As String

    Public Class GrouperResult
        Dim mDeclaratieresultsetnummer As String
        Dim mDeclaratiedatasetnummer As String
        Dim mDISHashtotal As String
        Dim mDISHashCalc As String
        Dim mDISHashString As String
        Dim mDISHashversie As String
        Dim mTabelsetversie As Date
        Dim mGrouperversie As String
        Dim mZorgproductcode As String
        Dim mDeclaratiecode As String
        Dim mAanspraakZVWToegepast As Boolean
        Dim mZAMetMachtiging As Boolean
        Dim mOranjeZAInProfiel As Boolean
        Dim mZAVertaling As Boolean
        Dim mZVZPHashtotal As String
        Dim mZVZPHashCalc As String
        Dim mZVZPHashString As String
        Dim mZVZPHashversie As String
        Dim mZorgactiviteitOpNota As List(Of GrouperZorgactiviteitOpNota)
        Dim mZorgactiviteitDeclaratie As List(Of GrouperZorgactiviteitDeclaratie)
        Dim mMelding As List(Of GrouperMelding)

        Public Sub New()
            mZorgactiviteitOpNota = New List(Of GrouperZorgactiviteitOpNota)
            mZorgactiviteitDeclaratie = New List(Of GrouperZorgactiviteitDeclaratie)
            mMelding = New List(Of GrouperMelding)
        End Sub

        Public Function DebugText() As String
            Dim aMessage As String = ""

            aMessage = aMessage & "Declaratieresultsetnummer = " + mDeclaratieresultsetnummer + vbCrLf
            aMessage = aMessage + "Declaratiedatasetnummer = " + mDeclaratiedatasetnummer + vbCrLf
            aMessage = aMessage + "DIS Hash Total = " + mDISHashtotal + vbCrLf
            aMessage = aMessage + "DIS Hash Calc = " + mDISHashCalc + vbCrLf
            aMessage = aMessage + "DIS Hash String = " + mDISHashString + vbCrLf
            aMessage = aMessage + "DIS Hash Versie = " + mDISHashversie + vbCrLf
            aMessage = aMessage + "Tabelset versie = " + mTabelsetversie.ToString("dd-MM-yyyy") + vbCrLf
            aMessage = aMessage + "Grouper versie = " + mGrouperversie + vbCrLf
            aMessage = aMessage + vbCrLf
            aMessage = aMessage + "Zorgproductdeclaratie:" + vbCrLf
            aMessage = aMessage + "Zorgproductcode = " + mZorgproductcode + vbCrLf
            aMessage = aMessage + "Declaratiecode = " + mDeclaratiecode + vbCrLf
            aMessage = aMessage + "Aanspraak ZVW Toegepast = " + mAanspraakZVWToegepast.ToString + vbCrLf
            aMessage = aMessage + "ZA Met Machtiging = " + mZAMetMachtiging.ToString + vbCrLf
            aMessage = aMessage + "Oranje ZA In Profiel = " + mOranjeZAInProfiel.ToString + vbCrLf
            aMessage = aMessage + "ZA Vertaling = " + mZAVertaling.ToString + vbCrLf
            aMessage = aMessage + "ZVZP Hash Total = " + mZVZPHashtotal + vbCrLf
            aMessage = aMessage + "ZVZP Hash Calc = " + mZVZPHashCalc + vbCrLf
            aMessage = aMessage + "ZVZP Hash String = " + mZVZPHashString + vbCrLf
            aMessage = aMessage + "ZVZP Hash Versie = " + mZVZPHashversie + vbCrLf

            Dim i As Integer = 1
            For Each item As GrouperZorgactiviteitDeclaratie In mZorgactiviteitDeclaratie
                aMessage = aMessage + vbCrLf
                aMessage = aMessage + "Zorgactiviteitdeclaratie " + i.ToString + vbCrLf
                aMessage = aMessage + "Zorgactiviteitnummer " + item.ZorgactiviteitNummer.ToString + vbCrLf
                aMessage = aMessage + "Declaratiecode = " + item.Declaratiecode + vbCrLf
                aMessage = aMessage + "ZVZA Hash Calc = " + item.ZVZAHashCalc + vbCrLf
                aMessage = aMessage + "ZVZA Hash String = " + item.ZVZAHashString + vbCrLf
                aMessage = aMessage + "ZVZA Hash Total = " + item.ZVZAHashtotal + vbCrLf
                aMessage = aMessage + "ZVZA Hash Versie = " + item.ZVZAHashversie + vbCrLf
                i += 1
            Next

            i = 1
            For Each item As GrouperZorgactiviteitOpNota In mZorgactiviteitOpNota
                aMessage = aMessage + vbCrLf
                aMessage = aMessage + "Zorgactiviteit Op Nota " + i.ToString + vbCrLf
                aMessage = aMessage + "ZorgactiviteitNummer = " & item.ZorgactiviteitNummer.ToString & vbCrLf
                i += 1
            Next

            i = 1
            For Each item As GrouperMelding In mMelding
                aMessage = aMessage & vbCrLf
                aMessage = aMessage & "Melding " & i.ToString & vbCrLf
                aMessage = aMessage & "Melding ID = " & item.MeldingID.ToString & vbCrLf
                aMessage = aMessage & "Melding Soort = " & item.MeldingSoort & vbCrLf
                aMessage = aMessage & "Melding Code = " & item.Meldingcode & vbCrLf
                aMessage = aMessage & "Melding Omschrijving = " & item.Meldingomschrijving & vbCrLf
                i += 1
            Next
            Return aMessage
        End Function

        Public Property Declaratieresultsetnummer() As String
            Get
                Return mDeclaratieresultsetnummer
            End Get
            Set(ByVal value As String)
                mDeclaratieresultsetnummer = value
            End Set
        End Property

        Public Property Declaratiedatasetnummer() As String
            Get
                Return mDeclaratiedatasetnummer
            End Get
            Set(ByVal value As String)
                mDeclaratiedatasetnummer = value
            End Set
        End Property

        Public Property DISHashtotal() As String
            Get
                Return mDISHashtotal
            End Get
            Set(ByVal value As String)
                mDISHashtotal = value
            End Set
        End Property

        Public Property DISHashCalc() As String
            Get
                Return mDISHashCalc
            End Get
            Set(ByVal value As String)
                mDISHashCalc = value
            End Set
        End Property

        Public Property DISHashString() As String
            Get
                Return mDISHashString
            End Get
            Set(ByVal value As String)
                mDISHashString = value
            End Set
        End Property

        Public Property DISHashversie() As String
            Get
                Return mDISHashversie
            End Get
            Set(ByVal value As String)
                mDISHashversie = value
            End Set
        End Property

        Public Property Tabelsetversie() As Date
            Get
                Return mTabelsetversie
            End Get
            Set(ByVal value As Date)
                mTabelsetversie = value
            End Set
        End Property

        Public Property Grouperversie() As String
            Get
                Return mGrouperversie
            End Get
            Set(ByVal value As String)
                mGrouperversie = value
            End Set
        End Property

        Public Property Zorgproductcode() As String
            Get
                Return mZorgproductcode
            End Get
            Set(ByVal value As String)
                mZorgproductcode = value
            End Set
        End Property

        Public Property Declaratiecode() As String
            Get
                Return mDeclaratiecode
            End Get
            Set(ByVal value As String)
                mDeclaratiecode = value
            End Set
        End Property

        Public Property AanspraakZVWToegepast() As Boolean
            Get
                Return mAanspraakZVWToegepast
            End Get
            Set(ByVal value As Boolean)
                mAanspraakZVWToegepast = value
            End Set
        End Property

        Public Property ZAMetMachtiging() As Boolean
            Get
                Return mZAMetMachtiging
            End Get
            Set(ByVal value As Boolean)
                mZAMetMachtiging = value
            End Set
        End Property

        Public Property ZAVertaling() As Boolean
            Get
                Return mZAVertaling
            End Get
            Set(ByVal value As Boolean)
                mZAVertaling = value
            End Set
        End Property

        Public Property OranjeZAInProfiel() As Boolean
            Get
                Return mOranjeZAInProfiel
            End Get
            Set(ByVal value As Boolean)
                mOranjeZAInProfiel = value
            End Set
        End Property

        Public Property ZVZPHashtotal() As String
            Get
                Return mZVZPHashtotal
            End Get
            Set(ByVal value As String)
                mZVZPHashtotal = value
            End Set
        End Property

        Public Property ZVZPHashCalc() As String
            Get
                Return mZVZPHashCalc
            End Get
            Set(ByVal value As String)
                mZVZPHashCalc = value
            End Set
        End Property

        Public Property ZVZPHashString() As String
            Get
                Return mZVZPHashString
            End Get
            Set(ByVal value As String)
                mZVZPHashString = value
            End Set
        End Property

        Public Property ZVZPHashversie() As String
            Get
                Return mZVZPHashversie
            End Get
            Set(ByVal value As String)
                mZVZPHashversie = value
            End Set
        End Property

        Public Property ZorgactiviteitOpNota() As List(Of GrouperZorgactiviteitOpNota)
            Get
                Return mZorgactiviteitOpNota
            End Get
            Set(ByVal value As List(Of GrouperZorgactiviteitOpNota))
                mZorgactiviteitOpNota = value
            End Set
        End Property

        Public Property ZorgactiviteitDeclaratie() As List(Of GrouperZorgactiviteitDeclaratie)
            Get
                Return mZorgactiviteitDeclaratie
            End Get
            Set(ByVal value As List(Of GrouperZorgactiviteitDeclaratie))
                mZorgactiviteitDeclaratie = value
            End Set
        End Property

        Public Property Melding() As List(Of GrouperMelding)
            Get
                Return mMelding
            End Get
            Set(ByVal value As List(Of GrouperMelding))
                mMelding = value
            End Set
        End Property

        Friend Function AddZorgactiviteitOpNota(ByVal ZorgactiviteitNummer As Integer) As Boolean
            Dim aZorgactiviteitOpNota As New GrouperZorgactiviteitOpNota

            aZorgactiviteitOpNota.ZorgactiviteitNummer = ZorgactiviteitNummer

            mZorgactiviteitOpNota.Add(aZorgactiviteitOpNota)

            Return True
        End Function

        Friend Function AddZorgactiviteitDeclaratie(ByVal ZorgactiviteitNummer As Integer, ByVal Declaratiecode As String, ByVal ZVZAHashCalc As String, ByVal ZVZAHashString As String, ByVal ZVZAHashtotal As String, ByVal ZVZAHashversie As String) As Boolean
            Dim aZorgactiviteitDeclaratie As New GrouperZorgactiviteitDeclaratie

            aZorgactiviteitDeclaratie.ZorgactiviteitNummer = ZorgactiviteitNummer
            aZorgactiviteitDeclaratie.Declaratiecode = Declaratiecode
            aZorgactiviteitDeclaratie.ZVZAHashCalc = ZVZAHashCalc
            aZorgactiviteitDeclaratie.ZVZAHashString = ZVZAHashString
            aZorgactiviteitDeclaratie.ZVZAHashtotal = ZVZAHashtotal
            aZorgactiviteitDeclaratie.ZVZAHashversie = ZVZAHashversie

            mZorgactiviteitDeclaratie.Add(aZorgactiviteitDeclaratie)

            Return True
        End Function

        Friend Function AddMelding(ByVal Meldingcode As String, ByVal MeldingID As Integer, ByVal Meldingomschrijving As String, ByVal MeldingSoort As String) As Boolean
            Dim aMelding As New GrouperMelding

            aMelding.Meldingcode = Meldingcode
            aMelding.MeldingID = MeldingID
            aMelding.Meldingomschrijving = Meldingomschrijving
            aMelding.MeldingSoort = MeldingSoort

            mMelding.Add(aMelding)

            Return True
        End Function

        Public Class GrouperZorgactiviteitOpNota
            Dim mZorgactiviteitNummer As Integer

            Public Property ZorgactiviteitNummer() As Integer
                Get
                    Return mZorgactiviteitNummer
                End Get
                Set(ByVal value As Integer)
                    mZorgactiviteitNummer = value
                End Set
            End Property
        End Class

        Public Class GrouperZorgactiviteitDeclaratie
            Dim mZorgactiviteitNummer As Integer
            Dim mDeclaratiecode As String
            Dim mZVZAHashtotal As String
            Dim mZVZAHashCalc As String
            Dim mZVZAHashString As String
            Dim mZVZAHashversie As String

            Public Property ZorgactiviteitNummer() As Integer
                Get
                    Return mZorgactiviteitNummer
                End Get
                Set(ByVal value As Integer)
                    mZorgactiviteitNummer = value
                End Set
            End Property

            Public Property Declaratiecode() As String
                Get
                    Return mDeclaratiecode
                End Get
                Set(ByVal value As String)
                    mDeclaratiecode = value
                End Set
            End Property

            Public Property ZVZAHashtotal() As String
                Get
                    Return mZVZAHashtotal
                End Get
                Set(ByVal value As String)
                    mZVZAHashtotal = value
                End Set
            End Property

            Public Property ZVZAHashCalc() As String
                Get
                    Return mZVZAHashCalc
                End Get
                Set(ByVal value As String)
                    mZVZAHashCalc = value
                End Set
            End Property

            Public Property ZVZAHashString() As String
                Get
                    Return mZVZAHashString
                End Get
                Set(ByVal value As String)
                    mZVZAHashString = value
                End Set
            End Property

            Public Property ZVZAHashversie() As String
                Get
                    Return mZVZAHashversie
                End Get
                Set(ByVal value As String)
                    mZVZAHashversie = value
                End Set
            End Property
        End Class

        Public Class GrouperMelding
            Dim mMeldingID As Integer
            Dim mMeldingSoort As String
            Dim mMeldingcode As String
            Dim mMeldingomschrijving As String

            Public Property MeldingID() As Integer
                Get
                    Return mMeldingID
                End Get
                Set(ByVal value As Integer)
                    mMeldingID = value
                End Set
            End Property

            Public Property MeldingSoort() As String
                Get
                    Return mMeldingSoort
                End Get
                Set(ByVal value As String)
                    mMeldingSoort = value
                End Set
            End Property

            Public Property Meldingcode() As String
                Get
                    Return mMeldingcode
                End Get
                Set(ByVal value As String)
                    mMeldingcode = value
                End Set
            End Property

            Public Property Meldingomschrijving() As String
                Get
                    Return mMeldingomschrijving
                End Get
                Set(ByVal value As String)
                    mMeldingomschrijving = value
                End Set
            End Property
        End Class
    End Class

    Enum GrouperResultaatType
        NietUitgevraagd = 0
        Uitgevraagd = 1
        Exception = 99
    End Enum

    Dim mResultaat As GrouperResultaatType
    Dim mMessage As String
    Dim mWSaddress As EndpointAddress
    Dim mWS As wsIntegratiePlatform.MCVServiceClient

    Private Function getGrouperAdres() As String
        Dim aEnvironment As String = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString()
        Return System.Configuration.ConfigurationManager.AppSettings("GrouperAdres_" & aEnvironment).ToString()
    End Function

    Public ReadOnly Property Resultaat() As GrouperResultaatType
        Get
            Return mResultaat
        End Get
    End Property

    Public ReadOnly Property Message() As String
        Get
            Return mMessage
        End Get
    End Property

    Public Sub New(ByVal ClientID As Integer, ByVal ClinicID As Integer, ByVal User As String)
        MyBase.New()
        mResultaat = GrouperResultaatType.NietUitgevraagd
        mMessage = ""

        Try
            mWSaddress = New EndpointAddress(getGrouperAdres())
            Dim binding As New BasicHttpBinding()
            binding.Security.Mode = BasicHttpSecurityMode.None
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic
            'binding.UseDefaultWebProxy = True
            'binding.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.Certificate
            'mWS = New wsIntegratiePlatform.MCVServiceClient
            mWS = New wsIntegratiePlatform.MCVServiceClient(binding, mWSaddress)
            mWS.Open()

            mClientID = ClientID
            mClinicID = ClinicID
            mUser = User

        Catch ex As Exception
            Throw New System.Exception(ex.Message)
        End Try
    End Sub

    Public Sub Dispose()
        Try
            mWS.Close()
        Finally
            mWS = Nothing
        End Try
    End Sub

    Public Function Controleer(ByVal BehandelID As Long) As GrouperResult
        Dim aGrouperResult As New GrouperResult

        Try
            Dim aDBCresult As wsIntegratiePlatform.DbcResult_Declaratieresultset = mWS.VerkrijgDbcVoorSubtraject(BehandelID)
            Dim provider As Globalization.CultureInfo = Globalization.CultureInfo.InvariantCulture

            aGrouperResult.Declaratiedatasetnummer = BehandelID
            If aDBCresult.DeclaratieResultsetNummer IsNot Nothing Then aGrouperResult.Declaratieresultsetnummer = aDBCresult.DeclaratieResultsetNummer

            If aDBCresult.TabelsetVersie IsNot Nothing Then aGrouperResult.Tabelsetversie = If(Not aDBCresult.TabelsetVersie Is Nothing, Date.ParseExact(aDBCresult.TabelsetVersie, "yyyyMMdd", provider), Nothing)
            If aDBCresult.GrouperVersie IsNot Nothing Then aGrouperResult.Grouperversie = aDBCresult.GrouperVersie
            If aDBCresult.ZPZAHashTotalDis IsNot Nothing Then aGrouperResult.DISHashtotal = aDBCresult.ZPZAHashTotalDis
            If aDBCresult.ZPZAHashCalcDis IsNot Nothing Then aGrouperResult.DISHashCalc = aDBCresult.ZPZAHashCalcDis
            If aDBCresult.ZPZAHashStringDIS IsNot Nothing Then aGrouperResult.DISHashString = aDBCresult.ZPZAHashStringDIS
            If aDBCresult.ZPZAHashVersieDIS IsNot Nothing Then aGrouperResult.DISHashversie = aDBCresult.ZPZAHashVersieDIS

            Dim aZorgproduct As wsIntegratiePlatform.DbcResult_Zorgproductdeclaratie = aDBCresult.Zorgproduct
            If aZorgproduct IsNot Nothing Then
                'aGrouperResult.OranjeZAInProfiel = aZorgproduct.OranjeZaInProfiel
                'aGrouperResult.AanspraakZVWToegepast = aZorgproduct.AanspraakZvwToegepast
                'aZorgproduct.ActiviteitenOpNota
                'aGrouperResult.ZAMetMachtiging = aZorgproduct.ZaMetMachtiging
                'aGrouperResult.ZAVertaling = aZorgproduct.ZaVertaling
                aGrouperResult.Zorgproductcode = aZorgproduct.ZorgproductCode
                aGrouperResult.Declaratiecode = aZorgproduct.DeclaratieCode
                aGrouperResult.ZVZPHashtotal = aZorgproduct.ZVZPHashTotal
                aGrouperResult.ZVZPHashCalc = aZorgproduct.ZVZPHashCalc
                aGrouperResult.ZVZPHashString = aZorgproduct.ZVZPHashString
                aGrouperResult.ZVZPHashversie = aZorgproduct.ZVZPHashVersie
                If aZorgproduct.ActiviteitenOpNota IsNot Nothing Then
                    For Each aZorgactiviteitOpNota As wsIntegratiePlatform.DbcResult_ZorgactiviteitOpNota In aZorgproduct.ActiviteitenOpNota
                        aGrouperResult.AddZorgactiviteitOpNota(aZorgactiviteitOpNota.Zorgactiviteitnummer)
                    Next
                End If
            End If

            'Dim mCertificaatVersieHash As String

            If aDBCresult.Meldingen IsNot Nothing Then
                For Each aMelding As wsIntegratiePlatform.DbcResult_Melding In aDBCresult.Meldingen
                    aGrouperResult.AddMelding(aMelding.Code, aMelding.Id, aMelding.Omschrijving, aMelding.Soort)
                Next
            End If
            If aDBCresult.Zorgactiviteiten IsNot Nothing Then
                For Each aZorgactiviteiten As wsIntegratiePlatform.DbcResult_Zorgactiviteitdeclaratie In aDBCresult.Zorgactiviteiten
                    aGrouperResult.AddZorgactiviteitDeclaratie(aZorgactiviteiten.Zorgactiviteitnummer, aZorgactiviteiten.DeclaratieCode, aZorgactiviteiten.ZVZAHashCalc, aZorgactiviteiten.ZVZAHashString, aZorgactiviteiten.ZVZAHashTotal, aZorgactiviteiten.ZVZAHashVersie)
                Next
            End If
            mResultaat = GrouperResultaatType.Uitgevraagd
            mMessage = ""
        Catch ex As Exception
            Me.mResultaat = GrouperResultaatType.Exception
            Me.mMessage = ex.Message

            aGrouperResult = Nothing
        End Try

        Return aGrouperResult
    End Function

    Public Shadows Function Insert(ByVal aGrouperResult As GrouperResult) As Boolean
        Dim rows As Integer
        Dim sqlupdate As System.Data.SqlClient.SqlCommand

        Try

            If dbConnection.State = ConnectionState.Closed Then dbConnection.Open()
            dbTransaction = dbConnection.BeginTransaction("Grouper")
            sqlupdate = New System.Data.SqlClient.SqlCommand("sp_add_GrouperDeclaratieresultset", dbConnection, dbTransaction)
            sqlupdate.CommandType = CommandType.StoredProcedure
            sqlupdate.Parameters.Add("@ClientID", SqlDbType.Int).Value = mClientID
            sqlupdate.Parameters.Add("@ClinicID", SqlDbType.Int).Value = mClinicID
            sqlupdate.Parameters.Add("@Declaratiedatasetnummer", SqlDbType.BigInt).Value = aGrouperResult.Declaratiedatasetnummer
            sqlupdate.Parameters.Add("@Declaratieresultsetnummer", SqlDbType.NVarChar, 40).Value = aGrouperResult.Declaratieresultsetnummer
            sqlupdate.Parameters.Add("@DISHashTotal", SqlDbType.NVarChar, 200).Value = aGrouperResult.DISHashtotal
            sqlupdate.Parameters.Add("@DISHashCalc", SqlDbType.NVarChar, 20).Value = aGrouperResult.DISHashCalc
            sqlupdate.Parameters.Add("@DISHashString", SqlDbType.NVarChar, -1).Value = aGrouperResult.DISHashString
            sqlupdate.Parameters.Add("@DISHashversie", SqlDbType.NVarChar, 50).Value = aGrouperResult.DISHashversie
            sqlupdate.Parameters.Add("@Tabelsetversie", SqlDbType.NVarChar, 40).Value = aGrouperResult.Tabelsetversie
            sqlupdate.Parameters.Add("@Grouperversie", SqlDbType.NVarChar, 12).Value = aGrouperResult.Grouperversie
            sqlupdate.Parameters.Add("@Zorgproductcode", SqlDbType.NVarChar, 15).Value = aGrouperResult.Zorgproductcode
            sqlupdate.Parameters.Add("@Declaratiecode", SqlDbType.NVarChar, 15).Value = If(aGrouperResult.Declaratiecode Is Nothing, "", aGrouperResult.Declaratiecode)
            sqlupdate.Parameters.Add("@AanspraakZVWToegepast", SqlDbType.Int).Value = aGrouperResult.AanspraakZVWToegepast
            sqlupdate.Parameters.Add("@ZAMetMachtiging", SqlDbType.Int).Value = aGrouperResult.ZAMetMachtiging
            sqlupdate.Parameters.Add("@OranjeZAInProfiel", SqlDbType.Int).Value = aGrouperResult.OranjeZAInProfiel
            sqlupdate.Parameters.Add("@ZAVertaling", SqlDbType.Int).Value = aGrouperResult.ZAVertaling
            sqlupdate.Parameters.Add("@ZVZPHashTotal", SqlDbType.NVarChar, 200).Value = aGrouperResult.ZVZPHashtotal
            sqlupdate.Parameters.Add("@ZVZPHashCalc", SqlDbType.NVarChar, 20).Value = aGrouperResult.ZVZPHashCalc
            sqlupdate.Parameters.Add("@ZVZPHashString", SqlDbType.NVarChar, -1).Value = aGrouperResult.ZVZPHashString
            sqlupdate.Parameters.Add("@ZVZPHashversie", SqlDbType.NVarChar, 50).Value = aGrouperResult.ZVZPHashversie
            sqlupdate.Parameters.Add("@CreateUser", SqlDbType.NVarChar, 20).Value = mUser

            rows = sqlupdate.ExecuteNonQuery()
            Dim result As Boolean = False
            If rows > 0 Then
                If DeleteGrouperChildObjects(aGrouperResult) Then
                    If AddGrouperChildObjects(aGrouperResult) Then
                        dbTransaction.Commit()
                        result = True
                    Else
                        dbTransaction.Rollback()
                    End If
                Else
                    dbTransaction.Rollback()
                End If
            Else
                dbTransaction.Rollback()
            End If

            If dbConnection.State = ConnectionState.Open Then dbConnection.Close()
            Return result
        Catch e As Exception
            '            ReportError(e, "Failed to insert PatientCareplan: " + Me.CarePlanName)
            If dbConnection.State = ConnectionState.Open Then
                dbTransaction.Rollback()
                If dbConnection.State = ConnectionState.Open Then dbConnection.Close()
            End If
            Return False
        End Try
    End Function

    Public Shadows Function Update(ByVal aGrouperResult As GrouperResult) As Boolean
        Dim rows As Integer

        Try
            Dim sqlupdate As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand("sp_update_GrouperDeclaratieresultset", dbConnection, dbTransaction)
            sqlupdate.CommandType = CommandType.StoredProcedure

            sqlupdate.Parameters.Add("@ClientID", SqlDbType.Int).Value = mClientID
            sqlupdate.Parameters.Add("@ClinicID", SqlDbType.Int).Value = mClinicID
            sqlupdate.Parameters.Add("@Declaratiedatasetnummer", SqlDbType.BigInt).Value = aGrouperResult.Declaratiedatasetnummer
            sqlupdate.Parameters.Add("@Declaratieresultsetnummer", SqlDbType.NVarChar, 40).Value = aGrouperResult.Declaratieresultsetnummer
            sqlupdate.Parameters.Add("@DISHashTotal", SqlDbType.NVarChar, 200).Value = aGrouperResult.DISHashtotal
            sqlupdate.Parameters.Add("@DISHashCalc", SqlDbType.NVarChar, 20).Value = aGrouperResult.DISHashCalc
            sqlupdate.Parameters.Add("@DISHashString", SqlDbType.NVarChar, -1).Value = aGrouperResult.DISHashString
            sqlupdate.Parameters.Add("@DISHashversie", SqlDbType.NVarChar, 50).Value = aGrouperResult.DISHashversie
            sqlupdate.Parameters.Add("@Tabelsetversie", SqlDbType.NVarChar, 40).Value = aGrouperResult.Tabelsetversie
            sqlupdate.Parameters.Add("@Grouperversie", SqlDbType.NVarChar, 12).Value = aGrouperResult.Grouperversie
            sqlupdate.Parameters.Add("@Zorgproductcode", SqlDbType.NVarChar, 15).Value = aGrouperResult.Zorgproductcode
            sqlupdate.Parameters.Add("@Declaratiecode", SqlDbType.NVarChar, 15).Value = aGrouperResult.Declaratiecode
            sqlupdate.Parameters.Add("@AanspraakZVWToegepast", SqlDbType.Int).Value = aGrouperResult.AanspraakZVWToegepast
            sqlupdate.Parameters.Add("@ZAMetMachtiging", SqlDbType.Int).Value = aGrouperResult.ZAMetMachtiging
            sqlupdate.Parameters.Add("@OranjeZAInProfiel", SqlDbType.Int).Value = aGrouperResult.OranjeZAInProfiel
            sqlupdate.Parameters.Add("@ZAVertaling", SqlDbType.Int).Value = aGrouperResult.ZAVertaling
            sqlupdate.Parameters.Add("@ZVZPHashTotal", SqlDbType.NVarChar, 200).Value = aGrouperResult.ZVZPHashtotal
            sqlupdate.Parameters.Add("@ZVZPHashCalc", SqlDbType.NVarChar, 20).Value = aGrouperResult.ZVZPHashCalc
            sqlupdate.Parameters.Add("@ZVZPHashString", SqlDbType.NVarChar, -1).Value = aGrouperResult.ZVZPHashString
            sqlupdate.Parameters.Add("@ZVZPHashversie", SqlDbType.NVarChar, 50).Value = aGrouperResult.ZVZPHashversie
            sqlupdate.Parameters.Add("@UpdateUser", SqlDbType.NVarChar, 20).Value = mUser

            dbConnection.Open()
            dbTransaction = dbConnection.BeginTransaction("Grouper")
            rows = sqlupdate.ExecuteNonQuery()
            Dim result As Boolean = False
            If rows > 0 Then
                If DeleteGrouperChildObjects(aGrouperResult) Then
                    If AddGrouperChildObjects(aGrouperResult) Then
                        dbTransaction.Commit()
                        result = True
                    Else
                        dbTransaction.Rollback()
                    End If
                Else
                    dbTransaction.Rollback()
                End If
            Else
                dbTransaction.Rollback()
            End If

            dbConnection.Close()
            Return result
        Catch e As Exception
            'ReportError(e, "Failed to update PatientCareplan: " + Me.CarePlanID.ToString)
            If dbConnection.State = ConnectionState.Open Then
                dbTransaction.Rollback()
                dbConnection.Close()
            End If
            Return False
        End Try
    End Function

    Public Shadows Function Delete(ByVal aGrouperResult As GrouperResult) As Boolean
        Dim rows As Integer
        Dim sqlupdate As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand("sp_delete_GrouperDeclaratieresultset", dbConnection, dbTransaction)

        Try
            sqlupdate.CommandType = CommandType.StoredProcedure

            sqlupdate.Parameters.Add("@ClientID", SqlDbType.Int).Value = mClientID
            sqlupdate.Parameters.Add("@ClinicID", SqlDbType.Int).Value = mClinicID
            sqlupdate.Parameters.Add("@Declaratiedatasetnummer", SqlDbType.BigInt).Value = aGrouperResult.Declaratiedatasetnummer
            sqlupdate.Parameters.Add("@DeletedUser", SqlDbType.NVarChar, 20).Value = mUser

            dbConnection.Open()
            dbTransaction = dbConnection.BeginTransaction("Grouper")
            rows = sqlupdate.ExecuteNonQuery()
            Dim result As Boolean = False
            If rows > 0 Then
                If DeleteGrouperChildObjects(aGrouperResult) Then
                    dbTransaction.Commit()
                    result = True
                Else
                    dbTransaction.Rollback()
                End If
            Else
                dbTransaction.Rollback()
            End If

            dbConnection.Close()
            Return result
        Catch e As Exception
            'ReportError(e, "Failed to update PatientCareplan: " + Me.CarePlanID.ToString)
            If dbConnection.State = ConnectionState.Open Then
                dbTransaction.Rollback()
                dbConnection.Close()
            End If
            Return False
        End Try
    End Function

    Private Function DeleteGrouperChildObjects(ByVal aGrouperResult As GrouperResult) As Boolean
        Dim sqlupdate As System.Data.SqlClient.SqlCommand
        Dim rows As Integer

        Try
            'empty GrouperZorgactiviteitdeclaratie
            sqlupdate = New System.Data.SqlClient.SqlCommand("sp_delete_GrouperZorgactiviteitdeclaratie", dbConnection, dbTransaction)
            sqlupdate.CommandType = CommandType.StoredProcedure
            sqlupdate.Parameters.Add("@ClientID", SqlDbType.Int).Value = mClientID
            sqlupdate.Parameters.Add("@ClinicID", SqlDbType.Int).Value = mClinicID
            sqlupdate.Parameters.Add("@Declaratiedatasetnummer", SqlDbType.BigInt).Value = aGrouperResult.Declaratiedatasetnummer
            rows = sqlupdate.ExecuteScalar()
            If rows = -1 Then Return False

            'empty GrouperMelding
            sqlupdate = New System.Data.SqlClient.SqlCommand("sp_delete_GrouperMelding", dbConnection, dbTransaction)
            sqlupdate.CommandType = CommandType.StoredProcedure
            sqlupdate.Parameters.Add("@ClientID", SqlDbType.Int).Value = mClientID
            sqlupdate.Parameters.Add("@ClinicID", SqlDbType.Int).Value = mClinicID
            sqlupdate.Parameters.Add("@Declaratiedatasetnummer", SqlDbType.BigInt).Value = aGrouperResult.Declaratiedatasetnummer
            rows = sqlupdate.ExecuteScalar()
            If rows = -1 Then Return False

            'empty GrouperZorgactiviteitOpNota
            sqlupdate = New System.Data.SqlClient.SqlCommand("sp_delete_GrouperZorgactiviteitOpNota", dbConnection, dbTransaction)
            sqlupdate.CommandType = CommandType.StoredProcedure
            sqlupdate.Parameters.Add("@ClientID", SqlDbType.Int).Value = mClientID
            sqlupdate.Parameters.Add("@ClinicID", SqlDbType.Int).Value = mClinicID
            sqlupdate.Parameters.Add("@Declaratiedatasetnummer", SqlDbType.BigInt).Value = aGrouperResult.Declaratiedatasetnummer
            rows = sqlupdate.ExecuteScalar()
            If rows = -1 Then Return False
        Catch e As Exception
            'ReportError(e, "Failed to update PatientCareplan: " + Me.CarePlanID.ToString)
            Return False
        End Try
        Return True
    End Function

    Private Function AddGrouperChildObjects(ByVal aGrouperResult As GrouperResult) As Boolean
        Dim sqlupdate As System.Data.SqlClient.SqlCommand
        Dim rows As Integer

        Try
            'Insert GrouperZorgactiviteitdeclaratie()
            For Each item As GrouperResult.GrouperZorgactiviteitDeclaratie In aGrouperResult.ZorgactiviteitDeclaratie
                sqlupdate = New System.Data.SqlClient.SqlCommand("sp_add_GrouperZorgactiviteitdeclaratie", dbConnection, dbTransaction)
                sqlupdate.CommandType = CommandType.StoredProcedure
                sqlupdate.Parameters.Add("@ClientID", SqlDbType.Int).Value = mClientID
                sqlupdate.Parameters.Add("@ClinicID", SqlDbType.Int).Value = mClinicID
                sqlupdate.Parameters.Add("@Declaratiedatasetnummer", SqlDbType.BigInt).Value = aGrouperResult.Declaratiedatasetnummer
                sqlupdate.Parameters.Add("@Zorgactiviteitnummer", SqlDbType.NVarChar, 15).Value = item.ZorgactiviteitNummer
                sqlupdate.Parameters.Add("@Declaratiecode", SqlDbType.NVarChar, 15).Value = item.Declaratiecode
                sqlupdate.Parameters.Add("@ZVZAHashTotal", SqlDbType.NVarChar, 200).Value = item.ZVZAHashtotal
                sqlupdate.Parameters.Add("@ZVZAHashCalc", SqlDbType.NVarChar, 20).Value = item.ZVZAHashCalc
                sqlupdate.Parameters.Add("@ZVZAHashString", SqlDbType.NVarChar, -1).Value = item.ZVZAHashString
                sqlupdate.Parameters.Add("@ZVZAHashversie", SqlDbType.NVarChar, 50).Value = item.ZVZAHashversie
                rows = sqlupdate.ExecuteNonQuery()
                If rows = 0 Then Return False
            Next

            'Insert GrouperMelding
            For Each item As GrouperResult.GrouperMelding In aGrouperResult.Melding
                sqlupdate = New System.Data.SqlClient.SqlCommand("sp_add_GrouperMelding", dbConnection, dbTransaction)
                sqlupdate.CommandType = CommandType.StoredProcedure
                sqlupdate.Parameters.Add("@ClientID", SqlDbType.Int).Value = mClientID
                sqlupdate.Parameters.Add("@ClinicID", SqlDbType.Int).Value = mClinicID
                sqlupdate.Parameters.Add("@Declaratiedatasetnummer", SqlDbType.BigInt).Value = aGrouperResult.Declaratiedatasetnummer
                sqlupdate.Parameters.Add("@MeldingID", SqlDbType.Int).Value = item.MeldingID
                sqlupdate.Parameters.Add("@Meldingsoort", SqlDbType.NVarChar, 1).Value = item.MeldingSoort
                sqlupdate.Parameters.Add("@Meldingcode", SqlDbType.NVarChar, 12).Value = item.Meldingcode
                sqlupdate.Parameters.Add("@Meldingomschrijving", SqlDbType.NVarChar, 200).Value = item.Meldingomschrijving
                rows = sqlupdate.ExecuteNonQuery()
                If rows = 0 Then Return False
            Next

            'Insert GrouperZorgactiviteitOpNota
            For Each item As GrouperResult.GrouperZorgactiviteitOpNota In aGrouperResult.ZorgactiviteitOpNota
                sqlupdate = New System.Data.SqlClient.SqlCommand("sp_add_GrouperZorgactiviteitOpNota", dbConnection, dbTransaction)
                sqlupdate.CommandType = CommandType.StoredProcedure
                sqlupdate.Parameters.Add("@ClientID", SqlDbType.Int).Value = mClientID
                sqlupdate.Parameters.Add("@ClinicID", SqlDbType.Int).Value = mClinicID
                sqlupdate.Parameters.Add("@Declaratiedatasetnummer", SqlDbType.BigInt).Value = aGrouperResult.Declaratiedatasetnummer
                sqlupdate.Parameters.Add("@Zorgactiviteitnummer", SqlDbType.NVarChar, 15).Value = item.ZorgactiviteitNummer
                rows = sqlupdate.ExecuteNonQuery()
                If rows = 0 Then Return False
            Next
        Catch e As Exception
            'ReportError(e, "Failed to update PatientCareplan: " + Me.CarePlanID.ToString)
            Return False
        End Try
        Return True
    End Function

    Public Function EvaluateForSQL(ByVal value As Image)
        If value Is Nothing Then
            Return DBNull.Value
        Else
            Return value
        End If
    End Function

    Public Function EvaluateForSQL(ByVal value As String)
        If value = "" Or value Is Nothing Then
            Return DBNull.Value
        Else
            Return value
        End If
    End Function

    Public Function EvaluateForSQL(ByVal value As DateTime)
        If value.ToString = "" Or value.Year = 1 Then
            Return DBNull.Value
        Else
            Return value
        End If
    End Function

    'Public Function EvaluateFromSQL(ByVal value As Image)
    '    If value Is Nothing Or value Is DBNull Then
    '        Return Nothing
    '    Else
    '        Return value
    '    End If
    'End Function

    Public Function EvaluateFromSQL(ByVal value As DBNull)
        Return Nothing
    End Function

    Public Function EvaluateFromSQL(ByVal value As String)
        If value = "" Or value Is Nothing Then
            Return Nothing
        Else
            Return value
        End If
    End Function

    Public Function EvaluateFromSQL(ByVal value As Date)
        If value.ToString = "" Or value.Year = 1 Then
            Return Nothing
        Else
            Return value
        End If
    End Function

    Public Function EvaluateFromSQL(ByVal value As TimeSpan)
        If value.ToString = "" Or value.Days = 0 Then
            Return Nothing
        Else
            Return value
        End If
    End Function
End Class
