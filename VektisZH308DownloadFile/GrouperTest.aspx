﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GrouperTest.aspx.vb" Inherits="VektisZH308DownloadFile.GrouperTest" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="style1">
             <tr>
                <td>
                    <a href="Main.aspx">Hoofdmenu</a></td>
                <td>
                    &nbsp;</td>
            </tr>
               <tr>
               <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
               <td>
                    Behandel ID (komma seperated):</td>
                <td>
                    <asp:TextBox ID="txtBehandelID" runat="server" Width="592px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Resultaat</td>
                <td>
                    <asp:TextBox ID="txtResultaat" runat="server" Height="57px" 
                        TextMode="MultiLine" Width="604px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    </td>
                <td>
                    <asp:TextBox ID="txtBericht" runat="server" Height="170px" 
                        TextMode="MultiLine" Width="604px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    </td>
                <td>
                    <asp:Button ID="btnControleer" runat="server" Text="Controleer" />
                &nbsp;<asp:CheckBox ID="chkWriteDB" runat="server" Text="Write DB" />
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
