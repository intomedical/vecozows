﻿Imports System.Data.SqlClient

Public Class SuperClass
    Private bError As Boolean
    Private sMessage As String
    Private bCollected As Boolean = False
    Private bChanged As Boolean = False
    Private bDeleted As Boolean = False

    'Dim cIMDLL As IntoMedicalDLL
    Private dtDeletedOn As DateTime
    Private sDeletedBy As String
    Private sDeletedByName As String
    Private dtCreatedOn As DateTime
    Private sCreatedBy As String
    Private sCreatedByName As String
    Private dtChangedOn As DateTime
    Private sChangedBy As String
    Private sChangedByName As String
    Private iClientID As Integer
    Private sCreatedByAbbreviation As String
    'Private cCurrentUser As User
    Public sTransactionID As String
    Public dbConnection As SqlConnection
    Private dvData As DataSet
    Protected dbIntoCareScedulerConnection As SqlConnection
    Private bAllowChangeData As Boolean = False

    'Public Property IMDLL As IntoMedicalDLL
    '    Get
    '        If cIMDLL Is Nothing Then cIMDLL = New IntoMedicalDLL

    '        Return cIMDLL
    '    End Get
    '    Set(value As IntoMedicalDLL)
    '        cIMDLL = value
    '    End Set
    'End Property

    'Public Property ClientID As Integer
    '    Get
    '        If iClientID = 0 Then
    '            Return CurrentUser.ClientID
    '        Else
    '            Return iClientID
    '        End If
    '    End Get
    '    Set(value As Integer)
    '        iClientID = value
    '    End Set
    'End Property

    Public ReadOnly Property Log As Boolean
        Get
            Try
                Return System.Web.Configuration.WebConfigurationManager.AppSettings("LOGGING").ToString() = "TRUE"
            Catch ex As Exception
                Return False
            End Try
        End Get
    End Property

    Public Property CreatedOn As DateTime
        Get
            Return dtCreatedOn
        End Get
        Set(value As DateTime)
            dtCreatedOn = value
        End Set
    End Property

    'Public Property CreatedBy As String
    '    Get
    '        If Not String.IsNullOrEmpty(sCreatedByName) Then Return sCreatedByName
    '        If String.IsNullOrEmpty(sCreatedBy) Then Return "[Onbekend]"

    '        Dim cuser As New User(sCreatedBy)
    '        sCreatedByName = cuser.VolledigeNaam
    '        If (Not cuser.Resource Is Nothing) Then CreatedByAbbreviation = cuser.Resource.Abbreviation
    '        Return sCreatedByName
    '    End Get
    '    Set(value As String)
    '        sCreatedBy = value
    '    End Set
    'End Property

    'Public WriteOnly Property CreatedByName As String
    '    Set(value As String)
    '        sCreatedByName = value
    '    End Set
    'End Property

    'Public Property CreatedByAbbreviation As String
    '    Set(value As String)
    '        sCreatedByAbbreviation = value
    '    End Set
    '    Get
    '        If Not String.IsNullOrEmpty(sCreatedByAbbreviation) Then Return sCreatedByAbbreviation

    '        Dim cuser As New User(sCreatedBy)
    '        sCreatedByName = cuser.VolledigeNaam
    '        If (Not cuser.Resource Is Nothing) Then sCreatedByAbbreviation = cuser.Resource.Abbreviation
    '        Return If(Not String.IsNullOrEmpty(sCreatedByAbbreviation), sCreatedByAbbreviation, Me.CreatedBy)
    '    End Get
    'End Property

    Public WriteOnly Property ChangedByName As String
        Set(value As String)
            sChangedByName = value
        End Set
    End Property

    Public WriteOnly Property DeletedByName As String
        Set(value As String)
            sDeletedByName = value
        End Set
    End Property

    Public Property ChangedOn As DateTime
        Get
            Return dtChangedOn
        End Get
        Set(value As DateTime)
            dtChangedOn = value
        End Set
    End Property

    'Public Property ChangedBy As String
    '    Get
    '        If Not String.IsNullOrEmpty(sChangedByName) Then Return sChangedByName
    '        If String.IsNullOrEmpty(sChangedBy) Then Return "[Onbekend]"

    '        Dim cuser As New User(sChangedBy)
    '        sChangedByName = cuser.VolledigeNaam
    '        Return sChangedByName
    '    End Get
    '    Set(value As String)
    '        sChangedBy = value
    '    End Set
    'End Property

    Public Property DeletedOn As DateTime
        Get
            Return dtDeletedOn
        End Get
        Set(value As DateTime)
            dtDeletedOn = value
        End Set
    End Property

    'Public Property DeletedBy As String
    '    Get
    '        If Not Deleted Then Return ""
    '        If Not String.IsNullOrEmpty(sDeletedByName) Then Return sDeletedByName
    '        If String.IsNullOrEmpty(sDeletedBy) Then Return "[Onbekend]"

    '        Dim cuser As New User(sDeletedBy)
    '        sDeletedByName = cuser.VolledigeNaam
    '        Return sDeletedByName
    '    End Get
    '    Set(value As String)
    '        sDeletedBy = value
    '    End Set
    'End Property

    'Public Property AllowChange As Boolean
    '    Get
    '        Return If(String.IsNullOrEmpty(sTransactionID), True, CurrentUser.isAllowed(sTransactionID, Transaction.Setting.Action.Change)) And bAllowChangeData
    '    End Get
    '    Set(value As Boolean)
    '        bAllowChangeData = value
    '    End Set
    'End Property

    'Public ReadOnly Property AllowAdd As Boolean
    '    Get
    '        Return CurrentUser.isAllowed(sTransactionID, Transaction.Setting.Action.Add)
    '    End Get
    'End Property

    'Public ReadOnly Property AllowDelete As Boolean
    '    Get
    '        Return If(String.IsNullOrEmpty(sTransactionID), True, CurrentUser.isAllowed(sTransactionID, Transaction.Setting.Action.Delete)) And bAllowChangeData
    '    End Get
    'End Property

    Protected Friend Property dsSqlData() As DataSet
        Get
            If dvData Is Nothing Then dvData = New DataSet
            Return dvData
        End Get
        Set(ByVal value As DataSet)
            dvData = value
        End Set
    End Property
    Public Property Changed() As Boolean
        Get
            Return bChanged
        End Get
        Set(ByVal value As Boolean)
            bChanged = value
        End Set
    End Property
    Public Property Deleted() As Boolean
        Get
            Return bDeleted
        End Get
        Set(ByVal value As Boolean)
            If value <> Deleted Then Changed = True
            bDeleted = value
        End Set
    End Property
    Public Property Collected() As Boolean
        Get
            Return bCollected
        End Get
        Set(ByVal value As Boolean)
            bCollected = value
        End Set
    End Property

    Public Property hasError() As Boolean
        Get
            Return bError
        End Get
        Set(ByVal value As Boolean)
            bError = value
        End Set
    End Property
    Public Property Message() As String
        Get
            Return sMessage
        End Get
        Set(ByVal value As String)
            sMessage = value
        End Set
    End Property

    Public Sub SetResult(ByVal isError As Boolean, ByVal Message As String)
        bError = isError
        sMessage = Message
    End Sub

    'Public Function EvaluateForSQL(ByVal value As Image) As Object
    '    If value Is Nothing Then
    '        Return DBNull.Value
    '    Else
    '        Return value
    '    End If
    'End Function

    'Public Function EvaluateForSQL(ByVal value As String) As Object
    '    If value = "" Or value Is Nothing Then
    '        Return DBNull.Value
    '    Else
    '        Return value
    '    End If
    'End Function

    'Public Function EvaluateForSQL(ByVal value As DateTime) As Object
    '    If value.ToString = "" Or value.Year = 1 Then
    '        Return DBNull.Value
    '    Else
    '        Return value
    '    End If
    'End Function

    Public Function EvaluateFromSQL(ByVal value As Long, Optional valueifnull As Long = Nothing) As Long
        If IsDBNull(value) Then
            Return valueifnull
        Else
            Return value
        End If
    End Function

    'Public Function EvaluateFromSQL(ByVal value As Image) As Image
    '    If value Is Nothing Or IsDBNull(value) Then
    '        Return Nothing
    '    Else
    '        Return value
    '    End If
    'End Function

    'Public Function EvaluateFromSQL(ByVal value As DBNull) As DBNull
    '    Return Nothing
    'End Function

    Public Function EvaluateFromSQL(ByVal value As String, Optional valueifnull As String = Nothing) As String
        If IsDBNull(value) Then
            Return valueifnull
        Else
            Return value
        End If
    End Function

    Public Function EvaluateFromSQL(ByVal value As Integer, Optional valueifnull As Integer = Nothing) As Integer
        If IsDBNull(value) Then
            Return valueifnull
        Else
            Return value
        End If
    End Function

    Public Function EvaluateFromSQL(ByVal value As Date) As Date
        If value.ToString = "" Or value.Year = 1 Then
            Return Nothing
        Else
            Return value
        End If
    End Function

    Public Function EvaluateFromSQL(ByVal value As TimeSpan) As TimeSpan
        If value.ToString = "" Or value.Days = 0 Then
            Return Nothing
        Else
            Return value
        End If
    End Function

    'Public ReadOnly Property Functions() As Functions
    '    Get
    '        Return New Functions
    '    End Get
    'End Property

    'Private ReadOnly Property Environment As String
    '    Get
    '        If dbConnection Is Nothing Then
    '            Dim connstring() As String = ConfigurationManager.ConnectionStrings("IntoCareConnection").ToString.Split(";")
    '            Return connstring(1).Replace("Data Source=", "Server: ") + vbNewLine + connstring(2).Replace("Initial Catalog=", "Database: ")
    '        Else
    '            Return "Server: " + dbConnection.DataSource + vbNewLine + "Database: " + dbConnection.Database
    '        End If
    '    End Get
    'End Property

    'Protected Friend Function ReportError(ByVal ex As Exception, ByVal sAction As String) As Boolean
    '    Throw New HttpUnhandledException("Environment: " + Environment + vbNewLine +
    '                                     "ClientID: " + ClientID.ToString + vbNewLine +
    '                                     "User: " + If(cCurrentUser Is Nothing, "Unknown", cCurrentUser.UserID) + vbNewLine +
    '                                     "Errormessage: " + vbNewLine + ex.Message.ToString + vbNewLine +
    '                                    "Inner: " + If(Not ex.InnerException Is Nothing, ex.InnerException.ToString, "Unknown") + vbNewLine +
    '                                    "Action: " + sAction + vbNewLine +
    '                                    "Stack trace: " + ex.StackTrace)
    'End Function

    Protected Friend Function GetSqlData(ByVal queryString As String, Optional ByVal parms As List(Of SqlParameter) = Nothing, Optional ByRef DS As DataSet = Nothing, Optional sdbConnection As SqlConnection = Nothing) As Boolean
        If sdbConnection Is Nothing Then sdbConnection = dbConnection
        Dim rows As Integer
        Dim dbCommand As SqlCommand = New System.Data.SqlClient.SqlCommand
        dbCommand.CommandText = queryString
        dbCommand.Connection = sdbConnection

        Try

            If dbCommand.CommandText.Contains(" ") Then
                dbCommand.CommandType = CommandType.Text
            Else
                dbCommand.CommandType = CommandType.StoredProcedure
            End If

            If dbCommand.Connection.State = ConnectionState.Closed Then dbCommand.Connection.Open()

            If Not parms Is Nothing Then
                For Each parm As SqlParameter In parms
                    dbCommand.Parameters.AddWithValue(parm.ParameterName, parm.Value)
                Next
            End If
            Dim dataAdapter As System.Data.SqlClient.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
            dataAdapter.SelectCommand = dbCommand

            If DS Is Nothing Then
                rows = dataAdapter.Fill(dsSqlData)
            Else
                rows = dataAdapter.Fill(DS)
            End If
            'bCollected = True

        Catch e As Exception
            '            ReportError(e, "Select failed.")
            Return False
        Finally
            If dbCommand.Connection.State = ConnectionState.Open Then dbCommand.Connection.close()
        End Try

        Return True

    End Function

    'Protected Friend Function GetSqlValue(ByVal queryString As String, Optional ByVal parms As SqlParameterCollection = Nothing, Optional ByRef DS As DataSet = Nothing) As String
    '    'The first column of the first row from the queryString, or Nothing i if the result set is empty. Returns a maximum of 2033 characters
    '    Dim dbCommand As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand
    '    dbCommand.CommandText = queryString
    '    dbCommand.Connection = dbConnection
    '    Try

    '        If Not parms Is Nothing Then
    '            For Each parm As Parameter In parms
    '                dbCommand.Parameters.Add(parm)
    '            Next
    '        End If

    '        If dbCommand.Connection.State = ConnectionState.Closed Then dbCommand.Connection.Open()

    '        Return EvaluateFromSQL(dbCommand.ExecuteScalar())

    '    Catch e As Exception
    '        '            ReportError(e, "Select failed.")
    '        Return False
    '    Finally
    '        If dbCommand.Connection.State = ConnectionState.Open Then dbCommand.Connection.Close()
    '    End Try

    '    Return True
    'End Function

    Public Function GetDataAdapter(ByVal sSql As String, Optional sdbConnection As SqlConnection = Nothing) As SqlDataAdapter
        If sdbConnection Is Nothing Then sdbConnection = dbConnection
        Dim cmd As New SqlCommand(sSql, sdbConnection) With {.CommandType = CommandType.Text}
        Dim dataAdapter As System.Data.SqlClient.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter() With {.SelectCommand = cmd}

        Return dataAdapter
    End Function

    'Public Property CurrentUser As User
    '    Get
    '        Try
    '            If Not cCurrentUser Is Nothing Then Return cCurrentUser

    '            If HttpContext.Current.Session Is Nothing Then
    '                Return New User(HttpContext.Current.User.Identity.Name)
    '            End If

    '            If System.Web.HttpContext.Current.Session("CurrentUser") Is Nothing Then
    '                Dim config = Web.Configuration.WebConfigurationManager.OpenWebConfiguration(System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath)
    '                Dim authentication As Web.Configuration.AuthenticationSection = config.GetSection("system.web/authentication")

    '                If authentication.Mode = Web.Configuration.AuthenticationMode.Windows Then
    '                    'try to get user based on windows account
    '                    Dim ex As Exception
    '                    Dim ccUser As New User

    '                    Dim status = ccUser.Logon(HttpContext.Current.User.Identity.Name, Nothing, ex)
    '                    If Not ex Is Nothing Then Throw New Exception(ex.Message)
    '                    If Not ccUser.acceptedLogonStatus(status) Then
    '                        Throw New Exception(ccUser.LogonStatusText(status))
    '                    End If
    '                    setCurrentUser(ccUser)
    '                ElseIf authentication.Mode = Web.Configuration.AuthenticationMode.Forms Then
    '                    Try
    '                        If Not String.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) And HttpContext.Current.User.Identity.IsAuthenticated Then
    '                            Dim ex As Exception
    '                            Dim ccUser As New User
    '                            Dim status = ccUser.Logon(HttpContext.Current.User.Identity.Name, Nothing, ex)
    '                            If Not ex Is Nothing Then Throw New Exception(ex.Message)
    '                            If Not ccUser.acceptedLogonStatus(status) Then
    '                                Throw New Exception(ccUser.LogonStatusText(status))
    '                            End If
    '                            setCurrentUser(ccUser)
    '                        Else
    '                            FormsAuthentication.RedirectToLoginPage()
    '                            System.Web.HttpContext.Current.Response.Redirect(FormsAuthentication.LoginUrl)
    '                            Return New User
    '                        End If
    '                    Catch ex As Exception
    '                        FormsAuthentication.RedirectToLoginPage()
    '                        System.Web.HttpContext.Current.Response.Redirect(FormsAuthentication.LoginUrl)
    '                        Return New User
    '                    End Try
    '                End If
    '            End If

    '            Return System.Web.HttpContext.Current.Session("CurrentUser")
    '        Catch ex As System.Threading.ThreadAbortException
    '            'ignore error.
    '        Catch ex As Exception
    '            System.Web.HttpContext.Current.Response.Redirect("~/ErrorPages/Http403NoAccess.htm?message=" + Replace(ex.Message, vbNewLine, " ") + ", gebruiker " + HttpContext.Current.User.Identity.Name.ToString())
    '            Return Nothing
    '        End Try

    '    End Get
    '    Set(value As User)
    '        cCurrentUser = value
    '        If Not HttpContext.Current.Session Is Nothing Then System.Web.HttpContext.Current.Session("CurrentUser") = value
    '        HttpContext.Current.Session("CLIENTID") = value.ClientID
    '    End Set
    'End Property

    'Public Property ApplicationSettings As ApplicationSettings
    '    Get
    '        If HttpContext.Current.Session Is Nothing Then
    '            'no sessionstate
    '            Return New ApplicationSettings(True, Me.ClientID)
    '        Else
    '            Dim Applsettings = CType(System.Web.HttpContext.Current.Session("Applicationsettings"), ApplicationSettings)

    '            If Applsettings Is Nothing Then
    '                Applsettings = New ApplicationSettings(True, Me.ClientID)
    '                System.Web.HttpContext.Current.Session("Applicationsettings") = Applsettings
    '            Else
    '                If Applsettings.ClientID = Me.ClientID Then Return Applsettings

    '                Applsettings = New ApplicationSettings(True, Me.ClientID)
    '                System.Web.HttpContext.Current.Session("Applicationsettings") = Applsettings
    '            End If

    '            Return System.Web.HttpContext.Current.Session("Applicationsettings")
    '        End If
    '    End Get
    '    Set(ByVal value As ApplicationSettings)

    '        If Not HttpContext.Current.Session Is Nothing Then
    '            System.Web.HttpContext.Current.Session("Applicationsettings") = value
    '        End If
    '    End Set
    'End Property

    'Private Function setCurrentUser(ByVal cUser As User) As Boolean
    '    If Not cUser.UserDataCollected Then If Not cUser.GetUserDetails(cUser.UserID, cUser.ClinicOrgCode) Then Return False
    '    If Not cUser.UserSettingsCollected Then If Not cUser.GetSettings() Then Return False
    '    If Not cUser.UserTransactionsCollected Then If Not cUser.GetUserTransactions() Then Return False
    '    If Not cUser.PopupWidowSettingsCollected Then If Not cUser.GetPopupWindowSettings() Then Return False
    '    'If Not cUser.WidgetsCollected Then If Not cUser.GetWidgets() Then Return False
    '    'If Not cUser.getRecentPatients() Then Return False
    '    System.Web.HttpContext.Current.Session.Add("CurrentUser", cUser)

    '    Return True
    'End Function

    Public Sub WriteToLog(ByVal text As String)
        Dim logFile As String = "~/App_Data/AppLog.txt"
        If Not HttpContext.Current Is Nothing Then
            logFile = HttpContext.Current.Server.MapPath(logFile)
        End If

        Try
            ' Open the log file for append and write the log
            Dim sw As IO.StreamWriter = New IO.StreamWriter(logFile, True)
            sw.WriteLine("**** " & DateTime.Now & " - " + text)
            sw.Close()
        Catch ex As Exception
            Return
        End Try

    End Sub

    'Public Function WriteToDBLog(ByVal Process As String, ByVal Text As String, Optional ccCurrentUser As User = Nothing) As Boolean
    '    If Not Log Then Return True
    '    Try
    '        Dim sqlupdate As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand("sp_add_Log", dbConnection)
    '        sqlupdate.CommandType = CommandType.StoredProcedure
    '        Dim sNothing As String = Nothing

    '        If ccCurrentUser Is Nothing And Not cCurrentUser Is Nothing Then
    '            ccCurrentUser = cCurrentUser
    '        End If

    '        sqlupdate.Parameters.Add("@ClientID", SqlDbType.Int).Value = EvaluateForSQL(If(ccCurrentUser Is Nothing, Nothing, ccCurrentUser.ClientID))
    '        sqlupdate.Parameters.Add("@Process", SqlDbType.NVarChar, 50).Value = EvaluateForSQL(Process)
    '        sqlupdate.Parameters.Add("@Result", SqlDbType.NVarChar).Value = EvaluateForSQL(Text)
    '        sqlupdate.Parameters.Add("@UpdateUserID", SqlDbType.NVarChar, 8).Value = EvaluateForSQL(If(ccCurrentUser Is Nothing, Nothing, ccCurrentUser.UserID))

    '        If dbConnection.State = ConnectionState.Closed Then dbConnection.Open()
    '        sqlupdate.ExecuteNonQuery()

    '        dbConnection.Close()
    '    Catch e As Exception
    '        If dbConnection.State = ConnectionState.Open Then dbConnection.Close()
    '        ReportError(e, "Failed to add to DB Log (Process: " + If(String.IsNullOrEmpty(Process), "Unknown", Process) + " Text: " + If(String.IsNullOrEmpty(Text), "Unknown", Text) + ")")
    '        Return False
    '    End Try

    'End Function

    Public Sub New()
        'dbIntoCareScedulerConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("IntoCareScedulerConnection").ToString())
        If Not ConfigurationManager.ConnectionStrings("IntoCareConnection") Is Nothing Then
            dbConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("IntoCareConnection").ToString())
        End If
    End Sub

End Class
