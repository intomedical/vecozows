﻿Option Strict Off

Imports System.Data.SqlClient
Imports System.IO

Public Class VektisZH308ZH309
    Inherits SuperClass
    Private intIdentificatieDetailrecord As Integer = 0

    Function GetVektisZH310File(ByVal OHWBatchID As Long) As MemoryStream
        Dim dbConnection As SqlConnection
        Dim dbCommandView As New SqlCommand
        Dim dbCommandStandard As New SqlCommand
        Dim dbReaderStandard As SqlDataReader
        Dim dbReaderView As SqlDataReader
        Dim sFile As String = ""
        Dim memStream As New MemoryStream()
        Dim writer As New StreamWriter(memStream)
        writer.AutoFlush = True
        intIdentificatieDetailrecord = 0
        Dim xxx As Integer = 0

        Try
            If Not ConfigurationManager.ConnectionStrings("IntomediFin") Is Nothing Then
                dbConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("IntomediFin").ToString())
                dbConnection.Open()
                'voorlooprecord
                dbCommandStandard.Connection = dbConnection
                dbCommandView.Connection = dbConnection
                dbCommandStandard.CommandText = "Select * From [VektisStandaardZH310] WHERE [Recordcode] = '01' ORDER BY [Volgnummer]"
                dbCommandView.CommandText = "Select * From [vIS_VECOZO_ZH310_01_Voorlooprecord] WHERE [OHWBatchID] = " & OHWBatchID.ToString
                dbReaderView = dbCommandView.ExecuteReader()
                Dim sLine As String = ""
                If dbReaderView.HasRows() Then
                    dbReaderView.Read()
                    dbReaderStandard = dbCommandStandard.ExecuteReader()
                    While dbReaderStandard.Read()
                        Dim sVeld = GetField("01", dbReaderStandard, dbReaderView)
                        sLine = String.Concat(sLine, sVeld)
                    End While
                End If
                dbReaderView.Close()
                dbReaderStandard.Close()
                sFile = sFile + sLine + Environment.NewLine
                Console.WriteLine(sFile)
                writer.WriteLine(sLine)


                Debug.Print("vIS_VECOZO_ZH310_02_VerzekerdenRecord")
                dbCommandStandard.CommandText = "Select * From [VektisStandaardZH310] WHERE [Recordcode] = '02' ORDER BY [Volgnummer]"
                dbCommandView.CommandText = "Select * From [vIS_VECOZO_ZH310_02_VerzekerdenRecord] WHERE [OHWBatchID] = " & OHWBatchID.ToString
                dbReaderView = dbCommandView.ExecuteReader()
                While dbReaderView.Read()
                    xxx += 1
                    Dim lOHWBatchLinenumber As Long = dbReaderView("OHWBatchLinenumber")
                    sLine = ""
                    dbReaderStandard = dbCommandStandard.ExecuteReader()
                    While dbReaderStandard.Read()
                        Dim sVeld = GetField("02", dbReaderStandard, dbReaderView)
                        sLine = String.Concat(sLine, sVeld)
                    End While
                    dbReaderStandard.Close()
                    Console.WriteLine(sFile)
                    sFile = sFile + sLine + Environment.NewLine
                    writer.WriteLine(sLine)

                    Dim dbCommandView2 As New SqlCommand
                    Dim dbCommandStandard2 As New SqlCommand
                    Dim dbReaderStandard2 As SqlDataReader
                    Dim dbReaderView2 As SqlDataReader
                    dbCommandStandard2.Connection = dbConnection
                    dbCommandView2.Connection = dbConnection

                    Debug.Print(xxx.ToString + ":vIS_VECOZO_ZH310_04_Prestatierecord")
                    dbCommandStandard2.CommandText = "Select * From [VektisStandaardZH310] WHERE [Recordcode] = '04' ORDER BY [Volgnummer]"
                    dbCommandView2.CommandText = "Select * From [vIS_VECOZO_ZH310_04_Prestatierecord] WHERE ([OHWBatchID] = " & OHWBatchID.ToString & ") AND ([OHWBatchLinenumber] = " & lOHWBatchLinenumber.ToString & ")"
                    dbReaderView2 = dbCommandView2.ExecuteReader()
                    While dbReaderView2.Read()
                        sLine = ""
                        dbReaderStandard2 = dbCommandStandard2.ExecuteReader()
                        While dbReaderStandard2.Read()
                            Dim sVeld = GetField("04", dbReaderStandard2, dbReaderView2)
                            sLine = String.Concat(sLine, sVeld)
                        End While
                        dbReaderStandard2.Close()
                        Console.WriteLine(sFile)
                        sFile = sFile + sLine + Environment.NewLine
                        writer.WriteLine(sLine)
                    End While
                    dbReaderView2.Close()

                    Debug.Print(xxx.ToString + ":vIS_VECOZO_ZH310_06_Tariefrecord")
                    dbCommandStandard2.CommandText = "Select * From [VektisStandaardZH310] WHERE [Recordcode] = '06' ORDER BY [Volgnummer]"
                    dbCommandView2.CommandText = "Select * From [vIS_VECOZO_ZH310_06_Tariefrecord] WHERE ([OHWBatchID] = " & OHWBatchID.ToString & ") AND ([OHWBatchLinenumber] = " & lOHWBatchLinenumber.ToString & ")"
                    dbReaderView2 = dbCommandView2.ExecuteReader()
                    While dbReaderView2.Read()
                        sLine = ""
                        dbReaderStandard2 = dbCommandStandard2.ExecuteReader()
                        While dbReaderStandard2.Read()
                            Dim sVeld = GetField("06", dbReaderStandard2, dbReaderView2)
                            sLine = String.Concat(sLine, sVeld)
                        End While
                        dbReaderStandard2.Close()
                        Console.WriteLine(sFile)
                        sFile = sFile + sLine + Environment.NewLine
                        writer.WriteLine(sLine)
                    End While
                    dbReaderView2.Close()

                End While
                dbReaderView.Close()


                Debug.Print(":vIS_VECOZO_ZH310_99_SluitRecord")
                dbCommandStandard.CommandText = "Select * From [VektisStandaardZH310] WHERE [Recordcode] = '99' ORDER BY [Volgnummer]"
                dbCommandView.CommandText = "Select * From [vIS_VECOZO_ZH310_99_SluitRecord] WHERE [OHWBatchID] = " & OHWBatchID.ToString
                dbReaderView = dbCommandView.ExecuteReader()
                Debug.Print(":vIS_VECOZO_ZH310_99_SluitRecord")
                While dbReaderView.Read()
                    Debug.Print("a")
                    sLine = ""
                    Debug.Print("b")
                    dbReaderStandard = dbCommandStandard.ExecuteReader()
                    Debug.Print("c")
                    While dbReaderStandard.Read()
                        Dim sVeld = GetField("99", dbReaderStandard, dbReaderView)
                        sLine = String.Concat(sLine, sVeld)
                    End While
                    Debug.Print("d")
                    dbReaderStandard.Close()
                    Debug.Print("e")
                    Console.WriteLine(sFile)
                    Debug.Print("f")
                    sFile = sFile + sLine + Environment.NewLine
                    Debug.Print("g")
                    writer.WriteLine(sLine)
                End While
                Debug.Print("z")
                dbReaderView.Close()
                dbConnection.Close()
                Return memStream

            End If
        Catch ex As Exception
            'MsgBox(ex.Message)
            If dbConnection.State = ConnectionState.Open Then
                dbConnection.Close()
            End If
            Return Nothing
        End Try
    End Function

    Function GetVektisZH308File(ByVal VecozoBatchID As Integer) As MemoryStream
        Dim dbCommandView As New SqlCommand
        Dim dbCommandStandard As New SqlCommand
        Dim dbReaderStandard As SqlDataReader = Nothing
        Dim dbReaderView As SqlDataReader
        Dim sFile As String = ""
        Dim memStream As New MemoryStream()
        Dim writer As New StreamWriter(memStream)
        writer.AutoFlush = True
        intIdentificatieDetailrecord = 0
        Try
            dbConnection.Open()

            'voorlooprecord
            dbCommandStandard.Connection = dbConnection
            dbCommandView.Connection = dbConnection
            dbCommandStandard.CommandText = "Select * From [VektisStandaardZH308] WHERE [Recordcode] = '01' ORDER BY [Volgnummer]"
            dbCommandView.CommandText = "Select * From [vIS_VECOZO_ZH308_01_Voorlooprecord] WHERE [VecozoBatchID] = " & VecozoBatchID.ToString
            dbReaderView = dbCommandView.ExecuteReader()
            Dim sLine As String = ""
            If dbReaderView.HasRows() Then
                dbReaderView.Read()
                dbReaderStandard = dbCommandStandard.ExecuteReader()
                While dbReaderStandard.Read()
                    Dim sVeld As String = GetField("01", dbReaderStandard, dbReaderView)
                    sLine = String.Concat(sLine, sVeld)
                End While
            End If
            dbReaderView.Close()
            dbReaderStandard.Close()
            sFile = sFile + sLine + Environment.NewLine
            Console.WriteLine(sFile)
            writer.WriteLine(sLine)

            dbCommandStandard.CommandText = "Select * From [VektisStandaardZH308] WHERE [Recordcode] = '02' ORDER BY [Volgnummer]"
            dbCommandView.CommandText = "Select * From [vIS_VECOZO_ZH308_02_VerzekerdenRecord] WHERE [VecozoBatchID] = " & VecozoBatchID.ToString
            dbReaderView = dbCommandView.ExecuteReader()
            While dbReaderView.Read()

                Dim lVecozoBatchLinenumber As Long = dbReaderView("VecozoBatchLinenumber")
                sLine = ""
                dbReaderStandard = dbCommandStandard.ExecuteReader()
                While dbReaderStandard.Read()
                    Dim sVeld = GetField("02", dbReaderStandard, dbReaderView)
                    sLine = String.Concat(sLine, sVeld)
                End While
                dbReaderStandard.Close()
                Console.WriteLine(sFile)
                sFile = sFile + sLine + Environment.NewLine
                writer.WriteLine(sLine)

                Dim dbCommandView2 As New SqlCommand
                Dim dbCommandStandard2 As New SqlCommand
                Dim dbReaderStandard2 As SqlDataReader
                Dim dbReaderView2 As SqlDataReader
                dbCommandStandard2.Connection = dbConnection
                dbCommandView2.Connection = dbConnection

                dbCommandStandard2.CommandText = "Select * From [VektisStandaardZH308] WHERE [Recordcode] = '04' ORDER BY [Volgnummer]"
                dbCommandView2.CommandText = "Select * From [vIS_VECOZO_ZH308_04_Prestatierecord] WHERE ([VecozoBatchID] = " & VecozoBatchID.ToString & ") AND ([VecozoBatchLinenumber] = " & lVecozoBatchLinenumber.ToString & ")"
                dbReaderView2 = dbCommandView2.ExecuteReader()
                While dbReaderView2.Read()
                    sLine = ""
                    dbReaderStandard2 = dbCommandStandard2.ExecuteReader()
                    While dbReaderStandard2.Read()
                        Dim sVeld = GetField("04", dbReaderStandard2, dbReaderView2)
                        sLine = String.Concat(sLine, sVeld)
                    End While
                    dbReaderStandard2.Close()
                    Console.WriteLine(sFile)
                    sFile = sFile + sLine + Environment.NewLine
                    writer.WriteLine(sLine)
                End While
                dbReaderView2.Close()

                dbCommandStandard2.CommandText = "Select * From [VektisStandaardZH308] WHERE [Recordcode] = '06' ORDER BY [Volgnummer]"
                dbCommandView2.CommandText = "Select * From [vIS_VECOZO_ZH308_06_Tariefrecord] WHERE ([VecozoBatchID] = " & VecozoBatchID.ToString & ") AND ([VecozoBatchLinenumber] = " & lVecozoBatchLinenumber.ToString & ")"
                dbReaderView2 = dbCommandView2.ExecuteReader()
                While dbReaderView2.Read()
                    sLine = ""
                    dbReaderStandard2 = dbCommandStandard2.ExecuteReader()
                    While dbReaderStandard2.Read()
                        Dim sVeld = GetField("06", dbReaderStandard2, dbReaderView2)
                        sLine = String.Concat(sLine, sVeld)
                    End While
                    dbReaderStandard2.Close()
                    Console.WriteLine(sFile)
                    sFile = sFile + sLine + Environment.NewLine
                    writer.WriteLine(sLine)
                End While
                dbReaderView2.Close()

                dbCommandStandard2.CommandText = "Select * From [VektisStandaardZH308] WHERE [Recordcode] = '16' ORDER BY [Volgnummer]"
                dbCommandView2.CommandText = "Select * From [vIS_VECOZO_ZH308_16_Zorgactiviteitrecord] WHERE ([VecozoBatchID] = " & VecozoBatchID.ToString & ") AND ([VecozoBatchLinenumber] = " & lVecozoBatchLinenumber.ToString & ")"
                dbReaderView2 = dbCommandView2.ExecuteReader()
                While dbReaderView2.Read()
                    sLine = ""
                    dbReaderStandard2 = dbCommandStandard2.ExecuteReader()
                    While dbReaderStandard2.Read()
                        Dim sVeld = GetField("16", dbReaderStandard2, dbReaderView2)
                        sLine = String.Concat(sLine, sVeld)
                    End While
                    dbReaderStandard2.Close()
                    Console.WriteLine(sFile)
                    sFile = sFile + sLine + Environment.NewLine
                    writer.WriteLine(sLine)
                End While
                dbReaderView2.Close()

            End While
            dbReaderView.Close()

            dbCommandStandard.CommandText = "Select * From [VektisStandaardZH308] WHERE [Recordcode] = '99' ORDER BY [Volgnummer]"
            dbCommandView.CommandText = "Select * From [vIS_VECOZO_ZH308_99_SluitRecord] WHERE [VecozoBatchID] = " & VecozoBatchID.ToString
            dbReaderView = dbCommandView.ExecuteReader()
            While dbReaderView.Read()
                sLine = ""
                dbReaderStandard = dbCommandStandard.ExecuteReader()
                While dbReaderStandard.Read()
                    Dim sVeld = GetField("99", dbReaderStandard, dbReaderView)
                    sLine = String.Concat(sLine, sVeld)
                End While
                dbReaderStandard.Close()
                Console.WriteLine(sFile)
                sFile = sFile + sLine + Environment.NewLine
                writer.WriteLine(sLine)
            End While
            dbReaderView.Close()
            dbConnection.Close()
            Return memStream
        Catch ex As Exception
            Me.Message = ex.Message + If(ex.InnerException Is Nothing, "", vbNewLine + ex.InnerException.ToString)
            Me.hasError = True
            If dbConnection.State = ConnectionState.Open Then
                dbConnection.Close()
            End If
            Return Nothing
        End Try
    End Function

    Private Function GetField(ByVal Segment As String, ByVal dbReaderStandard As SqlDataReader, ByVal dbReaderView As SqlDataReader) As String
        Dim x As String = ""
        Dim x2 As String = ""
        Dim sVeld As String = ""
        Try
            x = dbReaderStandard("NaamGegevenselement")
            'Debug.Assert(x <> "FACTUURNUMMER DECLARANT")
            x2 = dbReaderStandard("FieldInView")
            'Debug.Assert(x <> "BTW-PERCENTAGE DECLARATIEBEDRAG")
            Dim iLength As Integer = dbReaderStandard("Lengte")
            Dim sType As String = dbReaderStandard("Type")
            Dim sFormat As String = dbReaderStandard("Patroon")
            If dbReaderStandard("NaamGegevenselement") = "IDENTIFICATIE DETAILRECORD" Then
                intIdentificatieDetailrecord += 1
                sVeld = intIdentificatieDetailrecord.ToString
                sVeld = Long.Parse(sVeld).ToString(sFormat)
            ElseIf dbReaderStandard("NaamGegevenselement") = "TOTAAL AANTAL DETAILRECORDS" Then
                sVeld = intIdentificatieDetailrecord.ToString
                sVeld = Long.Parse(sVeld).ToString(sFormat)
            Else
                If sType = "D" Then
                    If Not IsDBNull(dbReaderView(dbReaderStandard("FieldInView"))) Then
                        sVeld = Date.Parse(dbReaderView(dbReaderStandard("FieldInView"))).ToString(sFormat)
                    End If
                ElseIf sType = "N" Then
                    If Not IsDBNull(dbReaderView(dbReaderStandard("FieldInView"))) Then
                        sVeld = Long.Parse(dbReaderView(dbReaderStandard("FieldInView"))).ToString(sFormat)
                    Else
                        sVeld = "0"
                        sVeld = Long.Parse(sVeld).ToString(sFormat)
                    End If
                Else
                    If Not IsDBNull(dbReaderView(dbReaderStandard("FieldInView"))) Then
                        sVeld = dbReaderView(dbReaderStandard("FieldInView"))
                    End If
                End If
            End If
            Return sVeld.PadRight(iLength)
        Catch ex As Exception
            Debug.Print("Segment: '" + Segment + "' - NaamGegevenselement: '" + x + "' - FieldInView: '" + x2 + "' - Error: '" + ex.Message + "'")
            Return ""
        End Try
    End Function
End Class

Public Class VektisDeclaratieRetour
    Inherits Vektis

    Enum VBLStatusType
        Onbekend = 0
        Open = 1
        Toegewezen = 2
        Afgewezen = 3
        DeelsToegewezen = 4
    End Enum

    Enum VBStatusType
        Open = 1
        Verstuurd = 2
        Toegewezen = 3
        Afgewezen = 4
        DeelsToegewezen = 5
        Verwijderd = 6
    End Enum

    Public Class VecozoBatchLineTable
        Private mVecozoBatchLinenumber As Integer
        Private mInvoiceID As Integer
        Private mRequested As Double
        Private mAssigned As Double
        Private mStatus As VBLStatusType

        Public Property VecozoBatchLinenumber As Integer
            Get
                Return mVecozoBatchLinenumber
            End Get
            Set(value As Integer)
                mVecozoBatchLinenumber = value
            End Set
        End Property

        Public Property InvoiceID As Integer
            Get
                Return mInvoiceID
            End Get
            Set(value As Integer)
                mInvoiceID = value
            End Set
        End Property

        Public Property Requested As Double
            Get
                Return mRequested
            End Get
            Set(value As Double)
                mRequested = value
            End Set
        End Property

        Public Property Status As VBLStatusType
            Get
                Return mStatus
            End Get
            Set(value As VBLStatusType)
                mStatus = value
            End Set
        End Property

        Public Property Assigned As Double
            Get
                Return mAssigned
            End Get
            Set(value As Double)
                mAssigned = value
            End Set
        End Property
    End Class

    Public Class Resultaat
        Enum StatusType
            None = -1
            Succes = 3
            SuccesMetOpmerking = 4
            Fout = 5
            Waarschuwing = 6
        End Enum

        Enum ResultaatcodeType
            None = -1
            I2M_DEL = 1000
            I2M_VERW = 1001
            I2M_SEND = 1002
            I2M_PROC = 1003
            I2M_VAL = 1004
            I2M_NFOUND = 1005
            I2M_NCON = 1006
            I2M_MSG = 1007
            I2M_OTH = 1008
            I2M_VALG = 1009
            I2M_NVSP = 1010
            I2M_VALID = 1011
            I2M_VALONTV = 1011
            I2M_VALVERPL = 1012
            I2M_VALAFGK = 1013
            I2M_DECLBEST = 1014
            I2M_VFNFOUND = 1015
            I2M_VFERR = 1016
            I2M_DALG = 1017
            I2M_DALID = 1018
            I2M_MODVSPEDP = 1019
            I2M_VRNFOUND = 1020
            I2M_DRNFOUND = 1021
            I2M_SENDVSP = 1022
        End Enum

        Public Class VoorloopRecord
            Private mStandaardcode As String
            Private mFactuurnummerDeclarant As String
            Private mReferentienummerZorgverzekeraar As String
            Private mDagtekeningRetourbericht As Date
            Private mZorgverlenerscode As Long
            Private mPraktijkcode As Long
            Private mInstellingscode As Long
            Private mRetourcode1 As String
            Private mRetourcode2 As String
            Private mRetourcode3 As String

            Public Property Standaardcode() As String
                Get
                    Return mStandaardcode
                End Get
                Set(ByVal value As String)
                    mStandaardcode = value
                End Set
            End Property

            Public Property FactuurnummerDeclarant() As String
                Get
                    Return mFactuurnummerDeclarant
                End Get
                Set(ByVal value As String)
                    mFactuurnummerDeclarant = value
                End Set
            End Property

            Public Property ReferentienummerZorgverzekeraar() As String
                Get
                    Return mReferentienummerZorgverzekeraar
                End Get
                Set(ByVal value As String)
                    mReferentienummerZorgverzekeraar = value
                End Set
            End Property

            Public Property DagtekeningRetourbericht() As Date
                Get
                    Return mDagtekeningRetourbericht
                End Get
                Set(ByVal value As Date)
                    mDagtekeningRetourbericht = value
                End Set
            End Property

            Public Property Zorgverlenerscode() As Long
                Get
                    Return mZorgverlenerscode
                End Get
                Set(ByVal value As Long)
                    mZorgverlenerscode = value
                End Set
            End Property

            Public Property Praktijkcode() As Long
                Get
                    Return mPraktijkcode
                End Get
                Set(ByVal value As Long)
                    mPraktijkcode = value
                End Set
            End Property

            Public Property Instellingscode() As Long
                Get
                    Return mInstellingscode
                End Get
                Set(ByVal value As Long)
                    mInstellingscode = value
                End Set
            End Property

            Public Property Retourcode1() As String
                Get
                    Return mRetourcode1
                End Get
                Set(ByVal value As String)
                    mRetourcode1 = value
                End Set
            End Property

            Public Property Retourcode2() As String
                Get
                    Return mRetourcode2
                End Get
                Set(ByVal value As String)
                    mRetourcode2 = value
                End Set
            End Property

            Public Property Retourcode3() As String
                Get
                    Return mRetourcode3
                End Get
                Set(ByVal value As String)
                    mRetourcode3 = value
                End Set
            End Property
        End Class

        Public Class RegelRecord
            Public Class VerzekerdeRecord
                Private mIdentificatieDetailrecord As Integer
                Private mRetourcode1 As String
                Private mRetourcode2 As String
                Private mRetourcode3 As String

                Public Property IdentificatieDetailrecord() As Integer
                    Get
                        Return mIdentificatieDetailrecord
                    End Get
                    Set(ByVal value As Integer)
                        mIdentificatieDetailrecord = value
                    End Set
                End Property

                Public Property Retourcode1() As String
                    Get
                        Return mRetourcode1
                    End Get
                    Set(ByVal value As String)
                        mRetourcode1 = value
                    End Set
                End Property

                Public Property Retourcode2() As String
                    Get
                        Return mRetourcode2
                    End Get
                    Set(ByVal value As String)
                        mRetourcode2 = value
                    End Set
                End Property

                Public Property Retourcode3() As String
                    Get
                        Return mRetourcode3
                    End Get
                    Set(ByVal value As String)
                        mRetourcode3 = value
                    End Set
                End Property
            End Class

            Public Class PrestatieRecord
                Private mIdentificatieDetailrecord As Integer
                Private mReferentienummerDitRecord As String
                Private mDeclaratieBedragInclBTW As Double
                Private mIndicatieDebetCredit2 As String
                Private mBerekendBedragZorgverzekeraar As Double
                Private mIndicatieDebetCredit3 As String
                Private mToegekendBedrag As Double
                Private mIndicatieDebetCredit4 As String
                Private mRetourcode1 As String
                Private mRetourcode2 As String
                Private mRetourcode3 As String

                Public Property IdentificatieDetailrecord() As Integer
                    Get
                        Return mIdentificatieDetailrecord
                    End Get
                    Set(ByVal value As Integer)
                        mIdentificatieDetailrecord = value
                    End Set
                End Property

                Public Property ReferentienummerDitRecord() As String
                    Get
                        Return mReferentienummerDitRecord
                    End Get
                    Set(ByVal value As String)
                        mReferentienummerDitRecord = value
                    End Set
                End Property

                Public Property DeclaratieBedragInclBTW() As Double
                    Get
                        Return mDeclaratieBedragInclBTW
                    End Get
                    Set(ByVal value As Double)
                        mDeclaratieBedragInclBTW = value
                    End Set
                End Property

                Public Property IndicatieDebetCredit2() As String
                    Get
                        Return mIndicatieDebetCredit2
                    End Get
                    Set(ByVal value As String)
                        mIndicatieDebetCredit2 = value
                    End Set
                End Property

                Public Property BerekendBedragZorgverzekeraar() As Double
                    Get
                        Return mBerekendBedragZorgverzekeraar
                    End Get
                    Set(ByVal value As Double)
                        mBerekendBedragZorgverzekeraar = value
                    End Set
                End Property

                Public Property IndicatieDebetCredit3() As String
                    Get
                        Return mIndicatieDebetCredit3
                    End Get
                    Set(ByVal value As String)
                        mIndicatieDebetCredit3 = value
                    End Set
                End Property

                Public Property ToegekendBedrag() As Double
                    Get
                        Return mToegekendBedrag
                    End Get
                    Set(ByVal value As Double)
                        mToegekendBedrag = value
                    End Set
                End Property

                Public Property IndicatieDebetCredit4() As String
                    Get
                        Return mIndicatieDebetCredit4
                    End Get
                    Set(ByVal value As String)
                        mIndicatieDebetCredit4 = value
                    End Set
                End Property

                Public Property Retourcode1() As String
                    Get
                        Return mRetourcode1
                    End Get
                    Set(ByVal value As String)
                        mRetourcode1 = value
                    End Set
                End Property

                Public Property Retourcode2() As String
                    Get
                        Return mRetourcode2
                    End Get
                    Set(ByVal value As String)
                        mRetourcode2 = value
                    End Set
                End Property

                Public Property Retourcode3() As String
                    Get
                        Return mRetourcode3
                    End Get
                    Set(ByVal value As String)
                        mRetourcode3 = value
                    End Set
                End Property
            End Class

            Public Class TariefRecord
                Private mIdentificatieDetailrecord As Integer
                Private mReferentienummerDitRecord As String
                Private mBerekendBedragZorgverzekeraar As Double
                Private mDeclaratieBedragInclBTW As Double
                Private mIndicatieDebetCredit2 As String
                Private mIndicatieDebetCredit3 As String
                Private mToegekendBedrag As Double
                Private mIndicatieDebetCredit4 As String
                Private mRetourcode1 As String
                Private mRetourcode2 As String
                Private mRetourcode3 As String

                Public Property IdentificatieDetailrecord() As Integer
                    Get
                        Return mIdentificatieDetailrecord
                    End Get
                    Set(ByVal value As Integer)
                        mIdentificatieDetailrecord = value
                    End Set
                End Property

                Public Property ReferentienummerDitRecord() As String
                    Get
                        Return ReferentienummerDitRecord
                    End Get
                    Set(ByVal value As String)
                        mReferentienummerDitRecord = value
                    End Set
                End Property

                Public Property DeclaratieBedragInclBTW() As Double
                    Get
                        Return mDeclaratieBedragInclBTW
                    End Get
                    Set(ByVal value As Double)
                        mDeclaratieBedragInclBTW = value
                    End Set
                End Property

                Public Property IndicatieDebetCredit2() As String
                    Get
                        Return mIndicatieDebetCredit2
                    End Get
                    Set(ByVal value As String)
                        mIndicatieDebetCredit2 = value
                    End Set
                End Property

                Public Property BerekendBedragZorgverzekeraar() As Double
                    Get
                        Return mBerekendBedragZorgverzekeraar
                    End Get
                    Set(ByVal value As Double)
                        mBerekendBedragZorgverzekeraar = value
                    End Set
                End Property

                Public Property IndicatieDebetCredit3() As String
                    Get
                        Return mIndicatieDebetCredit3
                    End Get
                    Set(ByVal value As String)
                        mIndicatieDebetCredit3 = value
                    End Set
                End Property

                Public Property ToegekendBedrag() As Double
                    Get
                        Return mToegekendBedrag
                    End Get
                    Set(ByVal value As Double)
                        mToegekendBedrag = value
                    End Set
                End Property

                Public Property IndicatieDebetCredit4() As String
                    Get
                        Return mIndicatieDebetCredit4
                    End Get
                    Set(ByVal value As String)
                        mIndicatieDebetCredit4 = value
                    End Set
                End Property

                Public Property Retourcode1() As String
                    Get
                        Return mRetourcode1
                    End Get
                    Set(ByVal value As String)
                        mRetourcode1 = value
                    End Set
                End Property

                Public Property Retourcode2() As String
                    Get
                        Return mRetourcode2
                    End Get
                    Set(ByVal value As String)
                        mRetourcode2 = value
                    End Set
                End Property

                Public Property Retourcode3() As String
                    Get
                        Return mRetourcode3
                    End Get
                    Set(ByVal value As String)
                        mRetourcode3 = value
                    End Set
                End Property
            End Class

            Public Class ZorgactiviteitRecord
                Private mIdentificatieDetailrecord As Integer
                Private mReferentienummerDitRecord As String
                Private mRetourcode1 As String
                Private mRetourcode2 As String
                Private mRetourcode3 As String

                Public Property IdentificatieDetailrecord() As Integer
                    Get
                        Return mIdentificatieDetailrecord
                    End Get
                    Set(ByVal value As Integer)
                        mIdentificatieDetailrecord = value
                    End Set
                End Property

                Public Property ReferentienummerDitRecord() As String
                    Get
                        Return mReferentienummerDitRecord
                    End Get
                    Set(ByVal value As String)
                        mReferentienummerDitRecord = value
                    End Set
                End Property

                Public Property Retourcode1() As String
                    Get
                        Return mRetourcode1
                    End Get
                    Set(ByVal value As String)
                        mRetourcode1 = value
                    End Set
                End Property

                Public Property Retourcode2() As String
                    Get
                        Return mRetourcode2
                    End Get
                    Set(ByVal value As String)
                        mRetourcode2 = value
                    End Set
                End Property

                Public Property Retourcode3() As String
                    Get
                        Return mRetourcode3
                    End Get
                    Set(ByVal value As String)
                        mRetourcode3 = value
                    End Set
                End Property
            End Class

            Private mVecozoBatchLineID As Long
            Private mVerzekerde As VerzekerdeRecord
            Private mPrestatie As PrestatieRecord
            Private mTarieven As List(Of TariefRecord)
            Private mZorgactiviteiten As List(Of ZorgactiviteitRecord)

            Public Sub AddTarief(aTarief As TariefRecord)
                If mTarieven Is Nothing Then mTarieven = New List(Of TariefRecord)
                mTarieven.Add(aTarief)
            End Sub

            Public Sub AddZorgactiviteit(aZorgacitiviteit As ZorgactiviteitRecord)
                If mZorgactiviteiten Is Nothing Then mZorgactiviteiten = New List(Of ZorgactiviteitRecord)
                mZorgactiviteiten.Add(aZorgacitiviteit)
            End Sub

            Public Property Verzekerde() As VerzekerdeRecord
                Get
                    Return mVerzekerde
                End Get
                Set(ByVal value As VerzekerdeRecord)
                    mVerzekerde = value
                End Set
            End Property

            Public Property Prestatie() As PrestatieRecord
                Get
                    Return mPrestatie
                End Get
                Set(ByVal value As PrestatieRecord)
                    mPrestatie = value
                End Set
            End Property

            Public Property Tarieven() As List(Of TariefRecord)
                Get
                    Return mTarieven
                End Get
                Set(ByVal value As List(Of TariefRecord))
                    mTarieven = value
                End Set
            End Property

            Public Property Zorgactiviteiten() As List(Of ZorgactiviteitRecord)
                Get
                    Return mZorgactiviteiten
                End Get
                Set(ByVal value As List(Of ZorgactiviteitRecord))
                    mZorgactiviteiten = value
                End Set
            End Property

            Public Property VecozoBatchLineID As Long
                Get
                    Return mVecozoBatchLineID
                End Get
                Set(value As Long)
                    mVecozoBatchLineID = value
                End Set
            End Property
        End Class

        Public Class SluitRecord
            Private mTotaalIngediendBedrag As Double
            Private mIndicatieDebetCredit01 As String
            Private mTotaalToegekendBedrag As Double
            Private mIndicatieDebetCredit02 As String

            Public Property TotaalIngediendBedrag() As Double
                Get
                    Return mTotaalIngediendBedrag
                End Get
                Set(ByVal value As Double)
                    mTotaalIngediendBedrag = value
                End Set
            End Property

            Public Property IndicatieDebetCredit01() As String
                Get
                    Return mIndicatieDebetCredit01
                End Get
                Set(ByVal value As String)
                    mIndicatieDebetCredit01 = value
                End Set
            End Property

            Public Property TotaalToegekendBedrag() As Double
                Get
                    Return mTotaalToegekendBedrag
                End Get
                Set(ByVal value As Double)
                    mTotaalToegekendBedrag = value
                End Set
            End Property

            Public Property IndicatieDebetCredit02() As String
                Get
                    Return mIndicatieDebetCredit02
                End Get
                Set(ByVal value As String)
                    mIndicatieDebetCredit02 = value
                End Set
            End Property
        End Class

        Private mVecozoBatchID As Long
        Private mResultaatCode As ResultaatcodeType
        Private mResultaatTextExtra As String
        Private mStatus As StatusType
        Private mFilename As String
        Private mFile As String
        Private mVoorloop As VoorloopRecord
        Private mSluit As SluitRecord
        Private mRegels As List(Of Resultaat.RegelRecord)

        Public Sub AddRegel(aRegel As RegelRecord)
            If mRegels Is Nothing Then mRegels = New List(Of Resultaat.RegelRecord)
            mRegels.Add(aRegel)
        End Sub

        Public Function GetRegel(InvoiceID As Long) As Resultaat.RegelRecord
            If Regels IsNot Nothing Then
                For Each aRegel As RegelRecord In Regels
                    If aRegel.Prestatie IsNot Nothing Then
                        If aRegel.Prestatie.ReferentienummerDitRecord = InvoiceID Then
                            Return aRegel
                        End If
                    End If
                Next
            End If
            Return Nothing
        End Function

        Public Function DebugText() As String
            Dim aMessage As String = ""

            If Voorloop IsNot Nothing Then
                aMessage = aMessage & "Standaardcode = " & Voorloop.Standaardcode & vbCrLf
                aMessage = aMessage & "FactuurnummerDeclarant = " & Voorloop.FactuurnummerDeclarant & vbCrLf
                aMessage = aMessage & "ReferentienummerZorgverzekeraar = " & Voorloop.ReferentienummerZorgverzekeraar & vbCrLf
                aMessage = aMessage & "DagtekeningRetourbericht = " & Voorloop.DagtekeningRetourbericht.ToShortDateString & vbCrLf
                aMessage = aMessage & "Zorgverlenerscode = " & Voorloop.Zorgverlenerscode.ToString & vbCrLf
                aMessage = aMessage & "Praktijkcode = " & Voorloop.Praktijkcode.ToString & vbCrLf
                aMessage = aMessage & "Instellingscode = " & Voorloop.Instellingscode.ToString & vbCrLf
                aMessage = aMessage & "Retourcode1 = " & Voorloop.Retourcode1 & vbCrLf
                aMessage = aMessage & "Retourcode2 = " & Voorloop.Retourcode2 & vbCrLf
                aMessage = aMessage & "Retourcode3 = " & Voorloop.Retourcode3 & vbCrLf
            End If
            If Sluit IsNot Nothing Then
                aMessage = aMessage & "TotaalIngediendBedrag = " & Sluit.TotaalIngediendBedrag.ToString & vbCrLf
                aMessage = aMessage & "IndicatieDebetCredit01 = " & Sluit.IndicatieDebetCredit01 & vbCrLf
                aMessage = aMessage & "TotaalToegekendBedrag = " & Sluit.TotaalToegekendBedrag.ToString & vbCrLf
                aMessage = aMessage & "IndicatieDebetCredit02 = " & Sluit.IndicatieDebetCredit02 & vbCrLf
            End If
            If Regels IsNot Nothing Then
                Dim iRegel As Integer = 1
                For Each aRegel As RegelRecord In Regels
                    aMessage = aMessage & "  Regel " & iRegel.ToString & "\" & Regels.Count.ToString + vbCrLf
                    If aRegel.Verzekerde IsNot Nothing Then
                        aMessage = aMessage & "    Verzekerde" & vbCrLf
                        aMessage = aMessage & "      IdentificatieDetailrecord = " & aRegel.Verzekerde.IdentificatieDetailrecord & vbCrLf
                        aMessage = aMessage & "      Retourcode1 = " & aRegel.Verzekerde.Retourcode1 & vbCrLf
                        aMessage = aMessage & "      Retourcode2 = " & aRegel.Verzekerde.Retourcode2 & vbCrLf
                        aMessage = aMessage & "      Retourcode3 = " & aRegel.Verzekerde.Retourcode3 & vbCrLf
                    End If
                    If aRegel.Prestatie IsNot Nothing Then
                        aMessage = aMessage & "    Prestatie" & vbCrLf
                        aMessage = aMessage & "      IdentificatieDetailrecord = " & aRegel.Prestatie.IdentificatieDetailrecord & vbCrLf
                        aMessage = aMessage & "      ReferentienummerDitRecord = " & aRegel.Prestatie.ReferentienummerDitRecord & vbCrLf
                        If Voorloop.Standaardcode = "DG302" Then
                            aMessage = aMessage & "      DeclaratieBedragInclBTW = " & aRegel.Prestatie.DeclaratieBedragInclBTW.ToString & vbCrLf
                            aMessage = aMessage & "      IndicatieDebetCredit2 = " & aRegel.Prestatie.IndicatieDebetCredit2 & vbCrLf
                            aMessage = aMessage & "      BerekendBedragZorgverzekeraar = " & aRegel.Prestatie.BerekendBedragZorgverzekeraar.ToString & vbCrLf
                            aMessage = aMessage & "      IndicatieDebetCredit3 = " & aRegel.Prestatie.IndicatieDebetCredit3 & vbCrLf
                            aMessage = aMessage & "      ToegekendBedrag = " & aRegel.Prestatie.ToegekendBedrag.ToString & vbCrLf
                            aMessage = aMessage & "      IndicatieDebetCredit4 = " & aRegel.Prestatie.IndicatieDebetCredit4 & vbCrLf
                        End If
                        aMessage = aMessage & "      Retourcode1 = " & aRegel.Prestatie.Retourcode1 & vbCrLf
                        aMessage = aMessage & "      Retourcode2 = " & aRegel.Prestatie.Retourcode2 & vbCrLf
                        aMessage = aMessage & "      Retourcode3 = " & aRegel.Prestatie.Retourcode3 & vbCrLf
                    End If
                    If aRegel.Tarieven IsNot Nothing Then
                        Dim i As Integer = 1
                        For Each aTarief As Resultaat.RegelRecord.TariefRecord In aRegel.Tarieven
                            aMessage = aMessage & "    Tarief " & i.ToString & "\" & aRegel.Tarieven.Count.ToString + vbCrLf
                            aMessage = aMessage & "      IdentificatieDetailrecord = " & aTarief.IdentificatieDetailrecord.ToString & vbCrLf
                            aMessage = aMessage & "      ReferentienummerDitRecord = " & aTarief.ReferentienummerDitRecord & vbCrLf
                            aMessage = aMessage & "      DeclaratieBedragInclBTW = " & aTarief.DeclaratieBedragInclBTW.ToString & vbCrLf
                            aMessage = aMessage & "      IndicatieDebetCredit2 = " & aTarief.IndicatieDebetCredit2 & vbCrLf
                            aMessage = aMessage & "      BerekendBedragZorgverzekeraar = " & aTarief.BerekendBedragZorgverzekeraar.ToString & vbCrLf
                            aMessage = aMessage & "      IndicatieDebetCredit3 = " & aTarief.IndicatieDebetCredit3 & vbCrLf
                            aMessage = aMessage & "      ToegekendBedrag = " & aTarief.ToegekendBedrag.ToString & vbCrLf
                            aMessage = aMessage & "      IndicatieDebetCredit4 = " & aTarief.IndicatieDebetCredit4 & vbCrLf
                            aMessage = aMessage & "      Retourcode1 = " & aTarief.Retourcode1 & vbCrLf
                            aMessage = aMessage & "      Retourcode2 = " & aTarief.Retourcode2 & vbCrLf
                            aMessage = aMessage & "      Retourcode3 = " & aTarief.Retourcode3 & vbCrLf
                            i += 1
                        Next
                    End If
                    If aRegel.Zorgactiviteiten IsNot Nothing Then
                        Dim i As Integer = 1
                        For Each aZorgactiviteit As Resultaat.RegelRecord.ZorgactiviteitRecord In aRegel.Zorgactiviteiten
                            aMessage = aMessage & "    Zorgactiviteit " & i.ToString & "\" & aRegel.Zorgactiviteiten.Count.ToString + vbCrLf
                            aMessage = aMessage & "      IdentificatieDetailrecord = " & aZorgactiviteit.IdentificatieDetailrecord.ToString & vbCrLf
                            aMessage = aMessage & "      ReferentienummerDitRecord = " & aZorgactiviteit.ReferentienummerDitRecord & vbCrLf
                            aMessage = aMessage & "      Retourcode1 = " & aZorgactiviteit.Retourcode1 & vbCrLf
                            aMessage = aMessage & "      Retourcode2 = " & aZorgactiviteit.Retourcode2 & vbCrLf
                            aMessage = aMessage & "      Retourcode3 = " & aZorgactiviteit.Retourcode3 & vbCrLf
                            i += 1
                        Next
                    End If
                    iRegel += 1
                Next
            End If

            Return aMessage
        End Function

        Public Function DebugTextLight() As String
            Dim aMessage As String = ""

            If Voorloop IsNot Nothing Then
                aMessage = aMessage & "Standaardcode = " & Voorloop.Standaardcode & vbCrLf
                aMessage = aMessage & "FactuurnummerDeclarant = " & Voorloop.FactuurnummerDeclarant & vbCrLf
            End If
            If Sluit IsNot Nothing Then
                aMessage = aMessage & "TotaalIngediendBedrag = " & Sluit.TotaalIngediendBedrag.ToString & " " & Sluit.IndicatieDebetCredit01 & vbCrLf
                aMessage = aMessage & "TotaalToegekendBedrag = " & Sluit.TotaalToegekendBedrag.ToString & " " & Sluit.IndicatieDebetCredit02 & vbCrLf
            End If
            If Regels IsNot Nothing Then
                Dim iRegel As Integer = 1
                For Each aRegel As RegelRecord In Regels
                    aMessage = aMessage & "  Regel " & iRegel.ToString & "\" & Regels.Count.ToString + vbCrLf
                    If aRegel.Prestatie IsNot Nothing Then
                        aMessage = aMessage & "    Prestatie" & vbCrLf
                        aMessage = aMessage & "      ReferentienummerDitRecord = " & aRegel.Prestatie.ReferentienummerDitRecord & vbCrLf
                        If Voorloop.Standaardcode = "DG302" Then
                            aMessage = aMessage & "      DeclaratieBedragInclBTW = " & aRegel.Prestatie.DeclaratieBedragInclBTW.ToString & " " & aRegel.Prestatie.IndicatieDebetCredit2 & vbCrLf
                            aMessage = aMessage & "      BerekendBedragZorgverzekeraar = " & aRegel.Prestatie.BerekendBedragZorgverzekeraar.ToString & " " & aRegel.Prestatie.IndicatieDebetCredit3 & vbCrLf
                            aMessage = aMessage & "      ToegekendBedrag = " & aRegel.Prestatie.ToegekendBedrag.ToString & " " & aRegel.Prestatie.IndicatieDebetCredit3 & vbCrLf
                        End If
                    End If
                    If aRegel.Tarieven IsNot Nothing Then
                        Dim i As Integer = 1
                        For Each aTarief As Resultaat.RegelRecord.TariefRecord In aRegel.Tarieven
                            aMessage = aMessage & "    Tarief " & i.ToString & "\" & aRegel.Tarieven.Count.ToString + vbCrLf
                            aMessage = aMessage & "      DeclaratieBedragInclBTW = " & aTarief.DeclaratieBedragInclBTW.ToString & " " & aTarief.IndicatieDebetCredit2 & vbCrLf
                            aMessage = aMessage & "      BerekendBedragZorgverzekeraar = " & aTarief.BerekendBedragZorgverzekeraar.ToString & " " & aTarief.IndicatieDebetCredit3 & vbCrLf
                            aMessage = aMessage & "      ToegekendBedrag = " & aTarief.ToegekendBedrag.ToString & " " & aTarief.IndicatieDebetCredit4 & vbCrLf
                            i += 1
                        Next
                    End If
                    iRegel += 1
                Next
            End If

            Return aMessage
        End Function

        Public Property Voorloop() As Resultaat.VoorloopRecord
            Get
                Return mVoorloop
            End Get
            Set(ByVal value As Resultaat.VoorloopRecord)
                mVoorloop = value
            End Set
        End Property

        Public Property Sluit() As Resultaat.SluitRecord
            Get
                Return mSluit
            End Get
            Set(ByVal value As Resultaat.SluitRecord)
                mSluit = value
            End Set
        End Property

        Public ReadOnly Property Regels() As List(Of Resultaat.RegelRecord)
            Get
                Return mRegels
            End Get
        End Property

        Public ReadOnly Property ResultaatCodeText() As String
            Get
                Select Case mResultaatCode
                    Case ResultaatcodeType.I2M_PROC
                        Return "I2M_PROC - Batch is al verwerkt."
                    Case ResultaatcodeType.I2M_DEL
                        Return "I2M_DEL - Batch is verwijderd."
                    Case ResultaatcodeType.I2M_VERW
                        Return "I2M_VERW - Batch in verwerking."
                    Case ResultaatcodeType.I2M_VALG
                        Return "I2M_VALG - Batch niet voor validatie verzonden."
                    Case ResultaatcodeType.I2M_DALG
                        Return "I2M_DALG - Batch niet verzonden."
                    Case ResultaatcodeType.I2M_SEND
                        Return "I2M_SEND - Batch is handmatig verzonden."
                    Case ResultaatcodeType.I2M_NVSP
                        Return "I2M_NVSP - Batch is niet via VSP aangeboden ter verificatie."
                    Case ResultaatcodeType.I2M_VAL
                        Return "I2M_VAL - Batch is al aangeboden ter verificatie."
                    Case ResultaatcodeType.I2M_NFOUND
                        Return "I2M_NFOUND - Batch niet gevonden."
                    Case ResultaatcodeType.I2M_NCON
                        Return "I2M_NCON - Fout bij het maken van de databaseconnectie."
                    Case ResultaatcodeType.I2M_MSG
                        Return "I2M_MSG - Messagestandard van batch wordt niet ondersteund."
                    Case ResultaatcodeType.I2M_VALID
                        Return "I2M_VALID - Vecozo ValidatieID ontbreekt."
                    Case ResultaatcodeType.I2M_DALID
                        Return "I2M_DALID - Vecozo DeclaratieID ontbreekt."
                    Case ResultaatcodeType.I2M_VALONTV
                        Return "I2M_VALONTV - Validatieretour nog niet verwerkt door Vecozo."
                    Case ResultaatcodeType.I2M_VALVERPL
                        Return "I2M_VALVERPL - Validatie voor verzenden verplicht."
                    Case ResultaatcodeType.I2M_VALAFGK
                        Return "I2M_VALAFGK - Validatie afgekeurd."
                    Case ResultaatcodeType.I2M_DECLBEST
                        Return "I2M_DECLBEST - Fout bij maken van declaratiebestand."
                    Case ResultaatcodeType.I2M_VFNFOUND
                        Return "I2M_VFNFOUND - Declaratiebestand verwacht maar niet gevonden."
                    Case ResultaatcodeType.I2M_VFERR
                        Return "I2M_VFERR - Fout bij ophalen van declaratiebestand."
                    Case ResultaatcodeType.I2M_MODVSPEDP
                        Return "I2M_OTH - Module VSPEDP niet actief."
                    Case ResultaatcodeType.I2M_VRNFOUND
                        Return "I2M_VFNFOUND - Validatieretour bestand verwacht maar niet gevonden."
                    Case ResultaatcodeType.I2M_DRNFOUND
                        Return "I2M_VFNFOUND - Declaratieretour bestand verwacht maar niet gevonden."
                    Case ResultaatcodeType.I2M_OTH
                        Return "I2M_OTH - Onbekende fout: "
                    Case ResultaatcodeType.I2M_SENDVSP
                        Return "I2M_SENDVSP - Batch is via VSP verzonden."
                    Case ResultaatcodeType.None
                        Return ""
                End Select
                Return "Onbekende fout"
            End Get
        End Property

        Public Property ResultaatCode() As ResultaatcodeType
            Get
                Return mResultaatCode
            End Get
            Set(ByVal value As ResultaatcodeType)
                mResultaatCode = value
            End Set
        End Property

        Public Property ResultaatTextExtra() As String
            Get
                Return mResultaatTextExtra
            End Get
            Set(ByVal value As String)
                mResultaatTextExtra = value
            End Set
        End Property

        Public Property Status() As StatusType
            Get
                Return mStatus
            End Get
            Set(ByVal value As StatusType)
                mStatus = value
            End Set
        End Property

        Public ReadOnly Property StatusText() As String
            Get
                Select Case mStatus
                    Case StatusType.None
                        Return ""
                    Case StatusType.Succes
                        Return "3 - Success"
                    Case StatusType.SuccesMetOpmerking
                        Return "4 - Succes met opmerking"
                    Case StatusType.Fout
                        Return "5 - Fout"
                    Case StatusType.Fout
                        Return "6 - Waarschuwing"
                    Case Else
                        Return "Onbekende status"
                End Select
            End Get
        End Property

        Public Property Filename As String
            Get
                Return mFilename
            End Get
            Set(value As String)
                mFilename = value
            End Set
        End Property

        Public Property File As String
            Get
                Return mFile
            End Get
            Set(value As String)
                mFile = value
            End Set
        End Property

        Public Property VecozoBatchID As Long
            Get
                Return mVecozoBatchID
            End Get
            Set(value As Long)
                mVecozoBatchID = value
            End Set
        End Property

    End Class

    Public Class UpdateResultaatZH309
        Enum UpdateResultaatZH309Status
            Open = 0
            Verwerkt = 1
            Processed = 2
            NotFound = 3
            Other = 4
        End Enum

        Private mFilename As String
        Private mVecozoBatchID As String
        Private mStatus As UpdateResultaatZH309Status
        Private mBatchnummer As Integer
        Private mStatusOther As String
        Private mHeaderStatus As VBStatusType

        Public Property HeaderStatus As VBStatusType
            Get
                Return mHeaderStatus
            End Get
            Set(value As VBStatusType)
                mHeaderStatus = value
            End Set
        End Property

        Public ReadOnly Property HeaderStatusText As String
            Get
                Select Case mHeaderStatus
                    Case VBStatusType.Open
                        Return "Open"
                    Case VBStatusType.Verstuurd
                        Return "Verstuurd"
                    Case VBStatusType.Toegewezen
                        Return "Toegewezen"
                    Case VBStatusType.Afgewezen
                        Return "Afgewezen"
                    Case VBStatusType.DeelsToegewezen
                        Return "DeelsToegewezen"
                    Case VBStatusType.Verwijderd
                        Return "Verwijderd"
                    Case Else
                        Return "Onbekend"
                End Select
            End Get
        End Property

        Public Property Filename As String
            Get
                Return mFilename
            End Get
            Set(value As String)
                mFile = value
            End Set
        End Property

        Public Property VecozoBatchID As String
            Get
                Return mVecozoBatchID
            End Get
            Set(value As String)
                mVecozoBatchID = value
            End Set
        End Property

        Public Property Batchnummer As Integer
            Get
                Return mBatchnummer
            End Get
            Set(value As Integer)
                mBatchnummer = value
            End Set
        End Property

        Public Property Status As UpdateResultaatZH309Status
            Get
                Return mStatus
            End Get
            Set(value As UpdateResultaatZH309Status)
                mStatus = value
            End Set
        End Property

        Public Property StatusOther As String
            Get
                Return mStatusOther
            End Get
            Set(value As String)
                mStatusOther = value
            End Set
        End Property
    End Class

    Public Function GetVektisDeclaratieRetourStatus(VecozoBatchID As Long) As Resultaat
        Dim dbCommand As New SqlCommand
        Dim dbReader As SqlDataReader

        Dim VBSendmethodID As Long
        Dim VBMessageStandardID As Long
        Dim VBStatus As Long
        Dim VBDeleted As String
        Dim VBProcessed As String

        Try
            If dbConnection.State = ConnectionState.Closed Then dbConnection.Open()
            dbCommand.Connection = dbConnection

            'Get vecozobatchdetails
            dbCommand = New SqlCommand("SELECT [Batchnummer], [VecozoBatchSendmethodID], [MessageStandardID], [Status], [Processed], [Deleted] FROM [VecozoBatch] WHERE VecozoBatchID = " & VecozoBatchID.ToString, dbConnection)
            dbReader = dbCommand.ExecuteReader()
            If dbReader.Read() Then
                VBSendmethodID = EvaluateFromSQL(dbReader.GetValue(dbReader.GetOrdinal("VecozoBatchSendmethodID")))
                VBMessageStandardID = EvaluateFromSQL(dbReader.GetValue(dbReader.GetOrdinal("MessageStandardID")))
                VBStatus = EvaluateFromSQL(dbReader.GetValue(dbReader.GetOrdinal("Status")))
                VBDeleted = EvaluateFromSQL(dbReader.GetValue(dbReader.GetOrdinal("Deleted")))
                VBProcessed = EvaluateFromSQL(dbReader.GetValue(dbReader.GetOrdinal("Processed")))

                'check if batch is processed
                If VBProcessed = "T" Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_PROC
                ElseIf Not (VBMessageStandardID = "1" Or VBMessageStandardID = "2") Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_MSG
                ElseIf VBDeleted = "T" Or VBStatus = 6 Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_DEL
                ElseIf VBSendmethodID = 1 Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_SENDVSP
                ElseIf VBStatus >= 3 And VBStatus <= 5 Then
                    aResultaat.Status = Resultaat.StatusType.Fout
                    aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_VERW
                End If
            Else
                aResultaat.Status = Resultaat.StatusType.Fout
                aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_NFOUND
            End If
            dbReader.Close()
        Catch e As Exception
            aResultaat.Status = Resultaat.StatusType.Fout
            aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_OTH
            aResultaat.ResultaatTextExtra = e.Message
        Finally
            If dbConnection.State = ConnectionState.Open Then dbConnection.Close()
        End Try
        Return aResultaat
    End Function

    Function ProcessVektisDeclaratieRetour(ByVal Filename As String, ByVal file As MemoryStream) As Resultaat
        Dim bDoNotclose As Boolean = False
        Dim dbCommandView As New SqlCommand
        Dim dbCommandStandard As New SqlCommand
        Dim sFile As String = ""
        Dim aResultaat As New Resultaat()
        Dim sKenmerk As String

        intIdentificatieDetailrecord = 0
        Try
            If dbConnection.State = ConnectionState.Closed Then dbConnection.Open() Else bDoNotclose = True

            Dim aStandaard As EIStandaard = New EIStandaard(file)
            If aStandaard.StandaardCode = EIStandaard.StandaardcodeType.ZH309 Or aStandaard.StandaardCode = EIStandaard.StandaardcodeType.DG302 Then
                'voorlooprecord
                dbCommandStandard.Connection = dbConnection

                file.Position = 0
                Dim ast As StreamReader = New StreamReader(file)
                Dim aLine As String = ast.ReadLine()
                Dim aRegel As Resultaat.RegelRecord
                While aLine IsNot Nothing
                    sKenmerk = aLine.Substring(0, 2)
                    If sKenmerk = "01" Then
                        Dim aVoorloop As Resultaat.VoorloopRecord = ProcessZH309Voorloop(aLine, aStandaard, dbCommandStandard)
                        aResultaat.Voorloop = aVoorloop
                        aResultaat.VecozoBatchID = GetVecozoBatchID(aVoorloop.FactuurnummerDeclarant)
                    ElseIf sKenmerk = "02" Then
                        Dim aVerzekerde As Resultaat.RegelRecord.VerzekerdeRecord = ProcessZH309Verzekerde(aLine, aStandaard, dbCommandStandard)
                        If aRegel IsNot Nothing Then aResultaat.AddRegel(aRegel)
                        aRegel = New Resultaat.RegelRecord
                        aRegel.Verzekerde = aVerzekerde

                        intIdentificatieDetailrecord = 0
                    ElseIf sKenmerk = "04" Then
                        If aStandaard.StandaardCode = EIStandaard.StandaardcodeType.DG302 Then
                            Dim aPrestatie As Resultaat.RegelRecord.PrestatieRecord = ProcessDG302Prestatie(aLine, aStandaard, dbCommandStandard)
                            aRegel.Prestatie = aPrestatie
                            aRegel.VecozoBatchLineID = GetVecozoBatchLineID(aResultaat.VecozoBatchID, aRegel.Prestatie.ReferentienummerDitRecord)
                        Else
                            Dim aPrestatie As Resultaat.RegelRecord.PrestatieRecord = ProcessZH309Prestatie(aLine, aStandaard, dbCommandStandard)
                            aRegel.Prestatie = aPrestatie
                            aRegel.VecozoBatchLineID = GetVecozoBatchLineID(aResultaat.VecozoBatchID, aRegel.Prestatie.ReferentienummerDitRecord)
                        End If
                    ElseIf sKenmerk = "06" Then
                        Dim aTarief As Resultaat.RegelRecord.TariefRecord = ProcessZH309Tarief(aLine, aStandaard, dbCommandStandard)
                        aRegel.AddTarief(aTarief)
                    ElseIf sKenmerk = "16" Then
                        Dim aZorgactiviteit As Resultaat.RegelRecord.ZorgactiviteitRecord = ProcessZH309Zorgactiviteit(aLine, aStandaard, dbCommandStandard)
                        aRegel.AddZorgactiviteit(aZorgactiviteit)
                    ElseIf sKenmerk = "99" Then
                        Dim aSluit As Resultaat.SluitRecord = ProcessZH309Sluit(aLine, aStandaard, dbCommandStandard)
                        aResultaat.Sluit = aSluit
                        If aRegel IsNot Nothing Then aResultaat.AddRegel(aRegel)
                    End If
                    aLine = ast.ReadLine()
                End While
            End If
            aResultaat.Status = Resultaat.StatusType.Succes
        Catch ex As Exception
            aResultaat.Status = Resultaat.StatusType.Fout
            aResultaat.ResultaatCode = Resultaat.ResultaatcodeType.I2M_OTH
            aResultaat.ResultaatTextExtra = e.Message
        Finally
            If dbConnection.State = ConnectionState.Open And Not bDoNotclose Then dbConnection.Close()
        End Try

        Return aResultaat
    End Function

    Private Function ProcessZH309Voorloop(aLine As String, aStandaard As EIStandaard, dbCommandStandard As SqlCommand) As Resultaat.VoorloopRecord
        Dim dbReaderStandard As SqlDataReader
        Dim sKenmerk As String = aLine.Substring(0, 2)
        Dim aVoorloop As New Resultaat.VoorloopRecord

        aVoorloop.Standaardcode = aStandaard.StandaardCode.ToString
        dbCommandStandard.CommandText = "Select * From [VektisStandaard] WHERE [Recordcode] = '" + sKenmerk + "' AND [Standaardcode] = '" + aStandaard.StandaardCode.ToString + "' AND [Versie] = " + aStandaard.Standaardversie.ToString + " AND [Subversie] = " + aStandaard.Standaardsubversie.ToString + "  ORDER BY [Volgnummer]"
        dbReaderStandard = dbCommandStandard.ExecuteReader()
        While dbReaderStandard.Read()
            Select Case dbReaderStandard("NaamGegevenselement")
                Case "FACTUURNUMMER DECLARANT"
                    aVoorloop.FactuurnummerDeclarant = GetFieldFromFile(dbReaderStandard, aLine)
                Case "REFERENTIENUMMER ZORGVERZEKERAAR"
                    aVoorloop.ReferentienummerZorgverzekeraar = GetFieldFromFile(dbReaderStandard, aLine)
                Case "DAGTEKENING RETOURBERICHT"
                    aVoorloop.DagtekeningRetourbericht = GetFieldFromFile(dbReaderStandard, aLine)
                Case "RETOURCODE (01)"
                    aVoorloop.Retourcode1 = GetFieldFromFile(dbReaderStandard, aLine)
                Case "RETOURCODE (02)"
                    aVoorloop.Retourcode2 = GetFieldFromFile(dbReaderStandard, aLine)
                Case "RETOURCODE (03)"
                    aVoorloop.Retourcode3 = GetFieldFromFile(dbReaderStandard, aLine)
                Case "ZORGVERLENERSCODE"
                    aVoorloop.Zorgverlenerscode = GetFieldFromFile(dbReaderStandard, aLine)
                Case "PRAKTIJKCODE"
                    aVoorloop.Praktijkcode = GetFieldFromFile(dbReaderStandard, aLine)
                Case "INSTELLINGSCODE"
                    aVoorloop.Instellingscode = GetFieldFromFile(dbReaderStandard, aLine)
            End Select
        End While
        dbReaderStandard.Close()
        Return aVoorloop
    End Function

    Private Function ProcessZH309Verzekerde(aLine As String, aStandaard As EIStandaard, dbCommandStandard As SqlCommand) As Resultaat.RegelRecord.VerzekerdeRecord
        Dim dbReaderStandard As SqlDataReader
        Dim sKenmerk As String = aLine.Substring(0, 2)
        Dim aVerzekerde As New Resultaat.RegelRecord.VerzekerdeRecord

        dbCommandStandard.CommandText = "Select * From [VektisStandaard] WHERE [Recordcode] = '" + sKenmerk + "' AND [Standaardcode] = '" + aStandaard.StandaardCode.ToString + "' AND [Versie] = " + aStandaard.Standaardversie.ToString + " AND [Subversie] = " + aStandaard.Standaardsubversie.ToString + "  ORDER BY [Volgnummer]"
        dbReaderStandard = dbCommandStandard.ExecuteReader()
        While dbReaderStandard.Read()
            Dim sVeld As String
            Dim sValue As String = dbReaderStandard("NaamGegevenselement")
            Select Case sValue
                Case "IDENTIFICATIE DETAILRECORD"
                    aVerzekerde.IdentificatieDetailrecord = GetFieldFromFile(dbReaderStandard, aLine)
                Case "RETOURCODE (01)"
                    aVerzekerde.Retourcode1 = GetFieldFromFile(dbReaderStandard, aLine)
                Case "RETOURCODE (02)"
                    aVerzekerde.Retourcode2 = GetFieldFromFile(dbReaderStandard, aLine)
                Case "RETOURCODE (03)"
                    aVerzekerde.Retourcode3 = GetFieldFromFile(dbReaderStandard, aLine)
            End Select
        End While
        dbReaderStandard.Close()
        Return aVerzekerde
    End Function

    Private Function ProcessZH309Prestatie(aLine As String, aStandaard As EIStandaard, dbCommandStandard As SqlCommand) As Resultaat.RegelRecord.PrestatieRecord
        Dim dbReaderStandard As SqlDataReader
        Dim sKenmerk As String = aLine.Substring(0, 2)
        Dim aPrestatie As New Resultaat.RegelRecord.PrestatieRecord

        dbCommandStandard.CommandText = "Select * From [VektisStandaard] WHERE [Recordcode] = '" + sKenmerk + "' AND [Standaardcode] = '" + aStandaard.StandaardCode.ToString + "' AND [Versie] = " + aStandaard.Standaardversie.ToString + " AND [Subversie] = " + aStandaard.Standaardsubversie.ToString + "  ORDER BY [Volgnummer]"
        dbReaderStandard = dbCommandStandard.ExecuteReader()
        While dbReaderStandard.Read()
            Select Case dbReaderStandard("NaamGegevenselement")
                Case "IDENTIFICATIE DETAILRECORD"
                    aPrestatie.IdentificatieDetailrecord = GetFieldFromFile(dbReaderStandard, aLine)
                Case "REFERENTIENUMMER DIT PRESTATIERECORD"
                    aPrestatie.ReferentienummerDitRecord = GetFieldFromFile(dbReaderStandard, aLine)
                Case "RETOURCODE (01)"
                    aPrestatie.Retourcode1 = GetFieldFromFile(dbReaderStandard, aLine)
                Case "RETOURCODE (02)"
                    aPrestatie.Retourcode2 = GetFieldFromFile(dbReaderStandard, aLine)
                Case "RETOURCODE (03)"
                    aPrestatie.Retourcode3 = GetFieldFromFile(dbReaderStandard, aLine)
            End Select
        End While
        dbReaderStandard.Close()
        Return aPrestatie
    End Function

    Private Function ProcessDG302Prestatie(aLine As String, aStandaard As EIStandaard, dbCommandStandard As SqlCommand) As Resultaat.RegelRecord.PrestatieRecord
        Dim dbReaderStandard As SqlDataReader
        Dim sKenmerk As String = aLine.Substring(0, 2)
        Dim aPrestatie As New Resultaat.RegelRecord.PrestatieRecord

        dbCommandStandard.CommandText = "Select * From [VektisStandaard] WHERE [Recordcode] = '" + sKenmerk + "' AND [Standaardcode] = '" + aStandaard.StandaardCode.ToString + "' AND [Versie] = " + aStandaard.Standaardversie.ToString + " AND [Subversie] = " + aStandaard.Standaardsubversie.ToString + "  ORDER BY [Volgnummer]"
        dbReaderStandard = dbCommandStandard.ExecuteReader()
        While dbReaderStandard.Read()
            Select Case dbReaderStandard("NaamGegevenselement")
                Case "IDENTIFICATIE DETAILRECORD"
                    aPrestatie.IdentificatieDetailrecord = GetFieldFromFile(dbReaderStandard, aLine)
                Case "REFERENTIENUMMER DIT PRESTATIERECORD"
                    aPrestatie.ReferentienummerDitRecord = GetFieldFromFile(dbReaderStandard, aLine)
                Case "DECLARATIEBEDRAG (INCL. BTW)"
                    aPrestatie.DeclaratieBedragInclBTW = GetFieldFromFile(dbReaderStandard, aLine)
                Case "INDICATIE DEBET/CREDIT (02)"
                    aPrestatie.IndicatieDebetCredit2 = GetFieldFromFile(dbReaderStandard, aLine)
                Case "BEREKEND BEDRAG ZORGVERZEKERAAR"
                    aPrestatie.BerekendBedragZorgverzekeraar = GetFieldFromFile(dbReaderStandard, aLine)
                Case "INDICATIE DEBET/CREDIT (03)"
                    aPrestatie.IndicatieDebetCredit3 = GetFieldFromFile(dbReaderStandard, aLine)
                Case "TOEGEKEND BEDRAG"
                    aPrestatie.ToegekendBedrag = GetFieldFromFile(dbReaderStandard, aLine)
                Case "INDICATIE DEBET/CREDIT (04)"
                    aPrestatie.IndicatieDebetCredit4 = GetFieldFromFile(dbReaderStandard, aLine)
                Case "RETOURCODE (01)"
                    aPrestatie.Retourcode1 = GetFieldFromFile(dbReaderStandard, aLine)
                Case "RETOURCODE (02)"
                    aPrestatie.Retourcode2 = GetFieldFromFile(dbReaderStandard, aLine)
                Case "RETOURCODE (03)"
                    aPrestatie.Retourcode3 = GetFieldFromFile(dbReaderStandard, aLine)
            End Select
        End While
        dbReaderStandard.Close()
        Return aPrestatie
    End Function

    Private Function ProcessZH309Tarief(aLine As String, aStandaard As EIStandaard, dbCommandStandard As SqlCommand) As Resultaat.RegelRecord.TariefRecord
        Dim dbReaderStandard As SqlDataReader
        Dim sKenmerk As String = aLine.Substring(0, 2)
        Dim aTarief As New Resultaat.RegelRecord.TariefRecord

        dbCommandStandard.CommandText = "Select * From [VektisStandaard] WHERE [Recordcode] = '" + sKenmerk + "' AND [Standaardcode] = '" + aStandaard.StandaardCode.ToString + "' AND [Versie] = " + aStandaard.Standaardversie.ToString + " AND [Subversie] = " + aStandaard.Standaardsubversie.ToString + "  ORDER BY [Volgnummer]"
        dbReaderStandard = dbCommandStandard.ExecuteReader()
        While dbReaderStandard.Read()
            Select Case dbReaderStandard("NaamGegevenselement")

                Case "IDENTIFICATIE DETAILRECORD"
                    aTarief.IdentificatieDetailrecord = GetFieldFromFile(dbReaderStandard, aLine)
                Case "REFERENTIENUMMER DIT TARIEFRECORD"
                    aTarief.ReferentienummerDitRecord = GetFieldFromFile(dbReaderStandard, aLine)
                Case "DECLARATIEBEDRAG (INCL. BTW)"
                    aTarief.DeclaratieBedragInclBTW = GetFieldFromFile(dbReaderStandard, aLine)
                Case "INDICATIE DEBET/CREDIT (02)"
                    aTarief.IndicatieDebetCredit2 = GetFieldFromFile(dbReaderStandard, aLine)
                Case "BEREKEND BEDRAG ZORGVERZEKERAAR"
                    aTarief.BerekendBedragZorgverzekeraar = GetFieldFromFile(dbReaderStandard, aLine)
                Case "INDICATIE DEBET/CREDIT (03)"
                    aTarief.IndicatieDebetCredit3 = GetFieldFromFile(dbReaderStandard, aLine)
                Case "TOEGEKEND BEDRAG"
                    aTarief.ToegekendBedrag = GetFieldFromFile(dbReaderStandard, aLine)
                Case "INDICATIE DEBET/CREDIT (04)"
                    aTarief.IndicatieDebetCredit4 = GetFieldFromFile(dbReaderStandard, aLine)
                Case "RETOURCODE (01)"
                    aTarief.Retourcode1 = GetFieldFromFile(dbReaderStandard, aLine)
                Case "RETOURCODE (02)"
                    aTarief.Retourcode2 = GetFieldFromFile(dbReaderStandard, aLine)
                Case "RETOURCODE (03)"
                    aTarief.Retourcode3 = GetFieldFromFile(dbReaderStandard, aLine)
            End Select
        End While
        dbReaderStandard.Close()
        Return aTarief
    End Function

    Private Function ProcessZH309Zorgactiviteit(aLine As String, aStandaard As EIStandaard, dbCommandStandard As SqlCommand) As Resultaat.RegelRecord.ZorgactiviteitRecord
        Dim dbReaderStandard As SqlDataReader
        Dim sKenmerk As String = aLine.Substring(0, 2)
        Dim aZorgactiviteit As New Resultaat.RegelRecord.ZorgactiviteitRecord

        dbCommandStandard.CommandText = "Select * From [VektisStandaard] WHERE [Recordcode] = '" + sKenmerk + "' AND [Standaardcode] = '" + aStandaard.StandaardCode.ToString + "' AND [Versie] = " + aStandaard.Standaardversie.ToString + " AND [Subversie] = " + aStandaard.Standaardsubversie.ToString + "  ORDER BY [Volgnummer]"
        dbReaderStandard = dbCommandStandard.ExecuteReader()
        While dbReaderStandard.Read()
            Select Case dbReaderStandard("NaamGegevenselement")
                Case "IDENTIFICATIE DETAILRECORD"
                    aZorgactiviteit.IdentificatieDetailrecord = GetFieldFromFile(dbReaderStandard, aLine)
                Case "REFERENTIENUMMER DIT ZORGACTIVITEITRECORD"
                    aZorgactiviteit.ReferentienummerDitRecord = GetFieldFromFile(dbReaderStandard, aLine)
                Case "RETOURCODE (01)"
                    aZorgactiviteit.Retourcode1 = GetFieldFromFile(dbReaderStandard, aLine)
                Case "RETOURCODE (02)"
                    aZorgactiviteit.Retourcode2 = GetFieldFromFile(dbReaderStandard, aLine)
                Case "RETOURCODE (03)"
                    aZorgactiviteit.Retourcode3 = GetFieldFromFile(dbReaderStandard, aLine)
            End Select
        End While
        dbReaderStandard.Close()
        Return aZorgactiviteit
    End Function

    Private Function ProcessZH309Sluit(aLine As String, aStandaard As EIStandaard, dbCommandStandard As SqlCommand) As Resultaat.SluitRecord
        Dim dbReaderStandard As SqlDataReader
        Dim sKenmerk As String = aLine.Substring(0, 2)
        Dim aSluit As New Resultaat.SluitRecord

        dbCommandStandard.CommandText = "Select * From [VektisStandaard] WHERE [Recordcode] = '" + sKenmerk + "' AND [Standaardcode] = '" + aStandaard.StandaardCode.ToString + "' AND [Versie] = " + aStandaard.Standaardversie.ToString + " AND [Subversie] = " + aStandaard.Standaardsubversie.ToString + "  ORDER BY [Volgnummer]"
        dbReaderStandard = dbCommandStandard.ExecuteReader()
        While dbReaderStandard.Read()
            Dim sVeld As String
            Select Case dbReaderStandard("NaamGegevenselement")
                Case "TOTAAL INGEDIEND DECLARATIEBEDRAG"
                    aSluit.TotaalIngediendBedrag = GetFieldFromFile(dbReaderStandard, aLine)
                Case "INDICATIE DEBET/CREDIT (01)"
                    aSluit.IndicatieDebetCredit01 = GetFieldFromFile(dbReaderStandard, aLine)
                Case "TOTAAL TOEGEKEND BEDRAG"
                    aSluit.TotaalToegekendBedrag = GetFieldFromFile(dbReaderStandard, aLine)
                Case "INDICATIE DEBET/CREDIT (02)"
                    aSluit.IndicatieDebetCredit02 = GetFieldFromFile(dbReaderStandard, aLine)
            End Select
        End While
        dbReaderStandard.Close()
        Return aSluit
    End Function

    Private Function GetHeaderStatus(ByVal aResult As VektisDeclaratieRetour.Resultaat, aVecozoBatchLines As List(Of VecozoBatchLineTable)) As VBStatusType
        Dim tmpTotaalIngediendBedrag As Double
        Dim tmpTotaalToegekendBedrag As Double

        If aResult.Sluit.IndicatieDebetCredit01 = "C" Then
            tmpTotaalToegekendBedrag = 0
        End If


        If aResult.Sluit IsNot Nothing Then
            tmpTotaalIngediendBedrag = IIf(aResult.Sluit.IndicatieDebetCredit01 = "D", 1, -1) * aResult.Sluit.TotaalIngediendBedrag / 100
            tmpTotaalToegekendBedrag = IIf(aResult.Sluit.IndicatieDebetCredit02 = "D", 1, -1) * aResult.Sluit.TotaalToegekendBedrag / 100
        End If

        If aResult.Regels IsNot Nothing Then
            Dim bOK = True
            Dim bAfgewezen = False
            For Each aRegel As VektisDeclaratieRetour.Resultaat.RegelRecord In aResult.Regels
                Dim tmpIngediendBedrag As Double = 0
                Dim tmpToegekendBedrag As Double = 0
                Dim InvoiceID As Integer = -1
                If aRegel.Prestatie IsNot Nothing Then
                    InvoiceID = aRegel.Prestatie.ReferentienummerDitRecord
                    If aResult.Voorloop.Standaardcode = "DG302" Then
                        tmpIngediendBedrag += IIf(aRegel.Prestatie.IndicatieDebetCredit2 = "D", 1, -1) * aRegel.Prestatie.DeclaratieBedragInclBTW / 100
                        tmpToegekendBedrag += IIf(aRegel.Prestatie.IndicatieDebetCredit4 = "D", 1, -1) * aRegel.Prestatie.ToegekendBedrag / 100
                    End If
                End If
                If aRegel.Tarieven IsNot Nothing Then
                    For Each aTarief As Resultaat.RegelRecord.TariefRecord In aRegel.Tarieven
                        tmpIngediendBedrag += IIf(aTarief.IndicatieDebetCredit2 = "D", 1, -1) * aTarief.DeclaratieBedragInclBTW / 100
                        tmpToegekendBedrag += IIf(aTarief.IndicatieDebetCredit4 = "D", 1, -1) * aTarief.ToegekendBedrag / 100
                    Next
                End If
                Dim aVecozobatchline As VecozoBatchLineTable = aVecozoBatchLines.Find(Function(p As VecozoBatchLineTable) p.InvoiceID = InvoiceID)
                If Not aVecozobatchline Is Nothing Then
                    If tmpIngediendBedrag = 0.0 And tmpToegekendBedrag = 0.0 Then
                        aVecozobatchline.Status = VBLStatusType.Toegewezen
                        aVecozobatchline.Assigned = aVecozobatchline.Requested
                    ElseIf tmpIngediendBedrag = tmpToegekendBedrag Then
                        aVecozobatchline.Status = VBLStatusType.Toegewezen
                        aVecozobatchline.Assigned = aVecozobatchline.Requested
                    ElseIf tmpIngediendBedrag <> 0.0 And tmpToegekendBedrag = 0.0 Then
                        aVecozobatchline.Status = VBLStatusType.Afgewezen
                        aVecozobatchline.Assigned = 0
                    ElseIf tmpIngediendBedrag <> tmpToegekendBedrag Then
                        aVecozobatchline.Status = VBLStatusType.DeelsToegewezen
                        aVecozobatchline.Assigned = tmpToegekendBedrag
                    End If
                End If
            Next
        End If
        Dim xStatus As VBLStatusType
        If tmpTotaalIngediendBedrag <> 0 And tmpTotaalToegekendBedrag = 0.0 Then
            xStatus = VBLStatusType.Afgewezen
        Else
            xStatus = VBLStatusType.Toegewezen
        End If

        For Each aVecozobatchline As VecozoBatchLineTable In aVecozoBatchLines
            If aVecozobatchline.Status = VBLStatusType.Onbekend Then
                aVecozobatchline.Status = xStatus
                If xStatus = VBLStatusType.Toegewezen Then aVecozobatchline.Assigned = aVecozobatchline.Requested
            End If
        Next

        If aVecozoBatchLines.Count = 0 Then
            Return VBStatusType.Verwijderd
        End If

        Dim aHeaderStatus As VBStatusType
        Dim i As Integer = 0
        For Each aVecozobatchline As VecozoBatchLineTable In aVecozoBatchLines
            Select Case aVecozobatchline.Status
                Case VBLStatusType.Toegewezen
                    If i = 0 Then
                        aHeaderStatus = VBStatusType.Toegewezen
                    Else
                        If aHeaderStatus = VBStatusType.Afgewezen Then
                            aHeaderStatus = VBStatusType.DeelsToegewezen
                            Exit For
                        End If
                    End If
                Case VBLStatusType.DeelsToegewezen
                    aHeaderStatus = VBStatusType.DeelsToegewezen
                    Exit For
                Case VBLStatusType.Afgewezen
                    If i = 0 Then
                        aHeaderStatus = VBStatusType.Afgewezen
                    Else
                        If aHeaderStatus <> VBStatusType.Afgewezen Then
                            aHeaderStatus = VBStatusType.DeelsToegewezen
                            Exit For
                        End If
                    End If
            End Select
            i += 1
        Next
        Return aHeaderStatus
    End Function

    Public Shadows Function UpdateZH309(ByVal Simulatie As Boolean, ByVal aResult As VektisDeclaratieRetour.Resultaat) As UpdateResultaatZH309
        Dim bDoNotclose As Boolean = False
        Dim dbCommand As New SqlCommand
        Dim dbReader As SqlDataReader
        Dim dbCommandVBL As New SqlCommand
        Dim dbReaderVBL As SqlDataReader
        Dim VecozobatchLines As New List(Of VecozoBatchLineTable)
        Dim rows As Integer

        Dim aUpdateResultaatZH309 As New UpdateResultaatZH309()
        aUpdateResultaatZH309.Filename = aResult.Filename
        aUpdateResultaatZH309.Batchnummer = aResult.Voorloop.FactuurnummerDeclarant
        aUpdateResultaatZH309.VecozoBatchID = -1
        Try
            If dbConnection.State = ConnectionState.Closed Then dbConnection.Open() Else bDoNotclose = True
            dbCommand.Connection = dbConnection

            dbCommand = New SqlCommand("SELECT [VecozoBatchID], [Processed] FROM [VecozoBatch] WHERE Batchnummer =" & aUpdateResultaatZH309.Batchnummer, dbConnection)
            dbReader = dbCommand.ExecuteReader()
            If dbReader.Read() Then
                aUpdateResultaatZH309.VecozoBatchID = dbReader.GetValue(0)
                If dbReader.GetValue(1) = "T" Then
                    If dbConnection.State = ConnectionState.Open And Not bDoNotclose Then dbConnection.Close()
                    aUpdateResultaatZH309.Status = UpdateResultaatZH309.UpdateResultaatZH309Status.Processed
                    dbReader.Close()
                    Return aUpdateResultaatZH309
                End If
            End If
            dbReader.Close()
            If aUpdateResultaatZH309.VecozoBatchID = -1 Then
                If dbConnection.State = ConnectionState.Open And Not bDoNotclose Then dbConnection.Close()
                aUpdateResultaatZH309.Status = UpdateResultaatZH309.UpdateResultaatZH309Status.NotFound
                Return aUpdateResultaatZH309
            End If

            dbCommandVBL.Connection = dbConnection
            dbCommandVBL.CommandText = "SELECT [VecozoBatchLinenumber], [InvoiceID], [Requested] FROM [VecozoBatchLine] WHERE [VecozoBatchID] = " & aUpdateResultaatZH309.VecozoBatchID.ToString & " AND Deleted = 'F'"
            dbReaderVBL = dbCommandVBL.ExecuteReader()
            While dbReaderVBL.Read()
                Dim aVecozobatchLine As New VecozoBatchLineTable
                aVecozobatchLine.VecozoBatchLinenumber = dbReaderVBL.GetValue(0)
                aVecozobatchLine.InvoiceID = dbReaderVBL.GetValue(1)
                aVecozobatchLine.Requested = dbReaderVBL.GetValue(2)
                aVecozobatchLine.Status = VBLStatusType.Onbekend
                VecozobatchLines.Add(aVecozobatchLine)
            End While
            dbReaderVBL.Close()

            aUpdateResultaatZH309.HeaderStatus = GetHeaderStatus(aResult, VecozobatchLines)

            If Not Simulatie Then
                dbTransaction = dbConnection.BeginTransaction("Vecozo")
                Dim sqlupdate As System.Data.SqlClient.SqlCommand
                For Each aVecozobatchline As VecozoBatchLineTable In VecozobatchLines
                    sqlupdate = New System.Data.SqlClient.SqlCommand("sp_update_VecozoBatchLineStatus", dbConnection, dbTransaction)
                    sqlupdate.CommandType = CommandType.StoredProcedure
                    sqlupdate.Parameters.Add("@VecozoBatchID", SqlDbType.BigInt).Value = aUpdateResultaatZH309.VecozoBatchID
                    sqlupdate.Parameters.Add("@VecozoBatchLinenumber", SqlDbType.Int).Value = aVecozobatchline.VecozoBatchLinenumber
                    sqlupdate.Parameters.Add(New SqlParameter("@Assigned", SqlDbType.Decimal) With {.Precision = 9, .Scale = 2}).Value = aVecozobatchline.Assigned
                    sqlupdate.Parameters.Add("@StatusID", SqlDbType.Int).Value = aVecozobatchline.Status
                    sqlupdate.Parameters.Add("@Remark", SqlDbType.NVarChar, -1).Value = DBNull.Value
                    sqlupdate.Parameters.Add("@UpdateUserID", SqlDbType.NVarChar, 8).Value = "YOKARIBA"
                    rows = sqlupdate.ExecuteNonQuery()
                Next
                sqlupdate = New System.Data.SqlClient.SqlCommand("sp_update_VecozoBatchStatus", dbConnection, dbTransaction)
                sqlupdate.CommandType = CommandType.StoredProcedure
                sqlupdate.Parameters.Add("@VecozoBatchID", SqlDbType.BigInt).Value = aUpdateResultaatZH309.VecozoBatchID
                sqlupdate.Parameters.Add("@Status", SqlDbType.Int).Value = aUpdateResultaatZH309.HeaderStatus
                sqlupdate.Parameters.Add("@Returndate", SqlDbType.Date).Value = aResult.Voorloop.DagtekeningRetourbericht
                sqlupdate.Parameters.Add("@Remarks", SqlDbType.NVarChar, -1).Value = DBNull.Value
                sqlupdate.Parameters.Add("@Processed", SqlDbType.Char, 1).Value = "T"
                sqlupdate.Parameters.Add("@ReturnFileURL", SqlDbType.NVarChar, -1).Value = DBNull.Value
                sqlupdate.Parameters.Add("@UpdateUserID", SqlDbType.NVarChar, 8).Value = "YOKARIBA"
                rows = sqlupdate.ExecuteNonQuery()
                dbTransaction.Commit()

                If dbConnection.State = ConnectionState.Open And Not bDoNotclose Then dbConnection.Close()
            End If
            aUpdateResultaatZH309.Status = UpdateResultaatZH309.UpdateResultaatZH309Status.Verwerkt
            Return aUpdateResultaatZH309
        Catch e As Exception
            If Not Simulatie Then
                If dbConnection.State = ConnectionState.Open Then
                    dbTransaction.Rollback()
                End If
                If dbConnection.State = ConnectionState.Open And Not bDoNotclose Then dbConnection.Close()
            End If
            aUpdateResultaatZH309.Status = UpdateResultaatZH309.UpdateResultaatZH309Status.Other
            aUpdateResultaatZH309.StatusOther = e.Message
            Return aUpdateResultaatZH309
        End Try
    End Function
End Class
