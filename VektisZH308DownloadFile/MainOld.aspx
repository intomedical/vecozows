﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MainOld.aspx.vb" Inherits="VektisZH308DownloadFile.MainOld" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 217px;
        }
        .auto-style1 {
            width: 217px;
            height: 30px;
        }
        .auto-style2 {
            height: 30px;
        }
        .auto-style3 {
            width: 217px;
            height: 26px;
        }
        .auto-style4 {
            height: 26px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="style1">
            <tr>
                <td class="style2">
                    <a href="OHWGrouper.aspx">OHWGrouper</a></td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    Vecozo Batch ID</td>
                <td>
                    <asp:TextBox ID="txtVecozoBatchID" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;</td>
                <td>
                    </td> 
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;</td>
                <td>
                    <asp:Button ID="btnDownload" runat="server" Text="Download ZH308" />
                    &nbsp;&nbsp;
                    &nbsp;<br />
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    </td>
                <td class="auto-style2">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;</td>
                <td>
                    <asp:FileUpload ID="FileUpload1" runat="server" style="margin-bottom: 4px" />
                &nbsp;<asp:Button ID="btnProcessZH309" runat="server" Text="Process ZH309" />
                &nbsp;<asp:TextBox ID="txtZH309File" runat="server" Width="75px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;</td>
                <td>
                    <asp:TextBox ID="txtResultaatProcessZH309" runat="server" Height="229px" Width="447px" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;</td>
                <td>
                    <asp:Button ID="btnDownload0" runat="server" Text="Download DG301" />
                </td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    Webservice:</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">
                    Thumbprint certificaat PRD</td>
                <td class="auto-style4">
                    <asp:TextBox ID="txtThumbprintCertificatePRD" runat="server" Width="447px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    Thumbprint certificaat ACC</td>
                <td>
                    <asp:TextBox ID="txtThumbprintCertificateACC" runat="server" Width="447px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    Thumbprint certificaat TST</td>
                <td>
                    <asp:TextBox ID="txtThumbprintCertificateTST" runat="server" Width="447px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    Environment</td>
                <td>
                    <asp:RadioButtonList ID="rblstEnvironment" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="P">Production</asp:ListItem>
                        <asp:ListItem Selected="True" Value="A">Acceptatie</asp:ListItem>
                        <asp:ListItem Value="T">Test</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    Vecozo Batch ID</td>
                <td>
                    <asp:TextBox ID="txtVecozoBatchIDforWS" runat="server"></asp:TextBox>
                &nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;</td>
                <td>
                    <asp:Button ID="btnWSValideerZH308" runat="server" Text="WS Indienen Validatie" />
                    <asp:Button ID="btnWSIndienenDeclaratie" runat="server" Text="WS Indienen Declaratie" />
                </td>
            </tr>
            <tr>
                <td class="style2">
                    Resultaat</td>
                <td>
                    <asp:TextBox ID="txtResultaatIndienenValidatie" runat="server" Height="121px" Width="447px" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    Validatie ID</td>
                <td>
                    <asp:TextBox ID="txtValidatieIDforWS" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;</td>
                <td>
                    <asp:Button ID="btnWSValideerZH309" runat="server" Text="WS Download Validatie" />
                    <asp:Button ID="btnWSDownloadDeclaratie" runat="server" Text="WS Download Declaratie" />
                    <asp:Button ID="btnWSDeclaratieStatus" runat="server" Text="WS DeclaratieStatus" />
                </td>
            </tr>
            <tr>
                <td class="style2">
                    Resultaat</td>
                <td>
                    <asp:TextBox ID="txtResultaatIDowloadenValidatie" runat="server" Height="114px" Width="447px" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
