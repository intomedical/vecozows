﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="OHWGrouperLines.aspx.vb" Inherits="VektisZH308DownloadFile.OHWGrouperLines" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">

        .auto-style1 {
            width: 199px;
            font-family:Calibri;
            font-size:small;
        }


        </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
                        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:IntomediFin %>" SelectCommand="SELECT * FROM av_OHWBatchAll ORDER BY LocationID, Koepel, UZOVICode"></asp:SqlDataSource>
        </div>
        <a href="OHWGrouper.aspx">OHWGrouper</a><br />
        <br />
        <br />
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" DataSourceID="SqlDataSource2" AutoGenerateColumns="False" KeyFieldName="OHWBatchID">
            <SettingsPager Visible="False" Mode="ShowAllRecords">
            </SettingsPager>
            <Settings ShowFilterRow="True" HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto" VerticalScrollableHeight="400" />
            <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
            <Columns>
                <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowSelectCheckbox="True" VisibleIndex="0">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn FieldName="OHWBatchID" ReadOnly="True" VisibleIndex="1">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="OHWGrouperID" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataDateColumn FieldName="Batchdate" VisibleIndex="3">
                </dx:GridViewDataDateColumn>
                <dx:GridViewDataTextColumn FieldName="LocationID" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="LocationName" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="AGBCode" VisibleIndex="6">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Period" VisibleIndex="7">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Bookyear" VisibleIndex="8">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Status" VisibleIndex="9">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="UZOVICode" VisibleIndex="10">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="OrgName" VisibleIndex="11">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Koepel" VisibleIndex="12">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Aantal" VisibleIndex="13">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Totaal" VisibleIndex="14">
                </dx:GridViewDataTextColumn>
            </Columns>
        </dx:ASPxGridView>
        <p>
                                <asp:Button ID="cmdDownloadZH310" runat="server" CssClass="auto-style1" Text="Download ZH310" />
        </p>
    </form>
</body>
</html>
