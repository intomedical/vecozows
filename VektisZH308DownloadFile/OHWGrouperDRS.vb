﻿Option Strict Off

Imports System
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports Microsoft.VisualBasic.FileIO


Public Class OHWGrouperDRS
    Inherits SuperClass
    Dim dbTransaction As SqlTransaction
    Dim mUser As String

    Public Class OHWBatch

        Public Class Batch

            Public Class BatchLine
                Private mBatchLines As List(Of BatchLine)
                Private mPatientTreatmentID As Long
                Private mRequested As Double

                Public Property PatientTreatmentID() As Long
                    Get
                        Return mPatientTreatmentID
                    End Get
                    Set(ByVal value As Long)
                        mPatientTreatmentID = value
                    End Set
                End Property

                Public Property Requested() As Double
                    Get
                        Return mRequested
                    End Get
                    Set(ByVal value As Double)
                        mRequested = value
                    End Set
                End Property
            End Class

            Public Function AddBatchLine(aBatchLine As BatchLine)
                If mBatchLines Is Nothing Then mBatchLines = New List(Of BatchLine)
                mBatchLines.Add(aBatchLine)
            End Function

            Public ReadOnly Property BatchLines() As List(Of BatchLine)
                Get
                    Return mBatchLines
                End Get
            End Property

            Private mBatches As List(Of Batch)
            Private mClientID As Integer
            Private mAGBCode As String
            Private mBookyear As Integer
            Private mPeriod As Integer
            Private mUZOVICode As String

            Public Property ClientID() As Integer
                Get
                    Return mClientID
                End Get
                Set(ByVal value As Integer)
                    mClientID = value
                End Set
            End Property

            Public Property AGBCode() As String
                Get
                    Return mAGBCode
                End Get
                Set(ByVal value As String)
                    mAGBCode = value
                End Set
            End Property

            Public Property Bookyear() As Integer
                Get
                    Return mBookyear
                End Get
                Set(ByVal value As Integer)
                    mBookyear = value
                End Set
            End Property

            Public Property Period() As Integer
                Get
                    Return mPeriod
                End Get
                Set(ByVal value As Integer)
                    mPeriod = value
                End Set
            End Property

            Public Property UZOVICode() As String
                Get
                    Return mUZOVICode
                End Get
                Set(ByVal value As String)
                    mUZOVICode = value
                End Set
            End Property
        End Class

        Public Function AddBatch(aBatch As Batch)
            If mBatches Is Nothing Then mBatches = New List(Of Batch)
            mBatches.Add(aBatch)
        End Function

        Public ReadOnly Property Batches() As List(Of Batch)
            Get
                Return mBatches
            End Get
        End Property
    End Class

    Public Class OHWGrouperDRSResult
        Public Enum StatusType
            isOK = 0
            isError = 1
            isWarning = 2
            isQuestion = 3
            isInformation = 4
        End Enum

        Public Status As StatusType
        Public Filename As String
        Public Environment As String
        Public AGBcode As Long
        Public Instellingsnummer As Long
        Public CreateDate As Date
        Public Sequence As Long
        Public ProcessDate As Date
        Public Runnumber As Long
        Public ErrorText As String
        Public Lines As List(Of LineType)

        Public Function DebugText() As String
            Dim aMessage As String = ""

            aMessage = aMessage & "Status = " & Status.ToString & vbCrLf
            aMessage = aMessage & "Filename = " & Filename & vbCrLf
            aMessage = aMessage & "Environment = " & Environment.ToString & vbCrLf
            aMessage = aMessage & "AGBcode = " & AGBcode.ToString & vbCrLf
            aMessage = aMessage & "Instellingsnummer = " & Instellingsnummer.ToString & vbCrLf
            aMessage = aMessage & "CreateDate = " & CreateDate.ToShortDateString & vbCrLf
            aMessage = aMessage & "Sequence = " & Sequence.ToString & vbCrLf
            aMessage = aMessage & "ProcessDate = " & ProcessDate.ToShortDateString & vbCrLf
            aMessage = aMessage & "Runnumber = " & Runnumber.ToString & vbCrLf
            aMessage = aMessage & "ErrorText = " & ErrorText & vbCrLf
            Dim i As Integer = 1
            For Each item As OHWGrouperDRSResult.LineType In Lines
                aMessage = aMessage & "Line" & i.ToString & "\" & Lines.Count.ToString & vbCrLf
                aMessage = aMessage & "Runnummer = " & item.Runnummer.ToString & vbCrLf
                aMessage = aMessage & "VeldType  = " & item.VeldType.ToString & vbCrLf
                aMessage = aMessage & "Entiteit = " & item.Entiteit & vbCrLf
                aMessage = aMessage & "Declaratieresultsetnummer = " & item.Declaratieresultsetnummer & vbCrLf
                aMessage = aMessage & "ID = " & item.ID & vbCrLf
                aMessage = aMessage & "KolomA = " & item.KolomA & vbCrLf
                aMessage = aMessage & "KolomB = " & item.KolomB & vbCrLf
                aMessage = aMessage & "KolomC = " & item.KolomC & vbCrLf
                aMessage = aMessage & "KolomD = " & item.KolomD & vbCrLf
                aMessage = aMessage & "KolomE = " & item.KolomE & vbCrLf
                aMessage = aMessage & "KolomF = " & item.KolomF & vbCrLf
                aMessage = aMessage & "KolomG = " & item.KolomG & vbCrLf
                aMessage = aMessage & "KolomH = " & item.KolomH & vbCrLf
                aMessage = aMessage & "KolomI = " & item.KolomI & vbCrLf
                aMessage = aMessage & "KolomJ = " & item.KolomJ & vbCrLf
                aMessage = aMessage & "KolomK = " & item.KolomK & vbCrLf
                i += 1
                If i > 100 Then Exit For
            Next
            Return aMessage
        End Function

        Public Class LineType
            Public Runnummer As Long
            Public VeldType As Integer
            Public Entiteit As String
            Public Declaratieresultsetnummer As String
            Public ID As String
            Public KolomA As String
            Public KolomB As String
            Public KolomC As String
            Public KolomD As String
            Public KolomE As String
            Public KolomF As String
            Public KolomG As String
            Public KolomH As String
            Public KolomI As String
            Public KolomJ As String
            Public KolomK As String
        End Class
    End Class

    Public Sub New(ByVal User As String)
        mUser = User
    End Sub

    Public Function UploadOHWGrouperDRSfile(aFilename As String, aZipFile As Byte(), Optional Validate As Boolean = True) As OHWGrouperDRSResult
        Dim aOHWGrouperDRSResult As New OHWGrouperDRSResult

        Dim glob As New Chilkat.Global
        Dim success As Boolean = glob.UnlockBundle("AYKRBS.CB1032020_sGdL35KunR1I")
        If (success <> True) Then
            aOHWGrouperDRSResult.ErrorText = "Fout in module Chillkat: " + glob.LastErrorText
            aOHWGrouperDRSResult.Status = OHWGrouperDRSResult.StatusType.isError
            Return aOHWGrouperDRSResult
        End If

        Dim zip As New Chilkat.Zip
        success = zip.OpenFromMemory(aZipFile)
        If success Then
            If zip.LastMethodSuccess Then
                Dim zipentry As Chilkat.ZipEntry = zip.GetEntryByIndex(i)
                Dim fileData As Byte()
                If zipentry.IsDirectory = False Then
                    fileData = zipentry.Inflate()
                End If
                zip.CloseZip()
                Dim memStream As MemoryStream = New MemoryStream(fileData, 0, fileData.Length)


                If memStream IsNot Nothing Then
                    Dim respons As OHWGrouperDRS.OHWGrouperDRSResult = ProcessOHWGrouperDRSfile(aFilename, memStream)

                    If respons IsNot Nothing AndAlso respons.Status = OHWGrouperDRSResult.StatusType.isOK Then
                        Dim DISBatchID As Long
                        Dim OHWRunnnummer As Long
                        Dim OHWDRS As Boolean
                        Dim OHWBatch As Boolean

                        Message = FindDISBatchFromFilename(respons.Filename, DISBatchID, OHWRunnnummer, OHWDRS, OHWBatch)
                        If Message = "" Then
                            If DISBatchID = -1 Then 'NotFOUND
                                aOHWGrouperDRSResult.ErrorText = "Er is geen bijbehorend OHW Grouper gevonden voor opgegeven bestand '" + aFilename + "'"
                                aOHWGrouperDRSResult.Status = OHWGrouperDRSResult.StatusType.isError
                            ElseIf OHWRunnnummer - 1 Then
                                aOHWGrouperDRSResult.ErrorText = "Er is geen bijbehorend OHW Grouper retourbestand gevonden voor opgegeven bestand '" + aFilename + "'"
                                aOHWGrouperDRSResult.Status = OHWGrouperDRSResult.StatusType.isError
                            ElseIf OHWBatch And Not Validate Then
                                aOHWGrouperDRSResult.ErrorText = "OHW Grouper voor bestand '" + respons.Filename + "' is eerder verwerkt (OHW Grouper ID: " + DISBatchID.ToString + ") en er zijn OHW informatiebestanden aangemaakt. Hudige regels zullen worden overschreven. Wilt u doorgaan?."
                                aOHWGrouperDRSResult.Status = OHWGrouperDRSResult.StatusType.isQuestion
                            ElseIf OHWDRS And Not Validate Then
                                aOHWGrouperDRSResult.ErrorText = "OHW Grouper voor bestand '" + respons.Filename + "' is eerder verwerkt (OHW Grouper ID: " + DISBatchID.ToString + ") echter zijn er geen OHW informatiebestanden aangemaakt. Hudige regels zullen worden overschreven. Wilt u doorgaan?."
                                aOHWGrouperDRSResult.Status = OHWGrouperDRSResult.StatusType.isQuestion
                            Else
                                Dim aResult As OHWGrouperDRSResult = InsertOHWGrouperDRS(DISBatchID, respons)
                                If aResult.Status = OHWGrouperDRSResult.StatusType.isOK Then
                                    aResult = InsertOHWBatch(DISBatchID)
                                    If aResult.Status = OHWGrouperDRSResult.StatusType.isOK Then
                                        aOHWGrouperDRSResult.ErrorText = "OHW Grouper voor bestand '" + respons.Filename + "' is  verwerkt (OHW Grouper ID: " + DISBatchID.ToString + "."
                                        aOHWGrouperDRSResult.Status = OHWGrouperDRSResult.StatusType.isInformation
                                    Else
                                        aOHWGrouperDRSResult.ErrorText = "Onverwachte fout bij het verwerken van bestand '" + respons.Filename + "': " + aResult.ErrorText
                                        aOHWGrouperDRSResult.Status = aResult.Status
                                    End If
                                Else
                                    aOHWGrouperDRSResult.ErrorText = "Onverwachte fout bij het verwerken van bestand '" + respons.Filename + "': " + aResult.ErrorText
                                    aOHWGrouperDRSResult.Status = aResult.Status
                                End If
                            End If
                        Else
                            aOHWGrouperDRSResult.ErrorText = "Onverwachte fout bij het zoeken naar het de bijbehorende OHW grouper voor het bestand '" + aFilename + "': " + Message
                            aOHWGrouperDRSResult.Status = False
                        End If
                        memStream.Close()
                    Else
                        If respons IsNot Nothing Then
                            aOHWGrouperDRSResult.ErrorText = "Fout bij lezen van bestand '" + aFilename + "': " + respons.ErrorText
                            aOHWGrouperDRSResult.Status = False
                        Else
                            aOHWGrouperDRSResult.ErrorText = "Fout bij lezen van bestand '" + aFilename + "' is een onverwachte fout opgetreden."
                            aOHWGrouperDRSResult.Status = False
                        End If
                    End If
                Else
                    aOHWGrouperDRSResult.ErrorText = "Bij het lezen van het bestand '" + aFilename + "' is een fout opgetreden"
                    aOHWGrouperDRSResult.Status = False
                End If
            Else
                aOHWGrouperDRSResult.ErrorText = "Bestand '" + aFilename + "' is niet correct: " + zip.LastErrorText
                aOHWGrouperDRSResult.Status = False
            End If
        Else
            aOHWGrouperDRSResult.ErrorText = "Bestand '" + aFilename + "' is niet correct: " + zip.LastErrorText
            aOHWGrouperDRSResult.Status = False
        End If
        Return aOHWGrouperDRSResult
    End Function

    Private Function FindDISBatchFromID(DISBatchID As Long, ByRef OHWRunnummer As Long, ByRef OHWDRS As Boolean, ByRef OHWBatch As Boolean) As String
        Dim bDoNotclose As Boolean = False
        Dim Message As String = ""

        OHWRunnummer = -1
        OHWDRS = False
        OHWBatch = False

        Try
            If dbConnection.State = ConnectionState.Closed Then dbConnection.Open() Else bDoNotclose = True

            Dim dbCommand As New SqlCommand
            dbCommand.Connection = dbConnection
            dbCommand = New System.Data.SqlClient.SqlCommand("SELECT [OHWRunnummer], [OHWDRSCount], [OHWBatchCount] FROM [av_DISBatchOHWRunnummer] WHERE [DISBatchID] = " + DISBatchID.ToString, dbConnection)
            Dim rows As SqlDataReader = dbCommand.ExecuteReader()
            If rows.Read() Then
                If Not rows.IsDBNull(0) Then
                    OHWRunnummer = rows.GetInt64(0)
                End If
                If Not rows.IsDBNull(1) Then
                    OHWDRS = rows.GetInt32(1) > 0
                End If
                If Not rows.IsDBNull(2) Then
                    OHWBatch = rows.GetInt32(2) > 0
                End If
            Else
                Message = "DIS gegevens voor ID '" + DISBatchID.ToString + "' niet gevonden."
            End If
        Catch ex As Exception
            Message = "Onverwachte fout bij ophalen van DIS gegevens voor ID '" + DISBatchID.ToString + "': " + ex.Message
        Finally
            If dbConnection.State = ConnectionState.Open And Not bDoNotclose Then dbConnection.Close()
        End Try
        Return Message
    End Function

    Private Function FindDISBatchFromFilename(Filename As String, ByRef DISBatchID As Long, ByRef OHWRunnummer As Long, ByRef OHWDRS As Boolean, ByRef OHWBatch As Boolean) As String
        Dim bDoNotclose As Boolean = False
        Dim Message As String = ""

        DISBatchID = -1
        OHWRunnummer = -1
        OHWDRS = False
        OHWBatch = False

        Try
            If dbConnection.State = ConnectionState.Closed Then dbConnection.Open() Else bDoNotclose = True

            Dim dbCommand As New SqlCommand
            dbCommand.Connection = dbConnection
            dbCommand = New System.Data.SqlClient.SqlCommand("SELECT [DISBatchID], [OHWRunnummer], [OHWDRSCount], [OHWBatchCount] FROM [av_DISBatchOHWRunnummer] WHERE [Filename] = '" + Filename + ".zip'", dbConnection)
            Dim rows As SqlDataReader = dbCommand.ExecuteReader()
            If rows.Read() Then
                If Not rows.IsDBNull(0) Then
                    DISBatchID = rows.GetInt64(0)
                End If
                If Not rows.IsDBNull(1) Then
                    OHWRunnummer = rows.GetInt64(1)
                End If
                If Not rows.IsDBNull(2) Then
                    OHWDRS = rows.GetInt32(2) > 0
                End If
                If Not rows.IsDBNull(3) Then
                    OHWBatch = rows.GetInt32(3) > 0
                End If
            Else
                Message = "DIS gegevens voor bestand '" + Filename + "' niet gevonden."
            End If
        Catch ex As Exception
            Message = "Onverwachte fout bij ophalen van DIS gegevens voor bestand '" + Filename + "': " + ex.Message
        Finally
            If dbConnection.State = ConnectionState.Open And Not bDoNotclose Then dbConnection.Close()
        End Try
        Return Message
    End Function

    Private Function ProcessOHWGrouperDRSfile(aPath As String, aFile As MemoryStream) As OHWGrouperDRSResult
        Dim aOHWGrouperDRSResult As New OHWGrouperDRSResult
        Dim bOK As Boolean = True
        Dim aFilename As String = ""
        Try
            aFilename = Path.GetFileNameWithoutExtension(aPath)
            If aFilename.Substring(0, 12) = "DIS_SZG_OHW_" And
            aFilename.Substring(16, 5) = "_020_" And
            aFilename.Substring(44, 5) = "_DRS_" Then
            Else
                aOHWGrouperDRSResult.ErrorText = "Filename niet volgens standaard."
                aOHWGrouperDRSResult.Status = False
                Return aOHWGrouperDRSResult
            End If
        Catch ex As Exception
            aOHWGrouperDRSResult.ErrorText = "Filename niet volgens standaard."
            aOHWGrouperDRSResult.Status = False
            Return aOHWGrouperDRSResult
        End Try

        Try
            Dim tfp As New TextFieldParser(aFile)
            tfp.Delimiters = New String() {";"}
            tfp.TextFieldType = FieldType.Delimited

            tfp.ReadLine() ' skip header
            While tfp.EndOfData = False
                Dim aLine As New OHWGrouperDRSResult.LineType
                Dim fields As String() = tfp.ReadFields()
                aLine.Runnummer = Long.Parse(fields(0))
                aLine.VeldType = Integer.Parse(fields(1))
                aLine.Entiteit = fields(2)
                aLine.Declaratieresultsetnummer = fields(3)
                aLine.ID = fields(4)
                aLine.KolomA = fields(5)
                aLine.KolomB = fields(6)
                aLine.KolomC = fields(7)
                aLine.KolomD = fields(8)
                aLine.KolomE = fields(9)
                aLine.KolomF = fields(10)
                aLine.KolomG = fields(11)
                aLine.KolomH = fields(12)
                aLine.KolomI = fields(13)
                aLine.KolomJ = fields(14)
                aLine.KolomK = fields(15)
                If aOHWGrouperDRSResult.Lines Is Nothing Then aOHWGrouperDRSResult.Lines = New List(Of OHWGrouperDRSResult.LineType)
                aOHWGrouperDRSResult.Lines.Add(aLine)
            End While

            aOHWGrouperDRSResult.Filename = aFilename.Substring(0, 44)
            aOHWGrouperDRSResult.Environment = aFilename.Substring(12, 4)
            aOHWGrouperDRSResult.AGBcode = Long.Parse(aFilename.Substring(21, 8))
            aOHWGrouperDRSResult.Instellingsnummer = Long.Parse(aFilename.Substring(30, 2))
            Dim aresult As Date
            If DateTime.TryParseExact(aFilename.Substring(33, 8), "yyyymmdd", System.Globalization.CultureInfo.InvariantCulture, Globalization.DateTimeStyles.None, aresult) Then
                aOHWGrouperDRSResult.CreateDate = DateTime.ParseExact(aFilename.Substring(33, 8), "yyyymmdd", System.Globalization.CultureInfo.InvariantCulture)
            End If
            aOHWGrouperDRSResult.Sequence = Long.Parse(aFilename.Substring(42, 2))
            If DateTime.TryParseExact(aFilename.Substring(49, 13), "yyyyMMdd_HHmm", System.Globalization.CultureInfo.InvariantCulture, Globalization.DateTimeStyles.None, aresult) Then
                aOHWGrouperDRSResult.ProcessDate = DateTime.ParseExact(aFilename.Substring(49, 13), "yyyyMMdd_HHmm", System.Globalization.CultureInfo.InvariantCulture)
            End If
            aOHWGrouperDRSResult.Runnumber = Long.Parse(aFilename.Substring(63))
            aOHWGrouperDRSResult.Status = True
        Catch ex As Exception
            aOHWGrouperDRSResult.ErrorText = ex.Message
            aOHWGrouperDRSResult.Status = False
        End Try
        Return aOHWGrouperDRSResult
    End Function

    Private Function GetOHWBatch(DISBatchID As Integer) As OHWBatch
        Dim bDoNotclose As Boolean = False
        Dim aOHWBatch As New OHWBatch

        Try
            If dbConnection.State = ConnectionState.Closed Then dbConnection.Open() Else bDoNotclose = True
            Dim dbCmdBatch As New SqlCommand
            Dim dbRdrBatch As SqlDataReader
            dbCmdBatch.Connection = dbConnection
            dbCmdBatch.CommandText = "SELECT * FROM [viewOHWBatch] WHERE [NumberRequested] > 0 AND [DISBatchID] = " & DISBatchID.ToString
            dbRdrBatch = dbCmdBatch.ExecuteReader()
            While dbRdrBatch.Read()
                Dim aBatch As New OHWBatch.Batch
                aBatch.ClientID = EvaluateFromSQL(dbRdrBatch("ClientID"))
                aBatch.AGBCode = EvaluateFromSQL(dbRdrBatch("AGBCode"))
                aBatch.Bookyear = EvaluateFromSQL(dbRdrBatch("Bookyear"))
                aBatch.Period = EvaluateFromSQL(dbRdrBatch("Period"))
                aBatch.UZOVICode = EvaluateFromSQL(dbRdrBatch("UZOVICode"))

                Dim dbCmdBatchLines As New SqlCommand
                Dim dbRdrBatchLines As SqlDataReader
                dbCmdBatchLines.Connection = dbConnection
                dbCmdBatchLines.CommandText = "SELECT PatientTreatmentID, Requested FROM viewOHWBatchLine WHERE [ClientID] = " & If(aBatch.ClientID = "", "NULL", aBatch.ClientID.ToString) & " AND [AGBCode] = " & If(aBatch.AGBCode = "", "NULL", "'" & aBatch.AGBCode & "'") & " AND [DISBatchID] = " & DISBatchID.ToString & " AND [UZOVICode] = " & If(aBatch.UZOVICode = "", "NULL", "'" & aBatch.UZOVICode & "'")
                dbRdrBatchLines = dbCmdBatchLines.ExecuteReader()
                While dbRdrBatch.Read()
                    Dim aBatchLine As New OHWBatch.Batch.BatchLine
                    aBatchLine.PatientTreatmentID = EvaluateFromSQL(dbRdrBatch("PatientTreatmentID"))
                    aBatchLine.Requested = EvaluateFromSQL(dbRdrBatch("Requested"))
                    aBatch.AddBatchLine(aBatchLine)
                End While
                dbRdrBatchLines.Close()
                aOHWBatch.AddBatch(aBatch)
            End While
            dbRdrBatch.Close()
        Catch ex As Exception
            aOHWBatch = Nothing
        Finally
            If dbConnection.State = ConnectionState.Open And Not bDoNotclose Then dbConnection.Close()
        End Try
        Return aOHWBatch
    End Function

    Private Function InsertOHWBatch(ByVal DISBatchID As Integer) As OHWGrouperDRSResult
        Dim aOHWGrouperDRSResult As New OHWGrouperDRSResult
        Dim bDoNotclose As Boolean = False
        Dim result As Boolean = True
        Dim rows As Integer
        Dim sqlupdate As System.Data.SqlClient.SqlCommand

        Try
            If dbConnection.State = ConnectionState.Closed Then dbConnection.Open() Else bDoNotclose = True
            Dim aOHWBatch As OHWBatch = GetOHWBatch(DISBatchID)
            If aOHWBatch IsNot Nothing Then
                dbTransaction = dbConnection.BeginTransaction("OHWBatch")
                Dim iBatchnummer As Long = 1
                For Each aBatch As OHWBatch.Batch In aOHWBatch.Batches
                    If Not result Then Exit For
                    sqlupdate = New System.Data.SqlClient.SqlCommand("sp_delete_OHWBatchByDISBatchID", dbConnection, dbTransaction)
                    sqlupdate.CommandType = CommandType.StoredProcedure
                    sqlupdate.Parameters.Add("@DISBatchID", SqlDbType.BigInt).Value = DISBatchID
                    sqlupdate.Parameters.Add("@UpdateUserID", SqlDbType.NVarChar, 8).Value = mUser
                    rows = sqlupdate.ExecuteScalar()
                    If rows > 0 Then
                        sqlupdate = New System.Data.SqlClient.SqlCommand("sp_add_OHWBatch", dbConnection, dbTransaction)
                        sqlupdate.CommandTimeout = 1000
                        sqlupdate.CommandType = CommandType.StoredProcedure
                        sqlupdate.Parameters.Add("@DISBatchID", SqlDbType.BigInt).Value = DISBatchID
                        sqlupdate.Parameters.Add("@ClientID", SqlDbType.Int).Value = aBatch.ClientID
                        sqlupdate.Parameters.Add("@LocationID", SqlDbType.Int).Value = DBNull.Value
                        sqlupdate.Parameters.Add("@Batchnummer", SqlDbType.Int).Value = iBatchnummer
                        sqlupdate.Parameters.Add("@VecozoBatchSendmethodID", SqlDbType.Int).Value = 0
                        sqlupdate.Parameters.Add("@MessageStandardID", SqlDbType.Int).Value = 3
                        sqlupdate.Parameters.Add("@VecozoDeclaratieID", SqlDbType.NVarChar, 50).Value = DBNull.Value
                        sqlupdate.Parameters.Add("@Bookyear", SqlDbType.Int).Value = aBatch.Bookyear
                        sqlupdate.Parameters.Add("@Period", SqlDbType.Int).Value = aBatch.Period
                        sqlupdate.Parameters.Add("@Remarks", SqlDbType.NVarChar, -1).Value = DBNull.Value
                        sqlupdate.Parameters.Add("@InsuranceCompanyOrgcode", SqlDbType.BigInt).Value = DBNull.Value
                        sqlupdate.Parameters.Add("@UZOVICode", SqlDbType.NVarChar, 10).Value = aBatch.UZOVICode
                        sqlupdate.Parameters.Add("@AGBCode", SqlDbType.NVarChar, 8).Value = aBatch.AGBCode
                        sqlupdate.Parameters.Add("@UpdateUserID", SqlDbType.NVarChar, 8).Value = mUser
                        sqlupdate.Parameters.Add("@OHWBatchID", SqlDbType.BigInt)
                        sqlupdate.Parameters("@OHWBatchID").Direction = ParameterDirection.Output
                        rows = sqlupdate.ExecuteScalar()
                        Dim OHWBatchID As Long = EvaluateFromSQL(sqlupdate.Parameters("@OHWBatchID").Value)
                        If rows > 0 Then
                            For Each aBatchLine As OHWBatch.Batch.BatchLine In aBatch.BatchLines
                                If Not result Then Exit For
                                sqlupdate = New System.Data.SqlClient.SqlCommand("sp_add_OHWBatchLine", dbConnection, dbTransaction)
                                sqlupdate.CommandTimeout = 1000
                                sqlupdate.CommandType = CommandType.StoredProcedure
                                sqlupdate.Parameters.Add("@OHWBatchID", SqlDbType.BigInt).Value = OHWBatchID
                                sqlupdate.Parameters.Add("@PatientTreatmentID", SqlDbType.Int).Value = aBatchLine.PatientTreatmentID
                                sqlupdate.Parameters.Add(New SqlParameter("@Requested", SqlDbType.Decimal) With {.Precision = 9, .Scale = 2}).Value = aBatchLine.Requested
                                sqlupdate.Parameters("@OHWBatchLineNumber").Direction = ParameterDirection.Output
                                rows = sqlupdate.ExecuteScalar()
                                Dim OHWBatchLineNumber As Long = EvaluateFromSQL(sqlupdate.Parameters("@OHWBatchLineNumber").Value)
                                If rows > 0 Then
                                    aOHWGrouperDRSResult.ErrorText = "Fout bij toevoegen van OHW informatiebestand regels in [InsertOHWBatch]."
                                    result = False
                                End If
                            Next
                        Else
                            aOHWGrouperDRSResult.ErrorText = "Fout bij toevoegen van OHW informatiebestand header in [InsertOHWBatch]."
                            result = False
                        End If
                    Else
                        aOHWGrouperDRSResult.ErrorText = "Fout bij verwijderen vorige OHW informatiebestanden in [InsertOHWBatch]."
                        result = False
                    End If
                    iBatchnummer += 1
                Next
            End If
            If result Then
                dbTransaction.Commit()
                aOHWGrouperDRSResult.Status = OHWGrouperDRSResult.StatusType.isOK
            Else
                aOHWGrouperDRSResult.Status = OHWGrouperDRSResult.StatusType.isError
                dbTransaction.Rollback()
            End If
        Catch ex As Exception
            If dbConnection.State = ConnectionState.Open Then
                dbTransaction.Rollback()
            End If
            aOHWGrouperDRSResult.Status = OHWGrouperDRSResult.StatusType.isError
            aOHWGrouperDRSResult.ErrorText = "Exceptie in [InsertOHWBatch]: " + ex.Message
        Finally
            If dbConnection.State = ConnectionState.Open And Not bDoNotclose Then dbConnection.Close()
        End Try
        Return aOHWGrouperDRSResult
    End Function

    Public Function InsertOHWGrouperDRS(DISBatchID As Long, aOHWGrouperDRSResult As OHWGrouperDRSResult) As OHWGrouperDRSResult
        Dim aOHWGrouperDRSResult2 As New OHWGrouperDRSResult
        Dim bDoNotclose As Boolean = False
        Dim result As Boolean = False
        Dim rows As Integer
        'Dim aResultaat As New OHWBatch

        Try
            If dbConnection.State = ConnectionState.Closed Then dbConnection.Open() Else bDoNotclose = True
            dbTransaction = dbConnection.BeginTransaction("OHWGrouperDRS")

            Dim sqlupdate As System.Data.SqlClient.SqlCommand
            sqlupdate = New System.Data.SqlClient.SqlCommand("sp_delete_OHWGrouperDRS", dbConnection, dbTransaction)
            sqlupdate.CommandType = CommandType.StoredProcedure
            sqlupdate.Parameters.Add("@Runnummer", SqlDbType.BigInt).Value = aOHWGrouperDRSResult.Runnumber
            rows = sqlupdate.ExecuteScalar()
            If rows > 0 Then
                sqlupdate = New System.Data.SqlClient.SqlCommand("sp_add_OHWGrouperDRS", dbConnection, dbTransaction)
                sqlupdate.CommandType = CommandType.StoredProcedure
                sqlupdate.Parameters.Add("@Runnummer", SqlDbType.BigInt).Value = aOHWGrouperDRSResult.Runnumber
                sqlupdate.Parameters.Add("@Filename", SqlDbType.NVarChar, 2000).Value = aOHWGrouperDRSResult.Filename
                sqlupdate.Parameters.Add("@Environment", SqlDbType.NVarChar, 4).Value = aOHWGrouperDRSResult.Environment
                sqlupdate.Parameters.Add("@AGBcode", SqlDbType.BigInt).Value = aOHWGrouperDRSResult.AGBcode
                sqlupdate.Parameters.Add("@Instellingsnummer", SqlDbType.BigInt).Value = aOHWGrouperDRSResult.Instellingsnummer
                sqlupdate.Parameters.Add("@CreationDate", SqlDbType.DateTime).Value = aOHWGrouperDRSResult.CreateDate
                sqlupdate.Parameters.Add("@Sequence", SqlDbType.BigInt).Value = aOHWGrouperDRSResult.Sequence
                sqlupdate.Parameters.Add("@ProcessDate", SqlDbType.DateTime).Value = aOHWGrouperDRSResult.ProcessDate
                sqlupdate.Parameters.Add("@UpdateUserID", SqlDbType.NVarChar, 8).Value = mUser
                rows = sqlupdate.ExecuteScalar()
                If rows > 0 Then
                    result = True
                    For Each item As OHWGrouperDRSResult.LineType In aOHWGrouperDRSResult.Lines
                        sqlupdate = New System.Data.SqlClient.SqlCommand("sp_add_OHWGrouperDRSLines", dbConnection, dbTransaction)
                        sqlupdate.CommandType = CommandType.StoredProcedure
                        sqlupdate.Parameters.Add("@Runnummer", SqlDbType.BigInt).Value = item.Runnummer
                        sqlupdate.Parameters.Add("@VeldType", SqlDbType.Int).Value = item.VeldType
                        sqlupdate.Parameters.Add("@Entiteit", SqlDbType.NVarChar, 2000).Value = item.Entiteit
                        sqlupdate.Parameters.Add("@Declaratieresultsetnummer", SqlDbType.NVarChar, 40).Value = item.Declaratieresultsetnummer
                        sqlupdate.Parameters.Add("@ID", SqlDbType.NVarChar, 200).Value = item.ID
                        sqlupdate.Parameters.Add("@KolomA", SqlDbType.NVarChar, 2000).Value = item.KolomA
                        sqlupdate.Parameters.Add("@KolomB", SqlDbType.NVarChar, 2000).Value = item.KolomB
                        sqlupdate.Parameters.Add("@KolomC", SqlDbType.NVarChar, 2000).Value = item.KolomC
                        sqlupdate.Parameters.Add("@KolomD", SqlDbType.NVarChar, 2000).Value = item.KolomD
                        sqlupdate.Parameters.Add("@KolomE", SqlDbType.NVarChar, 2000).Value = item.KolomE
                        sqlupdate.Parameters.Add("@KolomF", SqlDbType.NVarChar, 2000).Value = item.KolomF
                        sqlupdate.Parameters.Add("@KolomG", SqlDbType.NVarChar, 2000).Value = item.KolomG
                        sqlupdate.Parameters.Add("@KolomH", SqlDbType.NVarChar, 2000).Value = item.KolomH
                        sqlupdate.Parameters.Add("@KolomI", SqlDbType.NVarChar, 2000).Value = item.KolomI
                        sqlupdate.Parameters.Add("@KolomJ", SqlDbType.NVarChar, 2000).Value = item.KolomJ
                        sqlupdate.Parameters.Add("@KolomK", SqlDbType.NVarChar, 2000).Value = item.KolomK
                        sqlupdate.Parameters.Add("@UpdateUserID", SqlDbType.NVarChar, 8).Value = mUser
                        rows = sqlupdate.ExecuteScalar()
                        If rows <= 0 Then
                            aOHWGrouperDRSResult.ErrorText = "Fout bij het toevoegen van OHW Grouper retourbestand regels in [InsertOHWGrouperDRS]."
                            result = False
                            Exit For
                        End If
                    Next
                Else
                    aOHWGrouperDRSResult.ErrorText = "Fout bij het toevoegen van OHW Grouper retourbestand header in [InsertOHWGrouperDRS]."
                    result = False
                End If
            Else
                aOHWGrouperDRSResult.ErrorText = "Fout bij het verwijderen van vorige OHW Grouper retourbestand in [InsertOHWGrouperDRS]."
                result = False
            End If
            If result Then
                sqlupdate = New System.Data.SqlClient.SqlCommand("sp_update_DISBatchOHWRunnummer", dbConnection, dbTransaction)
                sqlupdate.CommandType = CommandType.StoredProcedure
                sqlupdate.Parameters.Add("@ID", SqlDbType.BigInt).Value = DISBatchID
                sqlupdate.Parameters.Add("@OHWRunnummer", SqlDbType.BigInt).Value = aOHWGrouperDRSResult.Runnumber
                sqlupdate.Parameters.Add("@UpdateUserID", SqlDbType.NVarChar, 8).Value = mUser
                rows = sqlupdate.ExecuteScalar()
                If rows <= 0 Then
                    result = True
                    dbTransaction.Commit()
                Else
                    result = False
                    aOHWGrouperDRSResult.ErrorText = "Fout bij het updaten van OHW Grouper retourbestand ID in [InsertOHWGrouperDRS]."
                    dbTransaction.Rollback()
                End If
            End If
            aOHWGrouperDRSResult.Status = result
        Catch ex As Exception
            If dbConnection.State = ConnectionState.Open Then
                dbTransaction.Rollback()
            End If
            aOHWGrouperDRSResult.Status = OHWGrouperDRSResult.StatusType.isError
            aOHWGrouperDRSResult.ErrorText = "Exceptie in [InsertOHWGrouperDRS]: " + ex.Message
        Finally
            If dbConnection.State = ConnectionState.Open And Not bDoNotclose Then dbConnection.Close()
        End Try
        Return aOHWGrouperDRSResult
    End Function
End Class
