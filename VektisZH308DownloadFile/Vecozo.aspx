﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Vecozo.aspx.vb" Inherits="VektisZH308DownloadFile.Vecozo" %>

<%@ Register assembly="DevExpress.Web.v17.1" namespace="DevExpress.Web" tagprefix="dx" %>

<%@ Register assembly="DevExpress.Web.ASPxSpellChecker.v17.1" namespace="DevExpress.Web.ASPxSpellChecker" tagprefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 83px;
        }
        .auto-style2 {
            width: 93px;
        }
        .auto-style3 {
            width: 83px;
            font-family: Tahoma, Geneva, sans-serif;
            font-size: 12px;
            color: #2C4D79;
            height: 25px;
            background-color: #E2F0FF;
            background-repeat: repeat-x;
            background-position: 50% top;
        }
        .auto-style4 {
            width: 93px;
            height: 25px;
        }
        .auto-style5 {
            height: 25px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="dxeBinImgCPnlSys">
                <tr>
                    <td class="auto-style3">Valideren</td>
                    <td class="auto-style4">
                        <dx:ASPxButton ID="btnDeclareren" runat="server" Text="Declareren" Theme="Aqua">
                        </dx:ASPxButton>
                    </td>
                    <td class="auto-style5"></td>
                    <td class="auto-style5"></td>
                    <td class="auto-style5"></td>
                    <td class="auto-style5"></td>
                    <td class="auto-style5"></td>
                    <td class="auto-style5"></td>
                </tr>
                <tr>
                    <td class="auto-style1">&nbsp;</td>
                    <td class="auto-style2">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
            <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="sqlsdVecozoBatch" KeyFieldName="VecozoBatchID" Theme="Aqua">
                <settingscontextmenu enabled="True" enablefootermenu="False" enablegroupfootermenu="False" enablegrouppanelmenu="False" enablerowmenu="True">
                </settingscontextmenu>
                <settingspager pagesize="20">
                </settingspager>
                <settings showfilterrow="True" />
                <settingsbehavior allowfocusedrow="True" allowselectbyrowclick="True" allowselectsinglerowonly="True" />
                <settingsdatasecurity allowdelete="False" allowedit="False" allowinsert="False" />
                <Columns>
                    <dx:GridViewCommandColumn ShowClearFilterButton="True" VisibleIndex="0">
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataTextColumn FieldName="VecozoBatchID" ReadOnly="True" Visible="False" VisibleIndex="1">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Zorgverzekeraar" FieldName="OrgName" VisibleIndex="7">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="UZOVI" FieldName="UZOVICode" VisibleIndex="8">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Batchnummer" VisibleIndex="3">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataDateColumn Caption="Datum" FieldName="Batchdate" VisibleIndex="4">
                    </dx:GridViewDataDateColumn>
                    <dx:GridViewDataTextColumn Caption="Verzendmode" FieldName="VecozoBatchSendmethodID" VisibleIndex="9">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Berichtstandaard" FieldName="MessageStandardID" VisibleIndex="10">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Vecozo Berichtstandaard" FieldName="VecozoMessageStandard" VisibleIndex="11">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Status" FieldName="VecozoBatchStatus" VisibleIndex="12">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="DeclaratieID" FieldName="VecozoDeclaratieID" VisibleIndex="13">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Boekjaar" FieldName="Bookyear" VisibleIndex="5">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataDateColumn Caption="Verzendatum" FieldName="Senddate" VisibleIndex="14">
                    </dx:GridViewDataDateColumn>
                    <dx:GridViewDataDateColumn Caption="Returndatum" FieldName="Returndate" VisibleIndex="15">
                    </dx:GridViewDataDateColumn>
                    <dx:GridViewDataTextColumn Caption="Verwerkt" FieldName="Processed" VisibleIndex="16">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Locatie" FieldName="LocationName" VisibleIndex="2">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Ingediend" FieldName="Requested" ReadOnly="True" VisibleIndex="17">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Toegewezen" FieldName="Assigned" ReadOnly="True" VisibleIndex="18">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Periode" FieldName="Period" VisibleIndex="6">
                    </dx:GridViewDataTextColumn>
                </Columns>
            </dx:ASPxGridView>
            <asp:SqlDataSource ID="sqlsdVecozoBatch" runat="server" ConnectionString="<%$ ConnectionStrings:IntomediFin %>" SelectCommand="SELECT [VecozoBatchID], [OrgName], [UZOVICode], [Batchnummer], [Batchdate], [VecozoBatchSendmethodID], [MessageStandardID], [VecozoMessageStandard], [VecozoBatchStatus], [VecozoDeclaratieID], [Period], [Bookyear], [Senddate], [Returndate], [Processed], [LocationName], [Requested], [Assigned] FROM [av_VecozoBatch] WHERE ([Deleted] = @Deleted) ORDER BY [Batchnummer] DESC">
                <SelectParameters>
                    <asp:Parameter DefaultValue="F" Name="Deleted" Type="String" />
                </SelectParameters>
            </asp:SqlDataSource>
            <dx:ASPxPopupMenu ID="ASPxPopupMenu1" runat="server">
                <Items>
                    <dx:MenuItem Text="Valideer">
                    </dx:MenuItem>
                    <dx:MenuItem Text="Verzend">
                    </dx:MenuItem>
                </Items>
            </dx:ASPxPopupMenu>
        </div>
    </form>
</body>
</html>
