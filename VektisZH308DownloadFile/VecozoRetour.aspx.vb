﻿Imports System.IO
Imports System.IO.Compression

Public Class VecozoRetour
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub btnProcessZH309_Click(sender As Object, e As EventArgs) Handles btnProcessZH309.Click

        UploadVecozoRetourFile()



    End Sub

    Public Function UploadVecozoRetourFile() As String
        Dim Message As String = ""
        Dim VektisZH308 As New VektisDeclaratieRetour

        Dim glob As New Chilkat.Global
        Dim success As Boolean = glob.UnlockBundle("AYKRBS.CB1032020_sGdL35KunR1I")
        If (success <> True) Then
            Return "Fout in module Chillkat: " + glob.LastErrorText
        End If

        If FileUpload1.HasFiles Then
            For Each aFile As HttpPostedFile In FileUpload1.PostedFiles
                Dim bData As Byte()
                Dim br As BinaryReader = New BinaryReader(aFile.InputStream)
                bData = br.ReadBytes(CInt(br.BaseStream.Length))
                br.Close()

                Dim zip As New Chilkat.Zip
                success = zip.OpenFromMemory(bData)
                If success Then
                    If zip.LastMethodSuccess Then
                        Dim zipentry As Chilkat.ZipEntry = zip.GetEntryByIndex(0)
                        Dim fileData As Byte()
                        If zipentry.IsDirectory = False Then
                            fileData = zipentry.Inflate()
                            Dim memStream As MemoryStream = New MemoryStream(fileData, 0, fileData.Length)
                            If memStream IsNot Nothing Then
                                Dim respons As VektisDeclaratieRetour.Resultaat = VektisZH308.ProcessVektisDeclaratieRetour(aFile.FileName, memStream)
                                If respons IsNot Nothing Then
                                    'txtResultaatProcessZH309.Text += respons.DebugTextLight + vbCrLf
                                    Dim aResultaat As VektisDeclaratieRetour.Resultaat = GetVektisDeclaratieRetour(respons.VecozoBatchID)

                                    Dim sUpdateResult As VektisDeclaratieRetour.UpdateResultaatZH309 = VektisZH308.UpdateZH309(chkSimulatie.Checked, respons)
                                    '                            txtResultaatHeader.Text += aFile.FileName & "," + sUpdateResult + vbCrLf
                                Else
                                    '                            Message += aFile.FileName + " - Onbekende fout.\n"
                                    ' txtResultaatProcessZH309.Text = "ONBEKENDE FOUT"
                                End If
                                memStream.Close()
                            End If
                        End If
                        zip.CloseZip()
                    Else
                        Message += aFile.FileName + " - Geen Zip bestand.\n"
                    End If
                End If
            Next
        End If
    End Function


End Class